
package com.dormalcorp.talkie

import io.flutter.app.FlutterApplication
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback
import io.flutter.plugins.GeneratedPluginRegistrant
import io.flutter.plugins.firebase.messaging.FlutterFirebaseMessagingPlugin
import com.github.cloudwebrtc.flutter_callkeep.FlutterCallkeepPlugin
import io.flutter.plugins.sharedpreferences.SharedPreferencesPlugin

class Application : FlutterApplication(), PluginRegistrantCallback {
 
  override fun onCreate() {
    super.onCreate()
  }

  override fun registerWith(registry: PluginRegistry?) {
 val key: String? = FlutterFirebaseMessagingPlugin::class.java.canonicalName
      if (!registry?.hasPlugin(key)!!) {
          FlutterFirebaseMessagingPlugin.registerWith(registry?.registrarFor("io.flutter.plugins.firebase.messaging.FlutterFirebaseMessagingPlugin"));
        }
            FlutterCallkeepPlugin.registerWith(registry?.registrarFor("com.github.cloudwebrtc.flutter_callkeep"));
    SharedPreferencesPlugin.registerWith(registry?.registrarFor("io.flutter.plugins.sharedpreferences.SharedPreferencesPlugin"));    
  }
  
}
