import 'dart:async';
import 'dart:convert';

//import 'package:dart_mercure/dart_mercure.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:mime/mime.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/Page/MyWidgetSlider.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/call.dart';
import 'package:voicechanger/data/network/Model/data.dart';
import 'package:voicechanger/data/network/Model/detailMessage.dart';
import 'dart:io' as io;
import 'dart:convert';
import 'package:file/file.dart';
import 'package:file/local.dart';
import 'package:path_provider/path_provider.dart';
import 'package:mercure_client/mercure_client.dart';

import 'call_screen.dart';
import 'filterRecordPage.dart';

class DetailMessagePage extends StatefulWidget {
  @override
  _DetailMessagePageState createState() => _DetailMessagePageState();
}

class _DetailMessagePageState extends State<DetailMessagePage> {
  @override
  Widget build(BuildContext context) {
    Map arguments;
    int id;
    String nom;

    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    if (arguments != null) {
      print("iddd" + arguments['id'].toString());
      id = arguments['id'];
      nom = arguments['nom'];
    }
    return DetailPage(id: id, nom: nom);
  }
}

class DetailPage extends StatefulWidget {
  final LocalFileSystem localFileSystem;

  final int id;
  final String nom;
  DetailPage({Key key, this.id, this.nom, localFileSystem})
      : this.localFileSystem = localFileSystem ?? LocalFileSystem();

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage>
    with SingleTickerProviderStateMixin {
  Future<DetailMessage> detailMessage;
  var listMessages = new List<DetailMessages>();
  double spreadradius = 0;
  FlutterAudioRecorder _recorder;
  Recording _current;
  RecordingStatus _currentStatus = RecordingStatus.Unset;
  bool recorder = false;
  String timer;
  File file;
  Timer _timer;
  Entete entete;
  String duree;
  Future<Data> data;
  int start = 60;
  String base64audio;
  String user_id;
  int currentPage = 1;
  int maxPage;
  //AudioPlayer _audioPlayer = AudioPlayer(playerId: 'my_unique_playerId');
  ScrollController _scrollController;
  bool selectable = false;
  double width_icone = 44;
  double height_icone = 44;
  bool activateSend = false;
  bool playing;
  Sender sender;
  List<Sender> receivers;
  // Mercure mercure;
  final TextEditingController textEditingController = TextEditingController();
  final FocusNode focusNode = FocusNode();
  String _base64Image;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  final mercure = Mercure(
    Endpoints.hub_url, // your mercure hub url
    Endpoints.topic_send_message, // your mercure topic
    token: Endpoints.token, // Bearer authorization
    // in case your stored last recieved event
    //showLogs: true, // Default to false
  );
  void startTimer(int timerDuration) {
    setState(() {
      start = timerDuration;
    });

    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (start < 1) {
            timer.cancel();
            _stop();
          } else {
            start = start - 1;
          }
        },
      ),
    );
  }

  @override
  void initState() {
    _scrollController = new ScrollController();
    super.initState();
    // mercure = Mercure(token: Endpoints.token, hub_url: Endpoints.hub_url);

    _init();
    _scrollController.addListener(() {
      if (_scrollController.position.atEdge) {
        if (_scrollController.position.pixels == 0 && currentPage < maxPage) {
          _getMoreData();
        }
      }
    });
    getToken().then((token) => {
          getDetailMessage(token, widget.id, 1),
        });
    getToken().then((id) {
      getUserID(id);
    });
    selectable = false;
    socketMessage();
  }

  _getMoreData() {
    currentPage++;

    getToken().then((cleapi) {
      detailMessage =
          ClientApi.getDetailMessage(cleapi, widget.id, currentPage);
      detailMessage
          .then((value) => {
                setState(() {
                  listMessages = value.data.messages + listMessages;
                })
              })
          .catchError((error) =>
              {EasyLoading.dismiss(), print("erreur : " + error.toString())});
    });
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  getDetailMessage(String token, int id, int page) async {
    detailMessage = ClientApi.getDetailMessage(token, id, page);
    detailMessage
        .then((value) => {
              print(value.data.sender.id),
              setState(() {
                entete = value.data.entete;
                listMessages = value.data.messages;
                sender = value.data.sender;
                receivers = value.data.receivers;
                maxPage = value.data.pagination.pages;
              }),
              WidgetsBinding.instance.addPostFrameCallback((_) => Timer(
                    Duration(milliseconds: 300),
                    () => {
                      if (_scrollController.hasClients)
                        {
                          _scrollController.jumpTo(
                              _scrollController.position.maxScrollExtent)
                        }
                    },
                  ))
            })
        .catchError((error) => {print("erreur : " + error.toString())});
  }

  @override
  void dispose() {
    textEditingController.dispose();
    focusNode.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    /*     Timer(
      Duration(milliseconds: 300),
      () => {
        if (_scrollController.hasClients)
          {_scrollController.jumpTo(_scrollController.position.maxScrollExtent)
          
          }
      },
    );*/

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          elevation: 0,
          centerTitle: true,
          backgroundColor: Color(0xff25a996),
          title: RichText(
            maxLines: 2,
            overflow:
                TextOverflow.ellipsis, // TextOverflow.clip // TextOverflow.fade
            textAlign: TextAlign.center,
            text: (entete != null)
                ? TextSpan(
                    text: entete.nom,
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      color: Color(0xffffffff),
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                    children: <TextSpan>[
                        if (entete?.is_groupe)
                          TextSpan(
                            text: '\n${entete.talkers}',
                            style: TextStyle(
                              fontFamily: 'Roboto-thin',
                              color: Color(0xffffffff),
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                      ])
                : TextSpan(),
          ),
          actions: <Widget>[
            if (entete != null)
              if (!entete.is_groupe)
                if (sender.id != receivers[0].id)
                  IconButton(
                    icon: Icon(
                      Icons.phone,
                    ),
                    onPressed: () async => call(),
                  ),
            StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return IconButton(
                  icon: SvgPicture.asset(
                    "assets/icon/more.svg",
                  ),
                  onPressed: () {
                    final act = CupertinoActionSheet(
                        actions: <Widget>[
                          if (entete.is_groupe)
                            CupertinoActionSheetAction(
                              child: Text(
                                  AppLocalizations.of(context)
                                      .translate('edit_group'),
                                  style: TextStyle(
                                    fontFamily: 'SFProText',
                                    color: Color(0xff262626),
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  )),
                              onPressed: () {
                                Navigator.of(context, rootNavigator: true)
                                    .pop();
                                Navigator.pushNamed(context, "/editGroup",
                                    arguments: {
                                      'id': entete.id_conv,
                                    });
                              },
                            ),
                          if (entete.is_groupe)
                            CupertinoActionSheetAction(
                              child: Text(
                                  AppLocalizations.of(context)
                                      .translate('group_info'),
                                  style: TextStyle(
                                    fontFamily: 'SFProText',
                                    color: Color(0xff262626),
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  )),
                              onPressed: () {
                                Navigator.of(context, rootNavigator: true)
                                    .pop();

                                Navigator.pushNamed(context, "/infogroup",
                                    arguments: {
                                      'id': entete.id_conv,
                                    });
                              },
                            ),
                          if (!entete.is_groupe &&
                              entete.idReceivers.isNotEmpty)
                            CupertinoActionSheetAction(
                              child: Text(
                                  AppLocalizations.of(context)
                                          .translate('block') +
                                      " ${entete.nom}",
                                  style: TextStyle(
                                    fontFamily: 'SFProText',
                                    color: Color(0xffec4956),
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  )),
                              onPressed: () {
                                Navigator.of(context, rootNavigator: true)
                                    .pop();

                                showAlertDialogBlock(
                                    context, entete.idReceivers[0]);
                              },
                            ),
                          if (!entete.is_groupe &&
                              entete.idReceivers.isNotEmpty)
                            CupertinoActionSheetAction(
                              child: Text(
                                  AppLocalizations.of(context)
                                          .translate('report') +
                                      " ${entete.nom}",
                                  style: TextStyle(
                                    fontFamily: 'SFProText',
                                    color: Color(0xff262626),
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  )),
                              onPressed: () {
                                Navigator.of(context, rootNavigator: true)
                                    .pop();
                                Navigator.pushNamed(context, "/signalercontact",
                                    arguments: {
                                      'id': entete.idReceivers[0],
                                      'username': entete.nom
                                    });
                              },
                            ),
                          if (!entete.is_groupe &&
                              entete.idReceivers.isNotEmpty)
                            CupertinoActionSheetAction(
                              child: Text(
                                  AppLocalizations.of(context)
                                          .translate('entrer_world') +
                                      " ${entete.nom}",
                                  style: TextStyle(
                                    fontFamily: 'SFProText',
                                    color: Color(0xff262626),
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  )),
                              onPressed: () {
                                Navigator.of(context, rootNavigator: true)
                                    .pop();
                                Navigator.pushNamed(context, "/chainePage",
                                    arguments: {
                                      'id': entete.idReceivers[0],
                                      'username': entete.nom
                                    });
                              },
                            ),
                          CupertinoActionSheetAction(
                            child: Text(
                                AppLocalizations.of(context)
                                    .translate('delete_message'),
                                style: TextStyle(
                                  fontFamily: 'SFProText',
                                  color: Color(0xff262626),
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            onPressed: () {
                              this.setState(() {
                                selectable = true;
                              });

                              Navigator.of(context, rootNavigator: true).pop();
                            },
                          )
                        ],
                        cancelButton: CupertinoActionSheetAction(
                          child: Text(
                              AppLocalizations.of(context).translate('cancel'),
                              style: TextStyle(
                                fontFamily: 'SFProText',
                                color: Color(0xff000000),
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop();
                          },
                        ));
                    showCupertinoModalPopup(
                        context: context,
                        builder: (BuildContext context) => act);
                  });
            })
          ],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
                child: Padding(
                    padding: EdgeInsets.all(10),
                    child: new ListView.builder(
                        controller: _scrollController,
                        cacheExtent: 999999999999999,
                        itemCount: currentPage == maxPage
                            ? listMessages.length
                            : listMessages.length + 1,
                        itemBuilder: (BuildContext ctxt, int index) {
                          if (listMessages.isNotEmpty) {
                            return (index >= listMessages.length)
                                ? null
                                : (index == 0)
                                    ? InkWell(
                                        onTap: () => {
                                              FocusScope.of(context)
                                                  .requestFocus(
                                                      new FocusNode()),
                                              if (selectable &&
                                                  listMessages[index]
                                                      .current_user)
                                                {
                                                  setState(() {
                                                    for (int i = 0;
                                                        i < listMessages.length;
                                                        i++) {
                                                      listMessages[i].selected =
                                                          false;
                                                    }
                                                    listMessages[index]
                                                        .selected = true;
                                                  }),
                                                  showAlertDialogDelete(
                                                      context,
                                                      listMessages[index].id,
                                                      index)
                                                }
                                            },
                                        child: IgnorePointer(
                                            ignoring: selectable,
                                            child: MyWidgetSlider(
                                                listMessages[index],
                                                entete,
                                                "",
                                                false,
                                                _scrollController,
                                                deleteMessage: () => deleteMessage(
                                                    listMessages[index].id,
                                                    index),
                                                reset: () => reset(
                                                    listMessages[index].id),
                                                key: ObjectKey(
                                                    listMessages[index].id))))
                                    : InkWell(
                                        onTap: () => {
                                              FocusScope.of(context)
                                                  .requestFocus(
                                                      new FocusNode()),
                                              if (selectable &&
                                                  listMessages[index]
                                                      .current_user)
                                                {
                                                  setState(() {
                                                    for (int i = 0;
                                                        i < listMessages.length;
                                                        i++) {
                                                      listMessages[i].selected =
                                                          false;
                                                    }
                                                    listMessages[index]
                                                        .selected = true;
                                                  }),
                                                  showAlertDialogDelete(
                                                      context,
                                                      listMessages[index].id,
                                                      index)
                                                }
                                            },
                                        child: IgnorePointer(
                                            ignoring: selectable,
                                            child: MyWidgetSlider(
                                                listMessages[index],
                                                entete,
                                                listMessages[index - 1].date,
                                                listMessages[index - 1].current_user,
                                                _scrollController,
                                                deleteMessage: () => deleteMessage(listMessages[index].id, index),
                                                reset: () => reset(listMessages[index].id),
                                                key: ObjectKey(listMessages[index].id))));
                          } else
                            return null;
                        }))),
          ],
        ),
        bottomNavigationBar: Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (entete != null) buildInput(),
                if (entete != null)
                  Container(
                      decoration: new BoxDecoration(
                        color: Color(0xffffffff),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x282d2d2d),
                              offset: Offset(0, -3),
                              blurRadius: 20,
                              spreadRadius: 0)
                        ],
                      ),
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(0, 11, 0, 28),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(timer,
                                    style: TextStyle(
                                      fontFamily: 'Ubuntu',
                                      color: Color(0xff949494),
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                                SizedBox(
                                  width: 15,
                                ),
                                GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              FilterRecordPage(
                                                  id_conv:
                                                      entete.id_conv.toString(),
                                                  idReceivers:
                                                      entete.idReceivers),
                                          fullscreenDialog: true,
                                        ),
                                      );
                                    },
                                    onLongPress: () => {
                                          HapticFeedback.vibrate(),
                                          _start(),
                                          setState(() {
                                            width_icone = 74;
                                            height_icone = 74;
                                            spreadradius = 5;
                                          })
                                        },
                                    onLongPressUp: () {
                                      _stop();
                                      setState(() {
                                        width_icone = 44;
                                        height_icone = 44;
                                        spreadradius = 0;
                                      });
                                    },
                                    child: Container(
                                        width: width_icone,
                                        height: height_icone,
                                        decoration: new BoxDecoration(
                                          color: Color(0xff25a996),
                                          border: new Border.all(
                                              color: Colors.white, width: 2),
                                          shape: BoxShape.circle,
                                          boxShadow: [
                                            BoxShadow(
                                              color: Color(0x33000000),
                                              offset: Offset(0, 0),
                                              blurRadius: 0,
                                              spreadRadius: spreadradius,
                                            )
                                          ],
                                        ),
                                        child: Padding(
                                            padding: EdgeInsets.all(11),
                                            child: SvgPicture.asset(
                                                ("assets/icon/mic-msg.svg"))))),
                                SizedBox(
                                  width: 15,
                                ),
                                if (start != 60)
                                  Text("0" + ":" + "$start",
                                      style: TextStyle(
                                        fontFamily: 'Ubuntu',
                                        color: Color(0xff949494),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      ))
                              ])))
              ],
            )));
  }

  Widget buildInput() {
    return Container(
      child: Row(
        children: <Widget>[
          // Button send image
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.0),
              child: IconButton(
                icon: Icon(Icons.image),
                onPressed: () => {getFile()},
                color: Color(0xff25a996),
              ),
            ),
            color: Colors.white,
          ),

          // Edit text
          Flexible(
            child: TextField(
              onSubmitted: null,
              onChanged: (value) {
                if (value.isNotEmpty) {
                  setState(() {
                    activateSend = true;
                  });
                } else {
                  setState(() {
                    activateSend = false;
                  });
                }
              },
              style: TextStyle(fontSize: 15.0),
              controller: textEditingController,
              decoration: InputDecoration.collapsed(
                hintText:
                    AppLocalizations.of(context).translate('type_message'),
                hintStyle: TextStyle(color: Colors.grey),
                border: InputBorder.none,
              ),
              focusNode: focusNode,
            ),
          ),

          // Button send message
          if (activateSend)
            Material(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 8.0),
                child: IconButton(
                  icon: Icon(Icons.send),
                  onPressed: () => {
                    getToken().then((cleapi) {
                      sendInstantMessage(
                          cleapi,
                          entete.idReceivers,
                          "text",
                          entete.id_conv.toString(),
                          textEditingController.text,
                          null,
                          null);
                    }),
                  },
                  color: Color(0xff25a996),
                ),
              ),
              color: Colors.white,
            ),
        ],
      ),
      width: double.infinity,
      height: 50.0,
      decoration: BoxDecoration(
          border:
              Border(top: BorderSide(color: Colors.transparent, width: 0.5)),
          color: Colors.white),
    );
  }

  Future getFile() async {
    FilePickerResult result =
        await FilePicker.platform.pickFiles(type: FileType.media);

    if (result != null) {
      PlatformFile file = result.files.first;
      io.File myFile = io.File(file.path);
      List<int> imageBytes = myFile.readAsBytesSync();
      print(base64Encode(imageBytes));
      String base64 = base64Encode(imageBytes);
      print(file.extension);

      String mimeStr = lookupMimeType(file.path);
      var fileType = mimeStr.split('/');
      print('file type ${fileType[0]}');

      getToken().then((cleapi) {
        sendInstantMessage(cleapi, entete.idReceivers, fileType[0],
            entete.id_conv.toString(), null, file.extension, base64);
      });
    } else {
      print('No image selected.');
    }
  }

  reset(id) {
    for (int i = 0; i < listMessages.length; i++) {
      if (listMessages[i].playerState == PlayerState.playing)
        setState(() {
          listMessages[i].playerState = PlayerState.stopped;
        });
    }
  }

  sendQucikMessage(
      String token, String file, var id_reciver, String durre, String id_conv) {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );

    data = ClientApi.sendquickMessage(token, file, id_reciver, durre, id_conv);
    data
        .then((value) => {
              print(value.data),
              EasyLoading.dismiss(),
              EasyLoading.showSuccess(
                  AppLocalizations.of(context).translate('message_send')),
              /* mercure.publish(topic: Endpoints.topic_quick_message, data: json.encode(value.socket)).then((status) {
    if (status == 200) {
      print('Message Sent');
    } else {
      print('Message Failed with code : $status');
    }
  }),*/

              cancelMessage()
            })
        .catchError((error) => {
              EasyLoading.showError(
                  AppLocalizations.of(context).translate('error_try_again')),
              print("erreur11 : " + error.toString()),
              EasyLoading.dismiss(),
              cancelMessage()
            });
  }

  sendInstantMessage(String token, var id_recivers, String type, String id_conv,
      String message, String extension, String file) {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;
    if (type != "text")
      EasyLoading.show(
        status: AppLocalizations.of(context).translate('loading'),
      );

    data = ClientApi.sendInstantMessage(
        token, id_recivers, type, id_conv, message, extension, file);
    data
        .then((value) async => {
              print(value.data),
              textEditingController.clear(),
              if (type != "text") EasyLoading.dismiss(),
              setState(() {
                activateSend = false;
              }),
              /* Mercure.publish<Socket>(
      url: Endpoints.hub_url,
      topic: Endpoints.topic_instant_message,
      data: json.encode(value.socket),
    ).then((response) {
  print(response.statusCode);
})*/
              /*    mercure.publish(topic: Endpoints.topic_instant_message, data: json.encode(value.socket)).then((status) {
                                    print(status);

    if (status == 200) {
      print('Message Sent');
    } else {
      print('Message Failed with code : $status');
    }
  }),*/
            })
        .catchError((error) => {
              EasyLoading.showError(
                  AppLocalizations.of(context).translate('error_try_again')),
              if (type != "text") EasyLoading.dismiss(),
              print("erreur11 : " + error.toString()),
            });
  }

  showAlertDialogDelete(BuildContext context, int id, int index) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(AppLocalizations.of(context).translate('no')),
      onPressed: () {
        this.setState(() {
          selectable = false;
          listMessages[index].selected = false;
        });
        Navigator.of(context, rootNavigator: true).pop(); // dismiss dialog
      },
    );
    Widget continueButton = FlatButton(
      child: Text(AppLocalizations.of(context).translate('yes')),
      onPressed: () async {
        deleteMessage(id, index);

        Navigator.of(context, rootNavigator: true).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(AppLocalizations.of(context)
          .translate('confirmation_deletion_title')),
      content: Text(AppLocalizations.of(context)
          .translate('confirmation_deletion_message')),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showAlertDialogBlock(BuildContext context, int id) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(AppLocalizations.of(context).translate('no')),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop(); // dismiss dialog
      },
    );
    Widget continueButton = FlatButton(
      child: Text(AppLocalizations.of(context).translate('yes')),
      onPressed: () {
        blockContact(id);
        Navigator.of(context, rootNavigator: true).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title:
          Text(AppLocalizations.of(context).translate('block_contact_title')),
      content:
          Text(AppLocalizations.of(context).translate('block_contact_message')),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  blockContact(id) {
    getToken().then((token) => {
          EasyLoading.instance
            ..loadingStyle = EasyLoadingStyle.custom
            ..backgroundColor = Colors.white
            ..progressColor = Colors.white
            ..indicatorType = EasyLoadingIndicatorType.wave
            ..indicatorColor = Color(0xff3bc0ae)
            ..textColor = Color(0xff3bc0ae)
            ..userInteractions = false
            ..maskType = EasyLoadingMaskType.black,
          EasyLoading.show(
            status: AppLocalizations.of(context).translate('loading'),
          ),
          data = ClientApi.blockUser(token, id),
          data
              .then((value) => {
                    EasyLoading.dismiss(),
                    Fluttertoast.showToast(
                        msg: value.data,
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.CENTER,
                        backgroundColor: Colors.black,
                        textColor: Colors.white,
                        fontSize: 16.0),
                    Navigator.of(context).pushNamedAndRemoveUntil(
                      '/home',
                      (Route<dynamic> route) => false,
                    ),
                  })
              .catchError((error) => {
                    EasyLoading.dismiss(),
                    Fluttertoast.showToast(
                        msg: error.toString(),
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.CENTER,
                        backgroundColor: Colors.black,
                        textColor: Colors.white,
                        fontSize: 16.0),
                    print("erreur : " + error.toString())
                  }),
        });
  }

  call() {
    getToken().then((token) => {
          EasyLoading.instance
            ..loadingStyle = EasyLoadingStyle.custom
            ..backgroundColor = Colors.white
            ..progressColor = Colors.white
            ..indicatorType = EasyLoadingIndicatorType.wave
            ..indicatorColor = Color(0xff3bc0ae)
            ..textColor = Color(0xff3bc0ae)
            ..userInteractions = false
            ..maskType = EasyLoadingMaskType.black,
          EasyLoading.show(
            status: AppLocalizations.of(context).translate('loading'),
          ),
          data =
              ClientApi.call(token, receivers[0].id, entete.id_conv.toString()),
          data
              .then((value) => {
                    EasyLoading.dismiss(),
                    print(value.data),
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CallScreen(
                            call: Call(
                          callerId: sender.id.toString(),
                          callerName: sender.nom,
                          callerPic: sender.photo,
                          receiverId: receivers[0].id,
                          receiverName: receivers[0].nom,
                          receiverPhoto: receivers[0].photo,
                          channelId: entete.id_conv.toString(),
                          call_id: int.parse(value.call_id),
                        )),
                      ),
                    ).then((value) => {
                          getToken().then((token) =>
                              {getDetailMessage(token, widget.id, 1)})
                        })
                  })
              .catchError((error) => {
                    EasyLoading.dismiss(),
                    Fluttertoast.showToast(
                        msg: error.toString(),
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.CENTER,
                        backgroundColor: Colors.black,
                        textColor: Colors.white,
                        fontSize: 16.0),
                    print("erreur : " + error.toString())
                  }),
        });
  }

  deleteMessage(int id, int i) {
    getToken().then((token) => {
          EasyLoading.instance
            ..loadingStyle = EasyLoadingStyle.custom
            ..backgroundColor = Colors.white
            ..progressColor = Colors.white
            ..indicatorType = EasyLoadingIndicatorType.wave
            ..indicatorColor = Color(0xff3bc0ae)
            ..textColor = Color(0xff3bc0ae)
            ..userInteractions = false
            ..maskType = EasyLoadingMaskType.black,
          EasyLoading.show(
            status: AppLocalizations.of(context).translate('loading'),
          ),
          data = ClientApi.deleteMessage(token, id),
          data
              .then((value) async => {
                    EasyLoading.dismiss(),
                    EasyLoading.showSuccess(value.data),
                    setState(() {
                      listMessages[i].visible = false;
                    }),
                    await Future.delayed(const Duration(milliseconds: 500)),
                    setState(() {
                      listMessages.removeWhere((item) => item.id == id);
                    }),
                  })
              .catchError((error) => {
                    EasyLoading.dismiss(),
                    print("erreur : " + error.toString())
                  }),
        });
  }

  cancelMessage() {
    _init();
    if (_timer != null) {
      _timer.cancel();
    }
  }

  _init() async {
    timer = "";

    start = 60;

    try {
      if (await FlutterAudioRecorder.hasPermissions) {
        String customPath = '/flutter_audio_recorder_';
        io.Directory appDocDirectory;
//        io.Directory appDocDirectory = await getApplicationDocumentsDirectory();
        if (io.Platform.isIOS) {
          appDocDirectory = await getApplicationDocumentsDirectory();
        } else {
          appDocDirectory = await getExternalStorageDirectory();
        }

        // can add extension like ".mp4" ".wav" ".m4a" ".aac"
        customPath = appDocDirectory.path +
            customPath +
            DateTime.now().millisecondsSinceEpoch.toString();

        // .wav <---> AudioFormat.WAV
        // .mp4 .m4a .aac <---> AudioFormat.AAC
        // AudioFormat is optional, if given value, will overwrite path extension when there is conflicts.
        _recorder =
            FlutterAudioRecorder(customPath, audioFormat: AudioFormat.WAV);

        await _recorder.initialized;
        // after initialization
        var current = await _recorder.current(channel: 0);
        print(current);
        // should be "Initialized", if all working fine
        setState(() {
          _current = current;
          _currentStatus = current.status;
        });
      } else {
        Scaffold.of(context).showSnackBar(
            new SnackBar(content: new Text("You must accept permissions")));
      }
    } catch (e) {
      print(e);
    }
  }

  _start() async {
    try {
      startTimer(60);
      await _recorder.start();
      var recording = await _recorder.current(channel: 0);
      setState(() {
        _current = recording;
      });

      const tick = const Duration(seconds: 1);
      new Timer.periodic(tick, (Timer t) async {
        if (_currentStatus == RecordingStatus.Stopped) {
          t.cancel();
        }

        var current = await _recorder.current(channel: 0);
        // print(current.status);
        setState(() {
          _current = current;
          _currentStatus = _current.status;
          recorder = true;
          timer = _current?.duration.inMinutes.remainder(60).toString() +
              ":" +
              current?.duration.inSeconds.remainder(60).toString();
        });
      });
    } catch (e) {
      print(e);
    }
  }

  _stop() async {
    var result = await _recorder.stop();
    print("Stop recording: ${result.path}");
    print("Stop recording: ${result.duration}");
    file = widget.localFileSystem.file(result.path);
    print("File length: ${await file.length()}");
    List<int> imageBytes = file.readAsBytesSync();
    getToken().then((cleapi) {
      sendQucikMessage(cleapi, base64Encode(imageBytes), entete.idReceivers,
          duree, entete.id_conv.toString());
    });
    _timer.cancel();

    setState(() {
      _current = result;
      _currentStatus = _current.status;
      recorder = false;
      duree = _current.duration.inMinutes.remainder(60).toString() +
          ":" +
          _current.duration.inSeconds.remainder(60).toString();
      timer = "";
    });
  }

  String getDate(DateTime date) {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final yesterday = DateTime(now.year, now.month, now.day - 1);
    String finaldate;
    final aDate = DateTime(date.year, date.month, date.day);
    if (aDate == today) {
      finaldate = DateFormat("h:mm a").format(date.toLocal());
    } else if (aDate == yesterday) {
      finaldate = AppLocalizations.of(context).translate('yesterday');
    } else {
      finaldate = DateFormat("EEE, dd MMM").format(date.toLocal());
    }
    return finaldate;
  }

  socketMessage() async {
    await mercure.subscribe((event) {
      print("socket :" + event.data);
      var data = Map<String, dynamic>.from(json.decode(event.data));

      if (widget.id == data["conversation"]["id"]) {
        DetailMessages detailMessages = new DetailMessages(
            id: data["message"]["id"],
            sender: data["message"]["sender"],
            voice: data["message"]["voice"],
            current_user:
                data["message"]["sender_id"].toString().contains(user_id),
            date: data["message"]["date"]["date"],
            sender_photo: data["message"]["sender_photo"],
            duree: data["message"]["duree"],
            selected: false,
            type: data["message"]["type"],
            message: data["message"]["message"],
            missed_call: data["message"]["missed_call"],
            vuPar: data["message"]["VuPar"]);

        setState(() {
          listMessages.add(detailMessages);
        });
        WidgetsBinding.instance.addPostFrameCallback((_) {
          if (_scrollController.hasClients) {
            _scrollController.animateTo(
              _scrollController.position.maxScrollExtent,
              duration: Duration(milliseconds: 300),
              curve: Curves.fastOutSlowIn,
            );
          }
        });
      }
    });

    /* mercure.subscribeTopics(
        topics: <String>[Endpoints.topic_message, Endpoints.topic_quick_message,Endpoints.topic_instant_message],
        onError:(e){
            print(e);
        },
        
        onData: (Event event) {
          print("sokcet: "+event.data);
          var data = json.decode(event.data);
          print(data["message"]["voice"]);
          if (widget.id == data["conversation"]["id"]) {
            print("aaaaddddddddd");
            DetailMessages detailMessages = new DetailMessages(
                id:  data["message"]["id"],
                sender: data["message"]["sender"],
                voice: data["message"]["voice"],
                current_user: data["message"]["sender_id"].toString().contains(user_id),
                date: data["message"]["date"],
                sender_photo: data["message"]["sender_photo"],
                duree: data["message"]["duree"],
                selected:false,
                type:data["message"]["type"],
                message:data["message"]["message"],
                missed_call:data["message"]["missed_call"],
                vuPar: data["message"]["VuPar"]);
                  
            setState(() {
              listMessages.add(detailMessages);
            });
             WidgetsBinding.instance
          .addPostFrameCallback((_){
        if (_scrollController.hasClients) {
           _scrollController.animateTo(
  _scrollController.position.maxScrollExtent,
  duration: Duration(milliseconds: 300),
  curve: Curves.fastOutSlowIn,
);        }
      });
 

          }
        });*/
  }

  getUserID(String token) {
    // here you "register" to get "notifications" when the getEmail method is done.
    data = ClientApi.getUserID(token);
    data
        .then((value) => {
              setState(() {
                user_id = value.data;
              })
            })
        .catchError((error) => {print("erreur : " + error.toString())});
  }
}
