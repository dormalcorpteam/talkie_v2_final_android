import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/Chaine.dart';
import 'package:voicechanger/data/network/Model/SearchUser.dart';

class ExplorerSearchActivePage extends StatefulWidget {
  ExplorerSearchActive createState() => ExplorerSearchActive();
}

class ExplorerSearchActive extends State<ExplorerSearchActivePage> {
  TextEditingController _searchQueryController = TextEditingController();
  String searchQuery = "";
  int currentPage = 1;
  int maxPage;
  var listUser = new List<UserChaine>();
  Future<SearchUser> searchUser;
  ScrollController _scrollController;

  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent &&
          currentPage < maxPage) {
        _getMoreData();
      }
    });
  }

  _getMoreData() {
    currentPage++;
    getToken().then((cleapi) {
      searchUser = ClientApi.searchUser(cleapi, searchQuery, currentPage);
      searchUser
          .then((value) => {
                setState(() {
                  listUser = listUser + value.data;
                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
                                          backgroundColor: Color(0xff25a996),
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        title: _buildSearchField(),
        actions: <Widget>[
          FlatButton(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              padding: EdgeInsets.zero,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                AppLocalizations.of(context).translate('cancel'),
                style: TextStyle(
                  fontFamily: 'SFProText',
                  color: Color(0xffffffff),
                  fontSize: 17,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: -0.4099999964237213,
                ),
              ))
        ],
      ),
      body: new Container(
          margin: EdgeInsets.fromLTRB(8, 15, 8, 0),
          child: GridView.builder(
            shrinkWrap: true,
            controller: _scrollController,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              // number of items per row
              crossAxisCount: 3,
              // vertical spacing between the items
              mainAxisSpacing: 30.0,
              // horizontal spacing between the items
              crossAxisSpacing: 15.0,
              childAspectRatio: 1.0,
            ),
            itemBuilder: (BuildContext context, int i) {
              if (listUser.isNotEmpty) {
                return (i >= listUser.length)
                    ? Center(child: CupertinoActivityIndicator())
                    : UserWidget(listUser[i]);
              }
              return null;
            },
            itemCount:
                currentPage == maxPage ? listUser.length : listUser.length + 1,
          )),
    );
  }

  UserWidget(UserChaine user) {
    return GestureDetector(
        onTap: () {
                                getID().then((id) {
                                                 
                                               
                      if(user.id!=id)

          Navigator.pushNamed(context, "/chainePage",
              arguments: {'id': user.id, 'username': user.name,}).then((value) => { 
                updateSearchQuery(searchQuery)
           
});
              });
        },
        child: Column(children: [
          if (user.photo != "")
            Container(
              width: 52,
              height: 52,
              decoration: new BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Color(0x33000000),
                        offset: Offset(0, 0),
                        blurRadius: 5,
                        spreadRadius: 0)
                  ],
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(Endpoints.baseUrlImage + user.photo),
                  ),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8)),
            ),
          SizedBox(
            height: 4,
          ),
          Text(user.name,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xff3f3c3c),
                fontSize: 13,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0.2708333333333333,
              )),
          SizedBox(
            height: 1,
          ),
          if (user.subscriber == 1)
            Text(AppLocalizations.of(context).translate('follower'),
                style: TextStyle(
                  fontFamily: 'Ubuntu',
                  color: Color(0xff949494),
                  fontSize: 13,
                  fontWeight: FontWeight.w300,
                  fontStyle: FontStyle.italic,
                  letterSpacing: 0.2708333333333333,
                ))
        ]));
  }

  Widget _buildSearchField() {
    return Container(
        margin: EdgeInsets.only(left: 20),
        height: 43,
        child: TextField(
          controller: _searchQueryController,
          autofocus: true,
          decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              hintStyle: TextStyle(
                fontFamily: 'SFProText',
                color: Color(0x7f9b9b9b),
                fontSize: 17,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: -0.408,
              ),
              hintText: AppLocalizations.of(context).translate('search'),
              contentPadding:
                  new EdgeInsets.symmetric(vertical: 10, horizontal: 20.0),
              prefixIcon: IconButton(
                  onPressed: null,
                  icon: SvgPicture.asset(
                    "assets/icon/search1.svg",
                  )),
              suffixIcon: IconButton(
                  onPressed: () {
                    _clearSearchQuery();
                  },
                  icon: SvgPicture.asset(
                    "assets/icon/cross.svg",
                  )),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(width: 1, color: Color(0x129b9b9b)),
              ),
              disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(width: 1, color: Color(0x129b9b9b)),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(width: 1, color: Color(0x129b9b9b)),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(width: 1, color: Color(0x129b9b9b)),
              )),
          style: TextStyle(
            fontFamily: 'SFProText',
            color: Color(0xff000000),
            fontSize: 17,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
            letterSpacing: -0.408,
          ),
          onChanged: (query) => updateSearchQuery(query),
        ));
  }



  void updateSearchQuery(String newQuery) {
    setState(() {
      searchQuery = newQuery;
      currentPage=1;
    });
    if (newQuery != ""){filterSearchResults(newQuery, currentPage);} else{
     setState(() {
     listUser = [];
    });
    }
  }

  void _clearSearchQuery() {
    setState(() {
      WidgetsBinding.instance
          .addPostFrameCallback((_) => _searchQueryController.clear());
      updateSearchQuery("");
      listUser = [];
    });
  }

  filterSearchResults(String value, int page) {
    getToken().then((cleapi) {
      searchUser = ClientApi.searchUser(cleapi, value, page);
      searchUser
          .then((value) => {
            if(value.data.isEmpty){
              EasyLoading.instance.loadingStyle =
                                  EasyLoadingStyle.custom,
                              EasyLoading.instance.backgroundColor = Colors.red,
                              EasyLoading.instance.indicatorColor =
                                  Colors.white,
                              EasyLoading.instance.progressColor = Colors.white,
                              EasyLoading.instance.textColor = Colors.white,
                              EasyLoading.instance.displayDuration =
                                  const Duration(milliseconds: 2000),
                              EasyLoading.showError(AppLocalizations.of(context).translate('no_user_found')),
            },
                setState(() {
                  listUser = value.data;
                  maxPage = value.pagination.pages;
                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }
}

Future<String> getToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("apikey");
}
  Future<int> getID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt("id");
  }