import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/Page/AppBarHeader.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/message.dart';

class NouvelleDiscussionPage extends StatefulWidget {
  NouvelleDiscussion createState() => NouvelleDiscussion();
}

class NouvelleDiscussion extends State<NouvelleDiscussionPage> {
  int currentPage = 1;
  int maxPage;
  ScrollController _scrollController;
  var listMessages = new List<Messages>();
  Future<Messagee> messages;
  var _key = GlobalKey();

  @override
  void initState() {
    super.initState();
    getToken().then((cleapi) {
      // here you "register" to get "notifications" when the getEmail method is done.
      _getMessages(cleapi, currentPage);
    });
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent &&
          currentPage < maxPage) {
        _getMoreData();
      }
    });
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  filterSearchResults(String value) {
    getToken().then((cleapi) {
      messages = ClientApi.searchMessage(cleapi, value);
      messages
          .then((value) => {
                setState(() {
                  listMessages = value.data;
                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }

  _getMoreData() {
    currentPage++;
    getToken().then((cleapi) {
      messages = ClientApi.getListemessages(cleapi, currentPage);
      messages
          .then((value) => {
                EasyLoading.dismiss(),
                setState(() {
                  listMessages = listMessages + value.data;
                })
              })
          .catchError((error) =>
              {EasyLoading.dismiss(), print("erreur : " + error.toString())});
    });
  }

  messageDetail(int id, String nom) {
    Navigator.pushNamed(context, "/detailmessage",
        arguments: {'id': id, 'nom': nom});
  }

  MessageWidget(Messages message) {
    print(message.photo);
    return GestureDetector(
        onTap: () {
          messageDetail(message.id, message.nom);
        },
        child: Column(children: [
          new Stack(overflow: Overflow.visible, children: <Widget>[
            Container(
              width: 52,
              height: 52,
              decoration: new BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Color(0x33000000),
                        offset: Offset(0, 0),
                        blurRadius: 5,
                        spreadRadius: 0)
                  ],
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(Endpoints.baseUrlImage + message.photo),
                  ),
                  color: Color(0xffffffff),
                  borderRadius: BorderRadius.circular(8)),
            ),
          ]),
          SizedBox(height: 4),
          Text(message.nom,
              textAlign: TextAlign.center,
              style: (message.notif)
                  ? TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xff3f3c3c),
                      fontSize: 13,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.07083300000000001,
                    )
                  : TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xff3f3c3c),
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.2708333333333333,
                    )),
          SizedBox(height: 2),
          if (message.date != "")
            Text(getDate(DateTime.parse(message.date)),
                style: (message.notif)
                    ? TextStyle(
                        fontFamily: 'Ubuntu',
                        color: Color(0xff3f3c3c),
                        fontSize: 13,
                        fontWeight: FontWeight.w300,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0.2708333333333333,
                      )
                    : TextStyle(
                        fontFamily: 'Ubuntu',
                        color: Color(0xff949494),
                        fontSize: 13,
                        fontWeight: FontWeight.w300,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0.2708333333333333,
                      ))
        ]));
  }

  String getDate(DateTime date) {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final yesterday = DateTime(now.year, now.month, now.day - 1);
    String finaldate;
    final aDate = DateTime(date.year, date.month, date.day);
    if (aDate == today) {
      finaldate = DateFormat("h:mm a").format(date.toLocal());
    } else if (aDate == yesterday) {
      finaldate = AppLocalizations.of(context).translate('yesterday');
    } else {
      finaldate = DateFormat("EEE, dd MMM").format(date.toLocal());
    }
    return finaldate;
  }

  _getMessages(String token, int page) {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: 'Loading...',
    );
    // here you "register" to get "notifications" when the getEmail method is done.
    messages = ClientApi.getListemessages(token, page);
    messages
        .then((value) => {
              EasyLoading.dismiss(),
              setState(() {
                print(value.data);
                listMessages = value.data;
                currentPage=1;
                maxPage = value.pagination.pages;
              })
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarHeader(
            name: AppLocalizations.of(context).translate('new_discussion'),
            buttonback: true,
            actionTalk: false,
            actionmessage: false),
        resizeToAvoidBottomInset: false,
        body:  Column(children: [
                Padding(
                    padding: EdgeInsets.fromLTRB(8, 15, 8, 0),
                    child: Theme(
                        data: Theme.of(context)
                            .copyWith(splashColor: Colors.transparent),
                        child: Container(
                          height: 36,
                          child: TextField(
                              onChanged: (query) => {
                                    if (query != "")
                                      filterSearchResults(query)
                                    else
                                      {
                                        getToken().then((cleapi) {
                                          _getMessages(cleapi, 1);
                                        })
                                      }
                                  },
                              style: TextStyle(
                                fontFamily: 'SFProText',
                                color: Color(0x7f9b9b9b),
                                fontSize: 17,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: -0.408,
                              ),
                              decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Color(0x129b9b9b),
                                  hintStyle: TextStyle(
                                    fontFamily: 'SFProText',
                                    color: Color(0x7f9b9b9b),
                                    fontSize: 17,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: -0.408,
                                  ),
                                  hintText: AppLocalizations.of(context).translate('search'),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 15.0),
                                  prefixIcon: Icon(Icons.search,
                                      color: Color(0xff949494)),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1, color: Color(0x129b9b9b)),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1, color: Color(0x129b9b9b)),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1, color: Color(0x129b9b9b)),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1, color: Color(0x129b9b9b)),
                                  ))),
                        ))),
                FlatButton(
                    onPressed: () {
                      Navigator.pushNamed(context, "/creatingroup1");
                    },
                    child: Text(AppLocalizations.of(context).translate('new_group'),
                        style: TextStyle(
                          fontFamily: 'Ubuntu',
                          color: Color(0xff25a996),
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0.2708333333333333,
                        )) //Text
                    ),
              
          Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 13, 50, 0),
                  child: Text(AppLocalizations.of(context).translate('latest_recipients'),
                      style: TextStyle(
                        fontFamily: 'Salome',
                        color: Color(0xff3f3c3c),
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0.4166666666666666,
                      )))),
          Expanded(child: Padding(
              padding: EdgeInsets.only(top: 15),
              child: GridView.builder(
                key: _key,
                shrinkWrap: true,
                controller: _scrollController,
                physics: const AlwaysScrollableScrollPhysics(),  
                padding: const EdgeInsets.all(4.0),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  // number of items per row
                  crossAxisCount: 3,
                  // vertical spacing between the items
                  mainAxisSpacing: 10.0,
                  // horizontal spacing between the items
                  crossAxisSpacing: 4.0,
                  childAspectRatio: 1.0,
                ),
                itemBuilder: (BuildContext context, int i) {
                  if (listMessages.isNotEmpty) {
                    return (i >= listMessages.length)
                        ? Center(child: CupertinoActivityIndicator())
                        : MessageWidget(listMessages[i]);
                  }
                  return null ;

                },
                itemCount: currentPage == maxPage
                    ? listMessages.length
                    : listMessages.length + 1,
              ))),
          //FlatButton
        ]));
  }
}
