import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/Page/creationGroupePage1.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/DetailGroupe.dart';

class InfoGroupePage extends StatefulWidget {
  InfoGroupe createState() => InfoGroupe();
}

class InfoGroupe extends State<InfoGroupePage> {
  List<Participant> selectedAmis = new List<Participant>();
  Future<DetailGroupe> detailGroup;
  InfosGroupe infoGroup;
  int idGroupe;

  @override
  void initState() {
    super.initState();
    getToken().then((token) {
      getInfosGroupe(token, idGroupe);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Map arguments;
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    if (arguments != null) {
      print(arguments['id']);
      idGroupe = arguments['id'];
    }

    return Scaffold(
        backgroundColor: Color(0xfff8f9f9),
        appBar: AppBar(
        backgroundColor: Color(0xff25a996),
          elevation: 0,
          centerTitle: true,
          title: Text(AppLocalizations.of(context).translate('group_info'),
              style: TextStyle(
                fontFamily: 'Roboto',
                color: Color(0xffffffff),
                fontSize: 17,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
              )),
          actions: <Widget>[],
        ),
        resizeToAvoidBottomInset: false,
        body: Column(children: [
          if (infoGroup != null)
            Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 18),
                width: 100,
                height: 100,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0x25000000),
                          offset: Offset(0, 5),
                          blurRadius: 15,
                          spreadRadius: 0)
                    ],
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                            Endpoints.baseUrlImage + infoGroup.photo)))),
          SizedBox(
            height: 14,
          ),
          if (infoGroup != null)
            Center(
                child: Text(infoGroup.nom,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xff3f3c3c),
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.39,
                    ))),
          SizedBox(
            height: 3,
          ),
          if (infoGroup != null)
            Center(
                child: Text("${infoGroup.nbrParticipant} "+AppLocalizations.of(context).translate('members'),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xff3f3c3c),
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.2708333333333333,
                    ))),
          if (infoGroup != null)
            new Container(
                margin: EdgeInsets.fromLTRB(8, 27, 8, 0),
                child: GridView.builder(
                  itemCount: selectedAmis.length + 1,
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    // number of items per row
                    crossAxisCount: 3,
                    // vertical spacing between the items
                    mainAxisSpacing: 30.0,
                    // horizontal spacing between the items
                    crossAxisSpacing: 15.0,
                    childAspectRatio: 1.0,
                  ),
                  itemBuilder: (BuildContext context, int i) {
                    if (i == selectedAmis.length)
                      return GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () async {
                            int id = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    settings: RouteSettings(arguments: {
                                      'infoGroup': infoGroup,
                                    }),
                                    builder: (_) => CreationGroupePage1()));
                            getToken().then((token) {
                              getInfosGroupe(token, id);
                            });
                          },
                          child: Column(children: [
                            Container(
                                width: 52,
                                height: 52,
                                decoration: new BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x33000000),
                                          offset: Offset(0, 0),
                                          blurRadius: 5,
                                          spreadRadius: 0)
                                    ],
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(8)),
                                child: DottedBorder(
                                  radius: Radius.circular(8),
                                  dashPattern: [4],
                                  strokeWidth: 1,
                                  strokeCap: StrokeCap.round,
                                  borderType: BorderType.RRect,
                                  color: Color(0xffc6c6c6),
                                  child: Padding(
                                      padding: EdgeInsets.all(10),
                                      child: SvgPicture.asset(
                                          ("assets/icon/add-user.svg"))),
                                )),
                            SizedBox(
                              height: 4,
                            ),
                            Text(AppLocalizations.of(context).translate('add'),
                                style: TextStyle(
                                  fontFamily: 'Ubuntu',
                                  color: Color(0xff3f3c3c),
                                  fontSize: 13,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0.2708333333333333,
                                ))
                          ]));
                    else
                      return UserWidget(selectedAmis[i]);
                  },
                ))
        ]));
  }

  getInfosGroupe(String token, int id) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    detailGroup = ClientApi.getInfoGroup(token, id);

    detailGroup
        .then((value) => {
              EasyLoading.dismiss(),
              setState(() {
                infoGroup = value.data;
                selectedAmis = value.data.participant;
              })
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  UserWidget(Participant user) {
    return Column(children: [
      if (user != null)
        Container(
          width: 52,
          height: 52,
          decoration: new BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: Color(0x33000000),
                    offset: Offset(0, 0),
                    blurRadius: 5,
                    spreadRadius: 0)
              ],
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(Endpoints.baseUrlImage + user.photo),
              ),
              color: Color(0xffffffff),
              borderRadius: BorderRadius.circular(8)),
        ),
      SizedBox(
        height: 4,
      ),
      if (user != null)
        Text(user.nom,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Ubuntu',
              color: Color(0xff3f3c3c),
              fontSize: 13,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              letterSpacing: 0.2708333333333333,
            )),
    ]);
  }
}
