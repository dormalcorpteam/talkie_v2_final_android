import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/DetailGroupe.dart';
import 'package:voicechanger/data/network/Model/ListeAmis.dart';
import 'package:voicechanger/data/network/Model/dataGroupe.dart';
import 'package:voicechanger/data/network/Model/user.dart';

class CreationGroupePage1 extends StatefulWidget {
  CreationGroupe1 createState() => CreationGroupe1();
}

class CreationGroupe1 extends State<CreationGroupePage1> {
  @override
  Widget build(BuildContext context) {
    Map arguments;
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    return CreationGroupe(arguments: arguments);
  }
}

class CreationGroupe extends StatefulWidget {
  final Map arguments;
  CreationGroupe({Key key, this.arguments});

  @override
  _CreationGroupeState createState() => _CreationGroupeState();
}

class _CreationGroupeState extends State<CreationGroupe> {
  Future<ListeAmis> listeAmis;
  var listamis = new List<UserData>();
  List<UserData> selectedAmis = new List<UserData>();
  InfosGroupe infoGroup;
  Future<DataGroupe> data;

  @override
  void initState() {
    super.initState();

    getListFriends();
    if (widget.arguments != null) {
      print(widget.arguments['infoGroup']);
      infoGroup = widget.arguments['infoGroup'];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff8f9f9),
        appBar: (widget.arguments != null)
            ? AppBar(
                elevation: 0,
                centerTitle: true,
                        backgroundColor: Color(0xff25a996),
                title: Text(AppLocalizations.of(context).translate('edit_group'),
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      color: Color(0xffffffff),
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    )),
                actions: <Widget>[
                  FlatButton(
                      child: Text(AppLocalizations.of(context).translate('edit'),
                          style: TextStyle(
                            fontFamily: 'SFCompactText',
                            color: Color(0xffffffff),
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          )),
                      onPressed: () {
                        getToken().then((token) => {
                              print("aaaa" + selectedAmis.length.toString()),
                              editGroupe(
                                  token, "", "", selectedAmis, infoGroup.id)
                            });
                      }),
                ],
              )
            : AppBar(
                elevation: 0,
                centerTitle: true,
                        backgroundColor: Color(0xff25a996),

                title: Text(AppLocalizations.of(context).translate('create_group'),
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      color: Color(0xffffffff),
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    )),
                actions: <Widget>[
                  FlatButton(
                      child: Text(AppLocalizations.of(context).translate('next'),
                          style: TextStyle(
                            fontFamily: 'SFCompactText',
                            color: Color(0xffffffff),
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          )),
                      onPressed: () async {
                        print("aaaa" + selectedAmis.length.toString());
                       dynamic result = await  Navigator.pushNamed(context, "/creatingroup2",
                            arguments: {
                              'selectedAmis': selectedAmis,
                            });
                            await getListFriends();
                            setState(() {
                              selectedAmis=result;
                            });
                            
                      }),
                ],
              ),
        resizeToAvoidBottomInset: false,
        body: Column(children: [
          Padding(
              padding: EdgeInsets.fromLTRB(8, 15, 8, 0),
              child: Theme(
                  data: Theme.of(context)
                      .copyWith(splashColor: Colors.transparent),
                  child: Container(
                    height: 36,
                    child: TextField(
                        onChanged: (query) => {
                              if (query != "")
                                {
                                  filterSearchResults(query),
                                }
                              else
                                {
                                  getListFriends(),
                                }
                            },
                        style: TextStyle(
                          fontFamily: 'SFProText',
                          color: Color(0x7f9b9b9b),
                          fontSize: 17,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: -0.408,
                        ),
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Color(0x129b9b9b),
                            hintStyle: TextStyle(
                              fontFamily: 'SFProText',
                              color: Color(0x7f9b9b9b),
                              fontSize: 17,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: -0.408,
                            ),
                            hintText: AppLocalizations.of(context).translate('search'),
                            contentPadding: new EdgeInsets.symmetric(
                                vertical: 0, horizontal: 15.0),
                            prefixIcon:
                                Icon(Icons.search, color: Color(0xff949494)),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                  width: 1, color: Color(0x129b9b9b)),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                  width: 1, color: Color(0x129b9b9b)),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                  width: 1, color: Color(0x129b9b9b)),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                  width: 1, color: Color(0x129b9b9b)),
                            ))),
                  ))),
          SizedBox(height: 10),
          new Expanded(
              child: GridView.builder(
            itemCount: listamis.length,
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              // number of items per row
              crossAxisCount: 3,
              // vertical spacing between the items
              mainAxisSpacing: 30.0,
              // horizontal spacing between the items
              crossAxisSpacing: 15.0,
              childAspectRatio: 1.0,
            ),
            itemBuilder: (BuildContext context, int i) {
              return Padding(
                  padding: EdgeInsets.only(top: 5),
                  child: UserWidget(listamis[i]));
            },
          ))
        ]));
  }

  filterSearchResults(String value) {
    getToken().then((cleapi) {
      listeAmis = ClientApi.searchFriends(cleapi, value);
      listeAmis
          .then((value) => {
                setState(() {
                  listamis = value.data;
                  for (int i = 0; i < listamis.length; i++) {
                    for (int j = 0; j < selectedAmis.length; j++) {
                      if (listamis[i].id == selectedAmis[j].id) {
                        listamis[i].value = true;
                      }
                    }
                  }
                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }

  getListFriends() {
    getToken().then((cleapi) {
      listeAmis = ClientApi.listFriends(cleapi);
      listeAmis
          .then((value) => {
                print(value.data),
                setState(() {
                  listamis = value.data;
                  for (int i = 0; i < listamis.length; i++) {
                    for (int j = 0; j < selectedAmis.length; j++) {
                      if (listamis[i].id == selectedAmis[j].id) {
                        listamis[i].value = true;
                      }
                    }
                  }
                  if (widget.arguments != null) {
                    for (int i = 0; i < listamis.length; i++) {
                      for (int j = 0; j < infoGroup.participant.length; j++) {
                        if (listamis[i].id == infoGroup.participant[j].id) {
                          listamis[i].value = true;
                          selectedAmis.add(UserData(
                              id: infoGroup.participant[j].id,
                              nom: infoGroup.participant[j].nom.split("")[1],
                              prenom: infoGroup.participant[j].nom.split("")[0],
                              photo: infoGroup.participant[j].photo));
                          print(infoGroup.participant[j].nom.split(" ")[0]);
                          print(infoGroup.participant[j].nom.split(" ")[1]);
                        }
                      }
                    }
                  }
                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  UserWidget(UserData user) {
    return GestureDetector(
        onTap: () {},
        child: Column(children: [
          new Stack(overflow: Overflow.visible, children: <Widget>[
            if (user.photo != "")
              GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    setState(() {
                      user.value = !user.value;
                    });
                    if (user.value) {
                      selectedAmis.add(UserData(
                          id: user.id,
                          nom: user.nom,
                          prenom: user.prenom,
                          photo: user.photo));
                    } else {
                      selectedAmis.removeWhere((item) => item.id == user.id);
                    }
                  },
                  child: Container(
                    width: 52,
                    height: 52,
                    decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x33000000),
                              offset: Offset(0, 0),
                              blurRadius: 5,
                              spreadRadius: 0)
                        ],
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image:
                              NetworkImage(Endpoints.baseUrlImage + user.photo),
                        ),
                        color: Color(0xffffffff),
                        borderRadius: BorderRadius.circular(8)),
                  )),
            new Positioned(
                top: -20.0,
                right: -20.0,
                child: new Stack(
                  children: <Widget>[
                    Center(
                        child: Theme(
                            data: ThemeData(
                                unselectedWidgetColor: Color(0xff949494)),
                            child: Checkbox(
                              activeColor: Color(0xff25a996),
                              value: user.value,
                              onChanged: (bool value) => {},
                            ))),
                  ],
                ))
          ]),
          SizedBox(
            height: 4,
          ),
          Text(user.nom + " " + user.prenom,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xff3f3c3c),
                fontSize: 13,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0.2708333333333333,
              )),
        ]));
  }

  editGroupe(
      String token, String name, String photo, var list, int idGroupe) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    data = ClientApi.editgroupe(token, name, photo, list, idGroupe);
    data
        .then((value) => {
              EasyLoading.dismiss(),
              EasyLoading.showSuccess(AppLocalizations.of(context).translate('group_edit')),
              Navigator.pop(context, idGroupe),
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }
}
