import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/Chaine.dart';
import 'package:voicechanger/data/network/Model/SearchUser.dart';

import 'AppBarHeader.dart';
import 'Footer.dart';
import 'discussionPage.dart';
import 'myWorldPage.dart';

class ListLikeDislikePage extends StatefulWidget {
  ListLikeDislike createState() => ListLikeDislike();
}

class ListLikeDislike extends State<ListLikeDislikePage> {
  @override
  Widget build(BuildContext context) {
    Map arguments;
    String nom;
    int id;
    int page;
    int userId;
    int userC;
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    if (arguments != null) {
        nom=arguments['nom'];
        id=arguments['id'];
        page=arguments['page'];
         userId=arguments['userId'];
         userC=arguments['user_c'];
        print(nom);
        print(id);
      print(page);
    }
    return TalkersListeners(nom: nom, id: id,page:page,userId:userId,userC:userC);
  }
}

class TalkersListeners extends StatefulWidget {
  final String nom;
  final int id;
  final int page;
  final int userId;
  final int userC;

  TalkersListeners({Key key, this.id, this.nom,this.page,this.userId,this.userC});

  @override
  _TalkersListenersPageState createState() => _TalkersListenersPageState();
}

class _TalkersListenersPageState extends State<TalkersListeners>   with SingleTickerProviderStateMixin {
  
  Map arguments;
  int _selectedIndex = 1;
  Widget appBar;
 List<UserChaine>  listLike = [];
  Future<SearchUser> getlistLike;
    int   nbr_like;
    int nbr_dislike;
  int _selectedIndexTab= 0;
TabController tabController;
bool show_like=false;bool show_dislike=false;
 List<UserChaine>  listDislike = [];
  Future<SearchUser> getlistDislike;

List<UserChaine> _searchResultLikers = [];
List<UserChaine> _searchResultDislikers = [];
  TextEditingController controllerliker = new TextEditingController();
TextEditingController controllerdisliker = new TextEditingController();
  @override
  void initState() {
    super.initState();
        appBar = AppBarHeader(
        name: widget.nom,
        buttonback: true,
        actionTalk: false,
        actionmessage: false);
         
   getListLikeAPI(widget.id);
 getListDislikeAPI(widget.id);
     
       

        
  }
 
  List<Widget> _widgetOptions;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;

      if (index == 0) {
        appBar = AppBarHeader(
            name: "Discussions",
            buttonback: false,
            actionTalk: false,
            actionmessage: true);
      }

      if (index == 1) {
        appBar = AppBarHeader(
            name: widget.nom,
            buttonback: true,
            actionTalk: false,
            actionmessage: false);
      }
      if (index == 2) {
        appBar = null;
      }
    });
    
  }


  @override
  Widget build(BuildContext context) {
  

       _widgetOptions = <Widget>[
      DiscussionPage(),
      DefaultTabController(
                initialIndex:widget.page,
            
                
      
        length: 2,
        child: Column(
          children: <Widget>[
            
            Container(
                            constraints: BoxConstraints.expand(height: 52),

              child: TabBar(
          
                labelColor:  Color(0xff25a996) ,
                unselectedLabelColor: Color(0xff9b9b9b),
              indicatorColor:Color(0xff25a996),
              labelStyle:TextStyle(
    fontFamily: 'Helvetica',
    color: Color(0xff25a996),
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 0.4,
    
    ),  //For Selected tab
              unselectedLabelStyle: TextStyle(
    fontFamily: 'Helvetica',
    color: Color(0xff9b9b9b),
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 0.4,
    
    ), //For Un-selected Tabs

                tabs: [
                Tab(child: Row( mainAxisAlignment: MainAxisAlignment.center ,crossAxisAlignment: CrossAxisAlignment.center,children: [Text(nbr_like!=null ?nbr_like.toString() :""),SizedBox(width: 10),SvgPicture.asset(("assets/icon/like.svg"))])),
                 Tab(child: Row( mainAxisAlignment: MainAxisAlignment.center ,crossAxisAlignment: CrossAxisAlignment.center,children: [SvgPicture.asset(("assets/icon/dislike.svg")),SizedBox(width: 10),Text(nbr_dislike!=null ?nbr_dislike.toString():"")])),
              ]),
            ),
            

            Expanded(
              child: Container(
                child: TabBarView(children: [
                     (listLike.isEmpty &&show_like)
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Center(
                      child:   Text( (widget.userC==widget.userId)?AppLocalizations.of(context).translate('empty_like'):AppLocalizations.of(context).translate('no_like'),
                          style: TextStyle(
                            fontFamily: 'Ubuntu',
                            color: Color(0xff9b9b9b),
                            fontSize: 21,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ))),
                ],
              )
            :
                  Column(
                    children: <Widget>[
                       Container(
                              height: 36,margin: EdgeInsets.fromLTRB(8, 20, 8, 0),child:TextField(
                                                    controller: controllerliker,

                                                          onChanged: onSearchLikersChanged,

                                      style: TextStyle(
                                        fontFamily: 'SFProText',
                                        color: Color(0x7f9b9b9b),
                                        fontSize: 17,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: -0.408,
                                      ),
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Color(0x129b9b9b),
                                          hintStyle: TextStyle(
                                            fontFamily: 'SFProText',
                                            color: Color(0x7f9b9b9b),
                                            fontSize: 17,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: -0.408,
                                          ),
                                          hintText: AppLocalizations.of(context).translate('search'),
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 0,
                                                  horizontal: 15.0),
                                          prefixIcon: Icon(Icons.search,
                                              color: Color(0xff949494)),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          disabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          )))),
                     
                     
                     
              _searchResultLikers.length != 0 || controllerliker.text.isNotEmpty
                ?    Expanded(child:Container(
                                  margin: EdgeInsets.fromLTRB(8, 15, 8, 0),

                        child:
           GridView.builder(
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              // number of items per row
              crossAxisCount: 3,
              // vertical spacing between the items
              mainAxisSpacing: 30.0,
              // horizontal spacing between the items
              crossAxisSpacing: 15.0,
              childAspectRatio: 1.0,
            ),
            itemBuilder: (BuildContext context, int i) {
                return 
              UserWidget(_searchResultLikers[i]);
              
            },
            itemCount:
                _searchResultLikers.length ,
          ))):
          Expanded(child:Container(
                                  margin: EdgeInsets.fromLTRB(8, 15, 8, 0),

                        child:
           GridView.builder(
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              // number of items per row
              crossAxisCount: 3,
              // vertical spacing between the items
              mainAxisSpacing: 30.0,
              // horizontal spacing between the items
              crossAxisSpacing: 15.0,
              childAspectRatio: 1.0,
            ),
            itemBuilder: (BuildContext context, int i) {
                return 
              UserWidget(listLike[i]);
              
            },
            itemCount:
                listLike.length ,
          )))
          
          ]),
                     (listDislike.isEmpty && show_dislike)
            
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Center(
                      child: Text((widget.userC==widget.userId)?AppLocalizations.of(context).translate('empty_dislike'):AppLocalizations.of(context).translate('no_dislike'),
                          style: TextStyle(
                            fontFamily: 'Ubuntu',
                            color: Color(0xff9b9b9b),
                            fontSize: 21,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ))),
                ],
              )
            :   Column(
                    children: <Widget>[
                       Container(
                              height: 36,margin: EdgeInsets.fromLTRB(8, 20, 8, 0),child:TextField(
                                controller: controllerdisliker,
                                      onChanged:onSearchDislikersChanged ,
                                      style: TextStyle(
                                        fontFamily: 'SFProText',
                                        color: Color(0x7f9b9b9b),
                                        fontSize: 17,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: -0.408,
                                      ),
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Color(0x129b9b9b),
                                          hintStyle: TextStyle(
                                            fontFamily: 'SFProText',
                                            color: Color(0x7f9b9b9b),
                                            fontSize: 17,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: -0.408,
                                          ),
                                          hintText: AppLocalizations.of(context).translate('search'),
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 0,
                                                  horizontal: 15.0),
                                          prefixIcon: Icon(Icons.search,
                                              color: Color(0xff949494)),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          disabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          )))),
               _searchResultDislikers.length != 0 || controllerdisliker.text.isNotEmpty
                ?      Expanded(child:Container(
          margin: EdgeInsets.fromLTRB(8, 15, 8, 0),
          child: GridView.builder(
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              // number of items per row
              crossAxisCount: 3,
              // vertical spacing between the items
              mainAxisSpacing: 30.0,
              // horizontal spacing between the items
              crossAxisSpacing: 15.0,
              childAspectRatio: 1.0,
            ),
            itemBuilder: (BuildContext context, int i) {
                return 
                
              
                UserWidget(_searchResultDislikers[i]);
              
            },
            itemCount:
                _searchResultDislikers.length ,
          ))):Expanded(child:Container(
          margin: EdgeInsets.fromLTRB(8, 15, 8, 0),
          child: GridView.builder(
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              // number of items per row
              crossAxisCount: 3,
              // vertical spacing between the items
              mainAxisSpacing: 30.0,
              // horizontal spacing between the items
              crossAxisSpacing: 15.0,
              childAspectRatio: 1.0,
            ),
            itemBuilder: (BuildContext context, int i) {
                return 
                
              
                UserWidget(listDislike[i]);
              
            },
            itemCount:
                listDislike.length ,
          )))]
                                )]),
              ),
            )

          ],
        ),
      ),
      MyWorldPage(),
    ];
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    return Scaffold(
        appBar: appBar,
        body: Container(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: Footer(_selectedIndex, _onItemTapped));
  }
  getListLikeAPI(int id) {
    getToken().then((cleapi) {
      getlistLike = ClientApi.getListLike(cleapi, id);
      getlistLike
          .then((value) => {
                setState(() {
                  listLike = value.data;
                  nbr_dislike=value.nbrDislike;
                  nbr_like=value.nbrLike;
                show_like=true;
                  

                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }


  onSearchLikersChanged(String text) async {
    _searchResultLikers.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    listLike.forEach((liker) {
      if (liker.name.toLowerCase().contains(text.toLowerCase()))
        _searchResultLikers.add(liker);
    });

    setState(() {});
  }

  onSearchDislikersChanged(String text) async {
    _searchResultDislikers.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    listDislike.forEach((disliker) {
      if (disliker.name.toLowerCase().contains(text.toLowerCase()))
       _searchResultDislikers.add(disliker);
    });

    setState(() {});
  }



    getListDislikeAPI(int id) {
    getToken().then((cleapi) {
      getlistDislike = ClientApi.getListDislike(cleapi, id);
      getlistDislike
          .then((value) => {
                setState(() {
                  listDislike = value.data;
                   nbr_dislike=value.nbrDislike;
                  nbr_like=value.nbrLike;
                  show_dislike=true;
                
                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }
   UserWidget(UserChaine user) {
    return GestureDetector(
        onTap: () {
                                getID().then((id) {
                                                 
                                               
                      if(user.id!=id)

          Navigator.pushNamed(context, "/chainePage",
              arguments: {'id': user.id, 'username': user.name}).then((value) =>  {  getListLikeAPI(widget.id),
 getListDislikeAPI(widget.id),}
);
              });
        },
        child: Column(children: [
          if (user.photo != "")
            Container(
              width: 52,
              height: 52,
              decoration: new BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Color(0x33000000),
                        offset: Offset(0, 0),
                        blurRadius: 5,
                        spreadRadius: 0)
                  ],
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(Endpoints.baseUrlImage + user.photo),
                  ),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8)),
            ),
          SizedBox(
            height: 4,
          ),
          Text(user.name,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xff3f3c3c),
                fontSize: 13,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0.2708333333333333,
              )),
       
        ]));
  }
 

}

Future<String> getToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("apikey");
}
  
  Future<int> getID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt("id");
  }

 