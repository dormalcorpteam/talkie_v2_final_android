import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/dataGroupe.dart';
import 'package:voicechanger/data/network/Model/user.dart';

class CreationGroupePage2 extends StatefulWidget {
  CreationGroupe2 createState() => CreationGroupe2();
}

class CreationGroupe2 extends State<CreationGroupePage2> {
  List<UserData> selectedAmis = new List<UserData>();
  TextEditingController _controller = new TextEditingController();
  Future<DataGroupe> data;

  void onValueChange() {
    setState(() {
      _controller.text;
    });
  }

  File _image;
  String _base64Image;

  @override
  void initState() {
    super.initState();
    _controller.addListener(onValueChange);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Map arguments;
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    if (arguments != null) {
      print(arguments['selectedAmis']);
      selectedAmis = arguments['selectedAmis'];
    }

    return WillPopScope(
        onWillPop: () async {
          
          
          Navigator.pop(context,selectedAmis);
          return false;
        },
        child:Scaffold(
        backgroundColor: Color(0xfff8f9f9),
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
                                            backgroundColor: Color(0xff25a996),

          title: Text(AppLocalizations.of(context).translate('create_group'),
              style: TextStyle(
                fontFamily: 'Roboto',
                color: Color(0xffffffff),
                fontSize: 17,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
              )),
          actions: <Widget>[
            FlatButton(
                child: Text(AppLocalizations.of(context).translate('create'),
                    style: TextStyle(
                      fontFamily: 'SFCompactText',
                      color: Color(0xffffffff),
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    )),
                onPressed: () {
if(selectedAmis.isEmpty || _controller.text == ''){
  EasyLoading.instance.loadingStyle =
                                  EasyLoadingStyle.custom;
                              EasyLoading.instance.backgroundColor = Colors.red;
                              EasyLoading.instance.indicatorColor =
                                  Colors.white;
                              EasyLoading.instance.progressColor = Colors.white;
                              EasyLoading.instance.textColor = Colors.white;
                              EasyLoading.instance.displayDuration =
                                  const Duration(milliseconds: 2000);
                              EasyLoading.showError(AppLocalizations.of(context).translate('empty_fields_grp'));

 }
                            else{
                  getToken().then((token) {
                    createGroupe(
                        token, _controller.text, _base64Image, selectedAmis);
                  });
                   }
                }),
          ],
        ),
        resizeToAvoidBottomInset: false,
        body: Column(children: [
          StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  final act = CupertinoActionSheet(
                      actions: <Widget>[
                        CupertinoActionSheetAction(
                          child: Text(AppLocalizations.of(context).translate('choose_photo'),
                              style: TextStyle(
                                fontFamily: 'SFProText',
                                color: Color(0xff262626),
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop();

                            getImage(ImageSource.gallery);
                          },
                        ),
                        CupertinoActionSheetAction(
                          child: Text(AppLocalizations.of(context).translate('take_picture'),
                              style: TextStyle(
                                fontFamily: 'SFProText',
                                color: Color(0xff262626),
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop();
                            getImage(ImageSource.camera);
                          },
                        )
                      ],
                      cancelButton: CupertinoActionSheetAction(
                        child: Text(AppLocalizations.of(context).translate('cancel'),
                            style: TextStyle(
                              fontFamily: 'SFProText',
                              color: Color(0xff262626),
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0,
                            )),
                        onPressed: () {
                          Navigator.of(context, rootNavigator: true).pop();
                        },
                      ));
                  showCupertinoModalPopup(
                      context: context, builder: (BuildContext context) => act);
                },
                child: Container(
                    margin: EdgeInsets.only(top: 18),
                    width: 100,
                    height: 100,
                    decoration: new BoxDecoration(
                      color: Color(0xa8000000),
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0x25000000),
                            offset: Offset(0, 5),
                            blurRadius: 15,
                            spreadRadius: 0)
                      ],
                    ),
                    alignment: Alignment.center,
                    child: (_image != null)
                        ? Container(
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: FileImage(_image))))
                        : new Stack(children: [
                            Padding(
                                padding: EdgeInsets.all(10),
                                child: SvgPicture.asset(
                                    ("assets/icon/multi-users.svg"))),
                            Padding(
                                padding: EdgeInsets.all(30),
                                child:
                                    SvgPicture.asset(("assets/icon/photo.svg")))
                          ])));
          }),
          Padding(
              padding: EdgeInsets.fromLTRB(8, 35, 7, 0),
              child: TextField(
                controller: _controller,
                decoration: InputDecoration(
                  hintText: AppLocalizations.of(context).translate('group_name'),
                  suffixIcon: IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        _controller.clear();
                      }),
                  hintStyle: TextStyle(
                    fontFamily: 'Ubuntu',
                    color: Color(0xffc6c6c6),
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                  ),
                  counterText: "${_controller.text.length}/40",
                ),
                maxLength: 40,
              )),
          new Container(
              margin: EdgeInsets.fromLTRB(8, 27, 8, 0),
              child: GridView.builder(
                itemCount: selectedAmis.length + 1,
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  // number of items per row
                  crossAxisCount: 3,
                  // vertical spacing between the items
                  mainAxisSpacing: 30.0,
                  // horizontal spacing between the items
                  crossAxisSpacing: 15.0,
                  childAspectRatio: 1.0,
                ),
                itemBuilder: (BuildContext context, int i) {
                  if (i == selectedAmis.length)
                  
                    return GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () {
                          Navigator.pop(context,selectedAmis);
                        },
                        child: Column(children: [
                          Container(
                                      margin: EdgeInsets.only(top: 10),
                              width: 52,
                              height: 52,
                              decoration: new BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0x33000000),
                                        offset: Offset(0, 0),
                                        blurRadius: 5,
                                        spreadRadius: 0)
                                  ],
                                  color: Color(0xffffffff),
                                  borderRadius: BorderRadius.circular(8)),
                              child: DottedBorder(
                                radius: Radius.circular(8),
                                dashPattern: [4],
                                strokeWidth: 1,
                                strokeCap: StrokeCap.round,
                                borderType: BorderType.RRect,
                                color: Color(0xffc6c6c6),
                                child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: SvgPicture.asset(
                                        ("assets/icon/add-user.svg"))),
                              )),
                          SizedBox(
                            height: 4,
                          ),
                          Text(AppLocalizations.of(context).translate('add'),
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                color: Color(0xff3f3c3c),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0.2708333333333333,
                              ))
                        ]));
                  else
                    return UserWidget(selectedAmis[i]);
                },
              ))
        ])));
  }

  Future getImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(source: source);
    if (image != null) {
      List<int> imageBytes = image.readAsBytesSync();
      print(imageBytes);
      String base64Image = base64Encode(imageBytes);
      print(base64Image);

      setState(() {
        _image = image;
        _base64Image = base64Image;
      });
    }
  }

  createGroupe(String token, String name, String photo, var list) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    data = ClientApi.creategroupe(token, name, photo, list);
    data
        .then((value) => {
              EasyLoading.dismiss(),
              EasyLoading.showSuccess(AppLocalizations.of(context).translate('group_created')),
              Navigator.pushNamed(context, "/detailmessage",
                  arguments: {'id': value.data, 'nom': _controller.text})
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  UserWidget(UserData user) {
    return Column(
      
      children: [
      if (user != null)
      new Stack(overflow: Overflow.visible, children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 10),
          width: 52,
          height: 52,
          decoration: new BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: Color(0x33000000),
                    offset: Offset(0, 0),
                    blurRadius: 5,
                    spreadRadius: 0)
              ],
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(Endpoints.baseUrlImage + user.photo),
              ),
              color: Color(0xffffffff),
              borderRadius: BorderRadius.circular(8)),
        ),
      SizedBox(
        height: 4,
      ),
      new Positioned(
        top: -10,
                  right: -20.0,
                  child: new Stack(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.remove_circle,
                          size: 20.0, color: Color(0xffe15656)),onPressed:() {
                             setState((){   selectedAmis.removeWhere((item) => item.id ==user.id);});
                          }),
                    ],
                  ))
          ]),
      if (user != null)
        Text(user.nom + " " + user.prenom,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Ubuntu',
              color: Color(0xff3f3c3c),
              fontSize: 13,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              letterSpacing: 0.2708333333333333,
            )),
    ]);
  }
}
