import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/Podcasts.dart';
import 'package:voicechanger/data/network/Model/data.dart';
enum PlayerState { stopped, playing, paused }

class PodcastWidget extends StatefulWidget {
    const PodcastWidget(this.podcast, {this.selectedindex,this.options=false,this.deletePodcast,this.refreshPodcast,this.reset});

 final PodcastsData podcast;
  final int selectedindex;
 final bool options;
  final DeletePodcast deletePodcast ;
  final RefreshPodcast refreshPodcast;
     final VoidCallback reset ;

  @override
  _PodcastWidgetState createState() => _PodcastWidgetState();

  static String formatDuration(Duration d) {
    var seconds = d.inSeconds;
    final days = seconds ~/ Duration.secondsPerDay;
    seconds -= days * Duration.secondsPerDay;
    final hours = seconds ~/ Duration.secondsPerHour;
    seconds -= hours * Duration.secondsPerHour;
    final minutes = seconds ~/ Duration.secondsPerMinute;
    seconds -= minutes * Duration.secondsPerMinute;

    final List<String> tokens = [];

    if (hours != 0) {
      tokens.add('${hours}h');
    }
    if (minutes != 0) {
      tokens.add('${minutes}min');
    }
    if (seconds != 0) {
      tokens.add('${seconds}s');
    }
    return tokens.join('');
  }
}

class _PodcastWidgetState extends State<PodcastWidget> {
 bool like;

 bool dislike; 
  int nbr_like;
  int nbr_dislike;
  Future<Data> data;
  AudioPlayer _audioPlayer;
  AudioPlayerState _audioPlayerState;
  Duration _duration;
  Duration _position;

  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  StreamSubscription _playerCompleteSubscription;
  StreamSubscription _playerErrorSubscription;
  StreamSubscription _playerStateSubscription;
  bool seekDone;

  get _isPlaying => widget.podcast.playerState == PlayerState.playing;
  get _isPaused => widget.podcast.playerState == PlayerState.paused;
  get _durationText =>
      "${_duration?.inMinutes?.remainder(60) ?? ''}:${(_duration?.inSeconds?.remainder(60)) ?? ''}";
  get _positionText =>
      "${_position?.inMinutes?.remainder(60) ?? ''}:${(_position?.inSeconds?.remainder(60)) ?? ''}";

 @override
void initState() {

    super.initState();
    like=widget.podcast.liked;
    dislike=widget.podcast.disliked;
    nbr_like=widget.podcast.nbrLike;
    nbr_dislike=widget.podcast.nbrDislike;
}

  







  @override
  Widget build(BuildContext context) {
    final iconShape = SvgPicture.asset(("assets/icon/shape.svg"));
    return  WillPopScope(
        onWillPop: () async {
          print(widget.podcast.playerState);
          if (widget.podcast.playerState == PlayerState.paused ||
              widget.podcast.playerState == PlayerState.playing) {
            await _audioPlayer.release();
          }
          Navigator.pop(context);
          return false;
        },
        child:
       
        Container(
                    height: 250,
          margin: EdgeInsets.fromLTRB(8.5, 0, 7.5, 15),
          decoration: new BoxDecoration(
            color: Color(0xffffffff),
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: Color(0x0d000000),
                  offset: Offset(0, 0),
                  blurRadius: 20,
                  spreadRadius: 0)
            ],
          ),
          child: Column(children: [
            ListTile(
                leading: GestureDetector(
                    onTap: () {
                      getID().then((id) {

                      if(widget.podcast.user.id!=id)
                      
                      Navigator.pushNamed(context, "/chainePage", arguments: {
                        'id': widget.podcast.user.id,
                        'username': widget.podcast.user.nom + " " + widget.podcast.user.prenom
                      }).then((value) => {
widget.refreshPodcast()

                      });
                       });
                    },
                    child: Container(
                      width: 40,
                      height: 40,
                      decoration: new BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                                Endpoints.baseUrlImage + widget.podcast.user.photo),
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Color(0x33000000),
                                offset: Offset(0, 0),
                                blurRadius: 5,
                                spreadRadius: 0)
                          ],
                          borderRadius: BorderRadius.circular(8)),
                    )),
                title: Row(children: [ Flexible(
  child:Text(widget.podcast.user.prenom.toUpperCase() + " " + widget.podcast.user.nom.toUpperCase(),
                    style: TextStyle(
                      fontFamily: 'Helvetica',
                      color: Color(0xff3f3c3c),
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.45,
                    ))),
                    Text("  -  ",style: TextStyle(
                      fontFamily: 'Helvetica',
                      color: Color(0xff3f3c3c),
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.45,
                    )),
                    Flexible(
  child:Text(widget.podcast.nom.toUpperCase(),
                    style: TextStyle(
                      fontFamily: 'Helvetica',
                      color: Color(0xff3f3c3c),
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.45,
                    ))),
                    
                    ]),
                subtitle: Text(
                    getDate(DateTime.parse(widget.podcast.createdAt),context) +
                        " - " +
                        PodcastWidget.formatDuration(parseDuration(widget.podcast.duree)),
                    style: TextStyle(
                      fontFamily: 'Helvetica',
                      color: Color(0xff949494),
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.2708333333333333,
                    )),
            
                
                
                ),
          Row(children: [
           Expanded(child: Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                    padding: EdgeInsets.fromLTRB(8, 6, 13, 11),
                    child: Text(widget.podcast.description,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontFamily: 'Helvetica',
                          color: Color(0xff949494),
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0.2916666666666667,
                        ))))),
   widget.options ? StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return IconButton(
                  icon: SvgPicture.asset(
                    "assets/icon/more.svg",
                    color: Colors.grey,
                  ),
                  onPressed: () {
                    final act = CupertinoActionSheet(
                        actions: <Widget>[
                            CupertinoActionSheetAction(
                              child: Text(AppLocalizations.of(context).translate('delete_talk'),
                                  style: TextStyle(
                                    fontFamily: 'SFProText',
                                    color: Color(0xff262626),
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  )),
                              onPressed: () {
                                Navigator.of(context, rootNavigator: true)
                                    .pop();
                              showAlertDialog(context);
                              },
                            ),
                            CupertinoActionSheetAction(
                              child: Text(AppLocalizations.of(context).translate('modify_talk'),
                                  style: TextStyle(
                                    fontFamily: 'SFProText',
                                    color: Color(0xff262626),
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  )),
                              onPressed: () {
                                                         Navigator.of(context, rootNavigator: true)
                                    .pop();
                                Navigator.pushNamed(context, "/editpodcast1", arguments: {
            'id': widget.podcast.id,
          });
                                      

                              },
                            )
                        ],
                        cancelButton: CupertinoActionSheetAction(
                          child: Text(AppLocalizations.of(context).translate('cancel'),
                              style: TextStyle(
                                fontFamily: 'SFProText',
                                color: Color(0xff000000),
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop();
                          },
                        ));
                    showCupertinoModalPopup(
                        context: context,
                        builder: (BuildContext context) => act);
                  });
            })
              : iconShape]),
                
                        Row(
  mainAxisAlignment:MainAxisAlignment.spaceEvenly ,
              children: [
               new InkWell(
                        onTap: () => {
                          initPlayer(),

                          print(_isPlaying),
                              if (_isPlaying) {_pause()} else {_play(Endpoints.baseUrlImage+widget.podcast.file)}
                        },
                        child: Container(
                          margin: EdgeInsets.only(left: 15),
                            width: 30,
                            height: 30,
                            decoration: new BoxDecoration(
                              border:
                                  new Border.all(color: Colors.white, width: 2),
                              shape: BoxShape.circle,
                              color: Color(0xff25A996),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0x2b2d2d2d),
                                    offset: Offset(0, 8),
                                    blurRadius: 20,
                                    spreadRadius: 0)
                              ],
                            ),
                            child: _isPlaying
                                ? Padding(
                                    padding: EdgeInsets.all(7),
                                    child: SvgPicture.asset(
                                        ("assets/icon/stop.svg")))
                                : Padding(
                                    padding: EdgeInsets.all(7),
                                    child: SvgPicture.asset(
                                        ("assets/icon/play.svg")))),
                      ),
                Expanded(
                  child: 
                  Stack(children: [
          SliderTheme(
                  
                      data: SliderThemeData(

                          trackHeight: 4,
                          thumbShape:
                              RoundSliderThumbShape(enabledThumbRadius: 0)),
                      child: Slider(
                        activeColor: Color(0xff25A996),
                        inactiveColor: Color(0xffC4C4C4),
                        onChanged: (v) {
                          if (_duration != null) {
                            final Position = v * _duration.inMilliseconds;
                            _audioPlayer
                                .seek(Duration(milliseconds: Position.round()));
                          }
                        },
                        value: (_position != null &&
                                _duration != null &&
                                _position.inMilliseconds > 0 &&
                                _position.inMilliseconds <
                                    _duration.inMilliseconds)
                            ? _position.inMilliseconds /
                                _duration.inMilliseconds
                            : 0.0,
                      )),
                       Positioned(
                         top: 30,
                         left: 27

                         ,child:

                  Text(_position != null ? _positionText : "0:0",
                        style: TextStyle(
                          fontFamily: 'Helvetica',
                          color: Color(0xff949494),
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0.2708333333333333,
                        ))),

   Positioned(
                         top: 30,
                         right: 27

                         ,child:

                  Text(_duration != null ? _durationText : widget.podcast.duree,
                        style: TextStyle(
                          fontFamily: 'Helvetica',
                          color: Color(0xff949494),
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0.2708333333333333,
                        ))),
                  ]),
                  
                 ),
            
              ],

              
            ),
                        Padding(padding: EdgeInsets.fromLTRB(12, 12, 12, 4),child:
 Divider(
              color: Color(0xFFC4C4C4),
              thickness: 1.0,
            )),

      IntrinsicHeight(
  child:      Row(        mainAxisAlignment: MainAxisAlignment.spaceAround,
children: [
              
              ElevatedButton.icon(
              
                style: ElevatedButton.styleFrom(
                  elevation: 0,
    primary: Colors.white, 
    onPrimary: Colors.white, 
    shadowColor: Colors.transparent,
    
  ),
  
                onPressed: (){
                  getID().then((id){
      
                 
                print("aaaaaaa"+widget.podcast.id.toString());
                Navigator.pushNamed(context, "/comments", arguments: {
            'id': widget.podcast.id,
            'id_user':id
          }).then((value) => {
            widget.refreshPodcast()
          });
           });
              }, icon:  SvgPicture.asset(("assets/icon/comment.svg"),width: 25.83,height: 24.25
,
              
              ), label: Text(widget.podcast.nbrComment.toString(),
              style: TextStyle(color:Color(0xFF949494) ,fontSize: 18,fontFamily: "Helvetica",fontWeight: FontWeight.normal),
              )),
              
  Padding(padding: EdgeInsets.symmetric(vertical: 5),child:   VerticalDivider(
              color: Color(0xFFC4C4C4),
              thickness: 1.0,
            )),
ElevatedButton.icon(
              
                style: ElevatedButton.styleFrom(
                  elevation: 0,
    primary: Colors.white, 
    onPrimary: Colors.white, 
    shadowColor: Colors.transparent,
    
  ),
  
                onPressed: (){
              
                
              }, icon: InkWell(
                onTap: (){
        
               if(dislike){
                 likeApi();
                  disLikeApi();
                  
                }
                else{
                 likeApi();
                }
                }
                
                ,child: SvgPicture.asset(("assets/icon/like.svg"),width: 19.44,height: 20,color:like ? Color(0xFF25A996): Color(0xFF949494))), label: InkWell(onTap: (){
                             getID().then((id) {
             Navigator.pushNamed(
      context,
      "/listlikedislike",
      arguments: {'id': widget.podcast.id,'nom': widget.podcast.nom,'page':0,'userId':widget.podcast.user.id,'user_c':id},
    ).then((value) => widget.refreshPodcast());
                             });
                  
                  
                },child: Text(nbr_like.toString(),
              style: TextStyle(color:like ? Color(0xFF25A996): Color(0xFF949494),fontSize: 18,fontFamily: "Helvetica",fontWeight: FontWeight.normal ),
              ))),

              ElevatedButton.icon(
              
                style: ElevatedButton.styleFrom(
                  elevation: 0,
    primary: Colors.white, 
    onPrimary: Colors.white, 
    shadowColor: Colors.transparent,
    
  ),
  
                onPressed: (){
              
              }, icon: InkWell(
                onTap: (){
                if(like){
                  disLikeApi();
                  likeApi();
                }
                else{
                 disLikeApi();
                }
               
                },
                child: SvgPicture.asset(("assets/icon/dislike.svg"),width: 19.44,height: 20,color:dislike ? Color(0xFFE15656): Color(0xFF949494))), label:InkWell(
                onTap: (){
                  getID().then((id) {
                  Navigator.pushNamed(
      context,
      "/listlikedislike",
      arguments: {'id': widget.podcast.id,'nom': widget.podcast.nom,'page':1,'userId':widget.podcast.user.id,'user_c':id},
    );});
                },
                child: Text(nbr_dislike.toString(),
              style: TextStyle(color:dislike ? Color(0xFFE15656): Color(0xFF949494),fontSize: 18,fontFamily: "Helvetica",fontWeight: FontWeight.normal),
              ))),
              
            ]))

          ]),
        ));
  }

  likeApi(){
getToken().then((token)=>{

    
    data = ClientApi.likePodcast(token, widget.podcast.id),
    data
        .then((value) => {
     setState((){
               
                 if(like){
                        nbr_like= nbr_like-1;
                 }
                 else{
 nbr_like= nbr_like+1;
                 }
                  like=!like;
                })


             
            })
        .catchError((error) =>
            { print("erreur : " + error.toString())}),
  });
  }


disLikeApi(){
         getToken().then((token)=>{

    
  
    
    data = ClientApi.dislikePodcast(token, widget.podcast.id),
    data
        .then((value) => {
              
   setState((){
                 if(dislike){
                        nbr_dislike= nbr_dislike-1;
                 }
                 else{
 nbr_dislike= nbr_dislike+1;
                 }
                                  dislike=!dislike;

                })


             
            })
        .catchError((error) =>
            {print("erreur : " + error.toString())}),
  });
}


  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(AppLocalizations.of(context).translate('no')),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop(); // dismiss dialog
      },
    );
    Widget continueButton = FlatButton(
      child: Text(AppLocalizations.of(context).translate('yes')),
      onPressed: () {
   widget.deletePodcast(widget.podcast.id);
        Navigator.of(context, rootNavigator: true).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(AppLocalizations.of(context).translate('confirmation_deletion_message')),
      content: Text(AppLocalizations.of(context).translate('confirmation_deletion_talk')),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

   Future<void> initPlayer() async {
        _audioPlayer = AudioPlayer(playerId: 'my_unique_playerId');
 seekDone = false;
   

    ///await _audioPlayer.setUrl(url);

// prepare the player with this audio but do not start playing
    await _audioPlayer.setReleaseMode(
        ReleaseMode.RELEASE); // set release mode so that it never releases

    _durationSubscription = _audioPlayer.onDurationChanged.listen((duration) {
      setState(() => {_duration = duration});
      print(duration);
    });

    _positionSubscription =
        _audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
              _position = p;
            }));

    _playerCompleteSubscription =
        _audioPlayer.onPlayerCompletion.listen((event) {
      _onComplete();
      setState(() {
        _position = null;
      });
    });

    _playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        widget.podcast.playerState = PlayerState.stopped;
        _duration = Duration(seconds: 0);
        _position = Duration(seconds: 0);
      });
    });

    _audioPlayer.onPlayerStateChanged.listen((state) {
      setState(() {
        _audioPlayerState = state;
      });
    });

    _audioPlayer.onNotificationPlayerStateChanged.listen((state) {
      setState(() => _audioPlayerState = state);
    });
  }

  Future<int> _play(String url) async {
   
     widget.reset();

    if (seekDone == true) {
      seekDone = false;
    }
    final playPosition = (_position != null &&
            _duration != null &&
            _position.inMilliseconds > 0 &&
            _position.inMilliseconds < _duration.inMilliseconds)
        ? _position
        : null;
    final result = await _audioPlayer.play(url, position: playPosition);

    if (result == 1) {
      setState(() => widget.podcast.playerState = PlayerState.playing);
    }
    print(widget.podcast.playerState);

    // default playback rate is 1.0
    // this should be called after _audioPlayer.play() or _audioPlayer.resume()
    // this can also be called everytime the user wants to change playback rate in the UI
    _audioPlayer.setPlaybackRate(playbackRate: 1.0);

    return result;
  }

  Future<int> _pause() async {
    final result = await _audioPlayer.pause();
    if (result == 1) setState(() => widget.podcast.playerState = PlayerState.paused);
    return result;
  }

  Future<int> _stop() async {
    final result = await _audioPlayer.stop();
    if (result == 1) {
      setState(() {
        widget.podcast.playerState = PlayerState.stopped;
        _position = Duration();
      });
    }
    return result;
  }




  void _onComplete() {
    setState(() => {
          widget.podcast.playerState = PlayerState.stopped,
          seekDone = true,
        });
  }
    Future<int> _stopPlayer() async {
    final result = await _audioPlayer.stop();
    if (result == 1) {
      setState(() {
        widget.podcast.playerState = PlayerState.stopped;
        _position = Duration();
      });
      _onComplete();
    }
    return result;
  }

  String getDate(DateTime date,context) {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final yesterday = DateTime(now.year, now.month, now.day - 1);
    String finaldate;
    final aDate = DateTime(date.year, date.month, date.day);
    if (aDate == today) {
      finaldate = DateFormat("h:mm a").format(date.toLocal());
    } else if (aDate == yesterday) {
      finaldate = AppLocalizations.of(context).translate('yesterday');
    } else {
      finaldate = DateFormat("EEE, dd MMM").format(date.toLocal());
    }
    return finaldate;
  }

  Duration parseDuration(String s) {
    int hours = 0;
    int minutes = 0;
    int micros;
    List<String> parts = s.split(':');
    if (parts.length > 2) {
      hours = int.parse(parts[parts.length - 3]);
    }
    if (parts.length > 1) {
      minutes = int.parse(parts[parts.length - 2]);
    }
    micros = (double.parse(parts[parts.length - 1]) * 1000000).round();
    return Duration(hours: hours, minutes: minutes, microseconds: micros);
  }

   Future<int> getID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt("id");
  }

    Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }
}
typedef DeletePodcast = void Function(int  i);
typedef RefreshPodcast = void Function();
