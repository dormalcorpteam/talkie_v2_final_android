import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Footer extends StatelessWidget {
  Footer(this.selectedIndex, this.onItemTapped);
  ValueChanged<int> onItemTapped;
  int selectedIndex;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: new BoxDecoration(
          color: Color(0xffffffff),
          boxShadow: [
            BoxShadow(
                color: Color(0x282d2d2d),
                offset: Offset(0, -3),
                blurRadius: 20,
                spreadRadius: 0)
          ],
        ),
        child: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: SvgPicture.asset(
                "assets/icon/bubble.svg",
              ),
              title: Text('Discussions'),
              activeIcon: SvgPicture.asset(
                "assets/icon/bubble.svg",
                color: Color(0xff25a996),
              ),
            ),
            BottomNavigationBarItem(
              icon: SvgPicture.asset(
                "assets/icon/mic2.svg",
              ),
              title: Text("Talk"),
              activeIcon: SvgPicture.asset(
                "assets/icon/mic2.svg",
                color: Color(0xff25a996),
              ),
            ),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  "assets/icon/user.svg",
                ),
                title: Text('My World'),
                activeIcon: SvgPicture.asset(
                  "assets/icon/user.svg",
                  color: Color(0xff25a996),
                )),
          ],
          currentIndex: selectedIndex,
          selectedItemColor: Color(0xff25a996),
          unselectedItemColor: Color(0xff949494),
          unselectedLabelStyle: TextStyle(
              color: const Color(0xff949494),
              fontWeight: FontWeight.w400,
              fontFamily: "SFProText",
              fontStyle: FontStyle.normal,
              fontSize: 10.0),
          selectedLabelStyle: TextStyle(
              color: const Color(0xff25a996),
              fontWeight: FontWeight.w700,
              fontFamily: "SFProText",
              fontStyle: FontStyle.normal,
              fontSize: 10.0),
          onTap: onItemTapped,
        ));
  }
}
