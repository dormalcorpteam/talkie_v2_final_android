import 'dart:async';
import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:video_player/video_player.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/detailMessage.dart';

import 'Bubble.dart';

enum PlayerState { stopped, playing, paused }

class MyWidgetSlider extends StatefulWidget {
  final DetailMessages detailMessages;
  final Entete entete;
  final String datePrevious;
  final bool previousIsme;
  final   ScrollController _scrollController;

   final VoidCallback deleteMessage ;
   final VoidCallback reset ;

  MyWidgetSlider( this.detailMessages, this.entete, this.datePrevious,
      this.previousIsme,this._scrollController,{Key key,this.deleteMessage,this.reset})
      : super(key:key);

  @override
  State<StatefulWidget> createState() {
    return _MyWidgetSliderState(detailMessages, entete, datePrevious,
        previousIsme,_scrollController );
  }
}

class _MyWidgetSliderState extends State<MyWidgetSlider>  {
  DetailMessages detailMessages;
  Entete entete;
  String datePrevious;
  bool previousIsme;
  Duration _duration;
  Duration _position;
 AudioPlayer _audioPlayer;
  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  StreamSubscription _playerCompleteSubscription;
  StreamSubscription _playerErrorSubscription;
  StreamSubscription _playerStateSubscription;
  VideoPlayerController _controller;
    ChewieController _chewieController;
  Future<void> _initializeVideoPlayerFuture;

  ScrollController _scrollController;

  AudioPlayerState _audioPlayerState;
  int selected_id;
  @override
      get _isPlaying => detailMessages.playerState == PlayerState.playing;
  get _isPaused => detailMessages.playerState == PlayerState.paused;
  get _durationText =>
      "${_duration?.inMinutes?.remainder(60) ?? ''}:${(_duration?.inSeconds?.remainder(60)) ?? ''}";
  get _positionText =>
      "${_position?.inMinutes?.remainder(60) ?? ''}:${(_position?.inSeconds?.remainder(60)) ?? ''}";
  _MyWidgetSliderState(this.detailMessages, this.entete, this.datePrevious,
      this.previousIsme,this._scrollController);
Random _rnd ;
  @override
  void initState() {
   
  super.initState();
  
  _rnd=Random();
        
  if(detailMessages.type=="video"){
         _controller = VideoPlayerController.network(
Endpoints.baseUrlImage+detailMessages.message, videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true))..initialize().then((value) => {setState((){}),
print("fin"),
       /*  Timer(
      Duration(milliseconds: 300),
      () => {
        if (_scrollController.hasClients)
          {_scrollController.jumpTo(_scrollController.position.maxScrollExtent)
          
          }
      },
    ),*/
 _chewieController = ChewieController(
    
      videoPlayerController: _controller,
      aspectRatio:  _controller.value.aspectRatio,
      showControlsOnInitialize: false,
      autoPlay: false,
      autoInitialize: false,
      looping: false,
      fullScreenByDefault: false,
      allowFullScreen: false,
      allowMuting: false,
      allowedScreenSleep: false,
      allowPlaybackSpeedChanging: false
      
      // Try playing around with some of these other options:

      // showControls: false,
      // materialProgressColors: ChewieProgressColors(
      //   playedColor: Colors.red,
      //   handleColor: Colors.blue,
      //   backgroundColor: Colors.grey,
      //   bufferedColor: Colors.lightGreen,
      // ),
      // placeholder: Container(
      //   color: Colors.grey,
      // ),
      // autoInitialize: true,
    
)});

    // Initialize the controller and store the Future for later use.
    //_initializeVideoPlayerFuture = _controller.initialize();

    // Use the controller to loop the video.
 
      }
   
   
  }

  @override
  dispose() async {
   
  if(detailMessages.type=="video"){   _chewieController?.dispose();_controller?.dispose();}

    super.dispose();
 if(mounted){  
      print("aaaa"); 
      await _audioPlayer?.dispose();
          _durationSubscription?.cancel();
    _positionSubscription?.cancel();
    _playerCompleteSubscription?.cancel();
    _playerErrorSubscription?.cancel();
    _playerStateSubscription?.cancel();

      }
  }



  @override
  Widget build(BuildContext context) {
  
    return AnimatedOpacity(
          // If the widget is visible, animate to 0.0 (invisible).
          // If the widget is hidden, animate to 1.0 (fully visible).
          opacity: detailMessages.visible ? 1.0 : 0.0,
          
          duration: const Duration(milliseconds: 500),
          // The green box must be a child of the AnimatedOpacity widget.
          child:WillPopScope(
        onWillPop: () async {
           print(detailMessages.playerState);
        initPlayer();
            await _audioPlayer?.release();
          
          Navigator.pop(context);
          return false;
        },
        child:
        Column(
   children: <Widget>[

     if(detailMessages.type=="audio")   Container(child:(!detailMessages.current_user)
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                    (datePrevious != "" && !previousIsme)
                        ? GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/chainePage", arguments: {
                    'id': detailMessages.sender_id,
                    'username': detailMessages.sender
                  });  
                },
                child:Container(
                            width: 30,
                            height: 30,
                            decoration: differenceDate(
                                    DateTime.parse(detailMessages.date),
                                    DateTime.parse(datePrevious))
                                ? new BoxDecoration(
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: NetworkImage(
                                          Endpoints.baseUrlImage +
                                              detailMessages.sender_photo),
                                    ),
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x33000000),
                                          offset: Offset(0, 0),
                                          blurRadius: 5,
                                          spreadRadius: 0)
                                    ],
                                    borderRadius: BorderRadius.circular(8))
                                : BoxDecoration(),
                          ))
                        : GestureDetector(
                onTap: () {
                                                 
                 


                  Navigator.pushNamed(context, "/chainePage", arguments: {
                    'id': detailMessages.sender_id,
                    'username': detailMessages.sender
                  });  
                },
                child:Container(
                            width: 30,
                            height: 30,
                            decoration: new BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(Endpoints.baseUrlImage +
                                      detailMessages.sender_photo),
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0x33000000),
                                      offset: Offset(0, 0),
                                      blurRadius: 5,
                                      spreadRadius: 0)
                                ],
                                borderRadius: BorderRadius.circular(8)),
                          )),
                    Container(
                        child: Bubble(
                      slider: SliderTheme(
                          data: SliderThemeData(
                              trackHeight: 4,
                              thumbShape:
                                  RoundSliderThumbShape(enabledThumbRadius: 0)),
                          child: Slider(
                            activeColor: (detailMessages.current_user)
                                ? Colors.white
                                : Color(0xff25a996),
                            inactiveColor: Color(0xffc4c4c4),
                            onChanged: (v) {
                              if (_duration != null) {
                                 final  Position = v * _duration.inMilliseconds;
                                _audioPlayer.seek(
                                    Duration(milliseconds: Position.round()));
                              }
                            },
                                                                        
                            value: (_position != null &&
                                    _duration != null &&
                                    _position.inMilliseconds > 0 &&
                                    _position.inMilliseconds <
                                        _duration.inMilliseconds)
                                ? _position.inMilliseconds /
                                    _duration.inMilliseconds
                                : 0.0,
                          )),
                      isMe: (detailMessages.current_user),
                      date: detailMessages.date,
                      duree: detailMessages.duree,
                    )),
                    new IconButton(
                        onPressed: () => {
                          print(_isPlaying),
                           initPlayer(),

                              if (_isPlaying) {_pause()} else {_play()},
                            },
                        icon: _isPlaying
                            ? Padding(
                                padding: EdgeInsets.all(7),
                                child: SvgPicture.asset(
                                    ("assets/icon/stop.svg"),
                                    color: Color(0xff949494)),
                              )
                            : Padding(
                                padding: EdgeInsets.all(7),
                                child: SvgPicture.asset(
                                    ("assets/icon/play.svg"),
                                    color: Color(0xff949494)),
                              ))
                  ])
            : GestureDetector(      
              behavior:HitTestBehavior.translucent,
                           onLongPress: () => {optionsMessage()
                                                 
                        },
                     
    
    child: Container(color:(detailMessages.selected)? Color(0xff25a996).withOpacity(0.5):null,child:Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
          if(detailMessages.selected)  IconButton( onPressed: () => {
                            },icon: Padding(
                                padding: EdgeInsets.all(4),child:     
          Icon(Icons.check_box , color:Color(0xff25a996),size: 26,))),


                    new IconButton(
                        onPressed: () => {
                           initPlayer(),

                          print(_isPlaying),
                              if (_isPlaying) {_pause()} else {_play()},
                              
                            },
                            
                        icon: _isPlaying
                            ? Padding(
                                padding: EdgeInsets.all(7),
                                child: SvgPicture.asset(
                                    ("assets/icon/stop.svg"),
                                    color: Color(0xff949494)),
                              )
                            : Padding(
                                padding: EdgeInsets.all(7),
                                child: SvgPicture.asset(
                                    ("assets/icon/play.svg"),
                                    color: Color(0xff949494)),
                              )),
                    Container(
                        

                        width: (MediaQuery.of(context).size.width * 2.5) / 4,
                        child: Bubble(
                          slider: SliderTheme(
                              data: SliderThemeData(
                                  trackHeight: 4,
                                  thumbShape: RoundSliderThumbShape(
                                      enabledThumbRadius: 0)),
                              child: Slider(
                                activeColor: (detailMessages.current_user)
                                    ? Colors.white
                                    : Color(0xff25a996),
                                inactiveColor: Color(0xffc4c4c4),
                                onChanged: (v) {
                                  if (_duration != null) {
                                    final Position =
                                        v * _duration.inMilliseconds;
                                    _audioPlayer.seek(Duration(
                                        milliseconds: Position.round()));
                                  }
                                },
                                value: (_position != null &&
                                        _duration != null &&
                                        _position.inMilliseconds > 0 &&
                                        _position.inMilliseconds <
                                            _duration.inMilliseconds)
                                    ? _position.inMilliseconds /
                                        _duration.inMilliseconds
                                    : 0.0,
                              )),
                          isMe: (detailMessages.current_user),
                          date: detailMessages.date,
                          duree: detailMessages.duree,
                        )),
                  ])))
                  
                  
                  
                  
                  ),












 if(detailMessages.type=="call")  
Container(child:(!detailMessages.current_user)
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                     (datePrevious != "" && !previousIsme)
                        ? GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/chainePage", arguments: {
                    'id': detailMessages.sender_id,
                    'username': detailMessages.sender
                  });  
                },
                child:Container(
                            width: 30,
                            height: 30,
                            decoration: differenceDate(
                                    DateTime.parse(detailMessages.date),
                                    DateTime.parse(datePrevious))
                                ? new BoxDecoration(
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: NetworkImage(
                                          Endpoints.baseUrlImage +
                                              detailMessages.sender_photo),
                                    ),
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x33000000),
                                          offset: Offset(0, 0),
                                          blurRadius: 5,
                                          spreadRadius: 0)
                                    ],
                                    borderRadius: BorderRadius.circular(8))
                                : BoxDecoration(),
                          ))
                        : GestureDetector(
                onTap: () {
                                                 
                 


                  Navigator.pushNamed(context, "/chainePage", arguments: {
                    'id': detailMessages.sender_id,
                    'username': detailMessages.sender
                  });  
                },
                child:Container(
                            width: 30,
                            height: 30,
                            decoration: new BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(Endpoints.baseUrlImage +
                                      detailMessages.sender_photo),
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0x33000000),
                                      offset: Offset(0, 0),
                                      blurRadius: 5,
                                      spreadRadius: 0)
                                ],
                                borderRadius: BorderRadius.circular(8)),
                          )),
                      Container(
                                               width: (MediaQuery.of(context).size.width * 2.5) / 4,

                        child: Bubble(
                    isMe: (detailMessages.current_user),
                          date: detailMessages.date,
                          duree: detailMessages.duree,
                    slider: Container(
                       width: (MediaQuery.of(context).size.width * 2.5) / 4,
                   child:Text((detailMessages.missed_call)? AppLocalizations.of(context).translate('missed_call')+entete.nom:AppLocalizations.of(context).translate('recieved_call')+entete.nom,    style:   TextStyle(
		fontSize: 11,
		fontFamily: 'Ubuntu',
		fontWeight: FontWeight.w300,
	))
            
                   
                   ))),
                   
                  ])
            : Container(child:Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                Container(
                                               width: (MediaQuery.of(context).size.width * 2.5) / 4,


                        child: Bubble(
                    isMe: (detailMessages.current_user),
                          date: detailMessages.date,
                          duree: detailMessages.duree,
                    slider: Container(
                     width: (MediaQuery.of(context).size.width * 2.5) / 4,
                   child: Text(AppLocalizations.of(context).translate('call_action')+entete.nom,style:   TextStyle(
		fontSize: 11,
		fontFamily: 'Ubuntu',
		fontWeight: FontWeight.w300,
    color: Colors.white
	))
                   
                   ))),
                  ]))
                  
                  
                  
                  
                  ),





 if(detailMessages.type=="text")  
Container(child:(!detailMessages.current_user)
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                   (datePrevious != "" && !previousIsme)
                        ? GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/chainePage", arguments: {
                    'id': detailMessages.sender_id,
                    'username': detailMessages.sender
                  });  
                },
                child:Container(
                            width: 30,
                            height: 30,
                            decoration: differenceDate(
                                    DateTime.parse(detailMessages.date),
                                    DateTime.parse(datePrevious))
                                ? new BoxDecoration(
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: NetworkImage(
                                          Endpoints.baseUrlImage +
                                              detailMessages.sender_photo),
                                    ),
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x33000000),
                                          offset: Offset(0, 0),
                                          blurRadius: 5,
                                          spreadRadius: 0)
                                    ],
                                    borderRadius: BorderRadius.circular(8))
                                : BoxDecoration(),
                          ))
                        : GestureDetector(
                onTap: () {
                                                 
                 


                  Navigator.pushNamed(context, "/chainePage", arguments: {
                    'id': detailMessages.sender_id,
                    'username': detailMessages.sender
                  });  
                },
                child:Container(
                            width: 30,
                            height: 30,
                            decoration: new BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(Endpoints.baseUrlImage +
                                      detailMessages.sender_photo),
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0x33000000),
                                      offset: Offset(0, 0),
                                      blurRadius: 5,
                                      spreadRadius: 0)
                                ],
                                borderRadius: BorderRadius.circular(8)),
                          )),
                      Container(
                        width: (MediaQuery.of(context).size.width * 2.5) / 4,
                        child: Bubble(
                    isMe: (detailMessages.current_user),
                          date: detailMessages.date,
                          duree: detailMessages.duree,
                    slider: Container(
                       width: (MediaQuery.of(context).size.width * 2.5) / 4,
 margin: EdgeInsets.all(2),
                   child:Text(detailMessages.message,    style:   TextStyle(
		fontSize: 15,
		fontFamily: 'Ubuntu',
		fontWeight: FontWeight.w300,
	))
            
                   
                   ))),
                   
                  ])
            : Container(color:(detailMessages.selected)? Color(0xff25a996).withOpacity(0.5):null,child:Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                     if(detailMessages.selected)  IconButton( onPressed: () => {
                            },icon: Padding(
                                padding: EdgeInsets.all(4),child:     
          Icon(Icons.check_box , color:Color(0xff25a996),size: 26,))),
                Container(
                        width: (MediaQuery.of(context).size.width * 2.5) / 4,

                        child: Bubble(
                    isMe: (detailMessages.current_user),
                          date: detailMessages.date,
                          duree: detailMessages.duree,
                    slider: Container(
                     width: (MediaQuery.of(context).size.width * 2.5) / 4,
                   child: Padding(padding: EdgeInsets.all(10),child:Text(detailMessages.message,style:   TextStyle(
		fontSize: 15,
		fontFamily: 'Ubuntu',
		fontWeight: FontWeight.w300,
    color: Colors.white
	)))
                   
                   ))),
                  ]))
                  
                  
                  
                  
                  ),






 if(detailMessages.type=="image")  
Container(child:(!detailMessages.current_user)
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                   (datePrevious != "" && !previousIsme)
                        ? GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/chainePage", arguments: {
                    'id': detailMessages.sender_id,
                    'username': detailMessages.sender
                  });  
                },
                child:Container(
                            width: 30,
                            height: 30,
                            decoration: differenceDate(
                                    DateTime.parse(detailMessages.date),
                                    DateTime.parse(datePrevious))
                                ? new BoxDecoration(
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: NetworkImage(
                                          Endpoints.baseUrlImage +
                                              detailMessages.sender_photo),
                                    ),
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x33000000),
                                          offset: Offset(0, 0),
                                          blurRadius: 5,
                                          spreadRadius: 0)
                                    ],
                                    borderRadius: BorderRadius.circular(8))
                                : BoxDecoration(),
                          ))
                        : GestureDetector(
                onTap: () {
                                                 
                 


                  Navigator.pushNamed(context, "/chainePage", arguments: {
                    'id': detailMessages.sender_id,
                    'username': detailMessages.sender
                  });  
                },
                child:Container(
                            width: 30,
                            height: 30,
                            decoration: new BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(Endpoints.baseUrlImage +
                                      detailMessages.sender_photo),
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0x33000000),
                                      offset: Offset(0, 0),
                                      blurRadius: 5,
                                      spreadRadius: 0)
                                ],
                                borderRadius: BorderRadius.circular(8)),
                          )),
                      Padding(padding:EdgeInsets.all(5),child:  Container(
                        width: (MediaQuery.of(context).size.width * 2.5) / 4,
                        child: Image.network(
      Endpoints.baseUrlImage +detailMessages.message,
      fit: BoxFit.cover,
      loadingBuilder: (BuildContext ctx, Widget child, ImageChunkEvent loadingProgress) {
        
        if (loadingProgress == null) {
          return child;
        }else {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        }
      },       
))),
                   
                  ])
            : Container(child:Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                 Padding(padding:EdgeInsets.all(5),child: Container(
                        width: (MediaQuery.of(context).size.width * 2.5) / 4,

                        child:   Image.network(
      Endpoints.baseUrlImage +detailMessages.message,
      fit: BoxFit.cover,
      loadingBuilder: (BuildContext ctx, Widget child, ImageChunkEvent loadingProgress) {
        if (loadingProgress == null) {
          return child;
        }else {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        }
      },       
))),
                  ]))
             
                  
                  
                  ),

             



                 if(detailMessages.type=="video")  
Container(child:(!detailMessages.current_user)
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                   (datePrevious != "" && !previousIsme)
                        ? GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/chainePage", arguments: {
                    'id': detailMessages.sender_id,
                    'username': detailMessages.sender
                  });  
                },
                child:Container(
                            width: 30,
                            height: 30,
                            decoration: differenceDate(
                                    DateTime.parse(detailMessages.date),
                                    DateTime.parse(datePrevious))
                                ? new BoxDecoration(
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: NetworkImage(
                                          Endpoints.baseUrlImage +
                                              detailMessages.sender_photo),
                                    ),
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x33000000),
                                          offset: Offset(0, 0),
                                          blurRadius: 5,
                                          spreadRadius: 0)
                                    ],
                                    borderRadius: BorderRadius.circular(8))
                                : BoxDecoration(),
                          ))
                        : GestureDetector(
                onTap: () {
                                                 
                 


                  Navigator.pushNamed(context, "/chainePage", arguments: {
                    'id': detailMessages.sender_id,
                    'username': detailMessages.sender
                  });  
                },
                child:Container(
                            width: 30,
                            height: 30,
                            decoration: new BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(Endpoints.baseUrlImage +
                                      detailMessages.sender_photo),
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0x33000000),
                                      offset: Offset(0, 0),
                                      blurRadius: 5,
                                      spreadRadius: 0)
                                ],
                                borderRadius: BorderRadius.circular(8)),
                          )),
                 Padding(padding:EdgeInsets.all(5),child:Container(
                                            width: (MediaQuery.of(context).size.width * 2.5) / 4,

                    child:
                   (_chewieController!=null)?  Container(
                      
             child: AspectRatio(
               
                  aspectRatio: _controller.value.aspectRatio,
                  child:Chewie(
                  controller: _chewieController,
                ),
              ),
                  ):
                  Center(child: Center(
            child: CupertinoActivityIndicator(),
          ))
                  )),

                   
                  ])
            : Container(
               
              child:Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  
                 Padding(padding:EdgeInsets.all(5),child: Container(
                        width: (MediaQuery.of(context).size.width * 2.5) / 4,

                    child:
                   (_chewieController!=null)?  Container(
                      
             child: AspectRatio(
               
                  aspectRatio: _controller.value.aspectRatio,
                  child:Chewie(
                  controller: _chewieController,
                ),
              ),
                  ):
                  Center(child: Center(
            child: CupertinoActivityIndicator(),
          ))
                  )),
                  ]))
             
                  
                  
                  ),


   ])));
  }

  Future<void> initPlayer() async {
     
    _audioPlayer = AudioPlayer(playerId: _rnd.toString());

     //await _audioPlayer.setUrl( Endpoints.baseUrlImage + detailMessages.voice); // prepare the player with this audio but do not start playing
    await _audioPlayer.setReleaseMode(
        ReleaseMode.STOP); // set release mode so that it never releases

    _durationSubscription = _audioPlayer.onDurationChanged.listen((duration) {
      setState(() => _duration = duration);
    });
    _positionSubscription =
        _audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
            print("pos "+ p.toString());
              _position = p;
            }));
           

    _playerCompleteSubscription =
        _audioPlayer.onPlayerCompletion.listen((event) {
      _onComplete();
      setState(() {
        _position = null;
      });
    });

    _playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        detailMessages.playerState = PlayerState.stopped;
        _duration = Duration(seconds: 0);
        _position = Duration(seconds: 0);
      });
    });

    _audioPlayer.onPlayerStateChanged.listen((state) {
       print("azer"+state.toString());
      setState(() {
        _audioPlayerState = state;
      });
    });

    _audioPlayer.onNotificationPlayerStateChanged.listen((state) {
      setState(() => _audioPlayerState = state);
    });

    
  }

  Future<int> _play() async {
     widget.reset();

 
    final playPosition = (_position != null &&
            _duration != null &&
            _position.inMilliseconds > 0 &&
            _position.inMilliseconds < _duration.inMilliseconds)
        ? _position
        : null;
    final result = await _audioPlayer.play(
        Endpoints.baseUrlImage + detailMessages.voice,
        position: playPosition);

    if (result == 1) {
      setState(() => detailMessages.playerState = PlayerState.playing);
    }
    print(detailMessages.playerState);
    // default playback rate is 1.0
    // this should be called after _audioPlayer.play() or _audioPlayer.resume()
    // this can also be called everytime the user wants to change playback rate in the UI
  //  _audioPlayer.setPlaybackRate(playbackRate: 1.0);

    return result;
  }

  Future<int> _pause() async {
    
    final result = await _audioPlayer.pause();
    if (result == 1) setState(() => {detailMessages.playerState = PlayerState.paused});
    return result;
  }

  Future<int> _stop() async {
    final result = await _audioPlayer.stop();
    if (result == 1) {
      setState(() {
        detailMessages.playerState = PlayerState.stopped;
        _position = Duration();
      });
    }
    return result;
  }

  void _onComplete() {
    setState(() => {
          detailMessages.playerState = PlayerState.stopped,
        });
  }

  bool differenceDate(DateTime current, DateTime previous) {
    Duration difference = current.difference(previous);
    return (difference.inHours >= 1);
  }



  optionsMessage(){
      final act = CupertinoActionSheet(
                        actions: <Widget>[
                            CupertinoActionSheetAction(
                              child: Text(AppLocalizations.of(context).translate('delete_message'),
                                  style: TextStyle(
                                    fontFamily: 'SFProText',
                                    color: Color(0xff262626),
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  )),
                              onPressed: () {
                                Navigator.of(context, rootNavigator: true)
                                    .pop();
                              showAlertDialog(context);
                              },
                            ),
                         
                        ],
                        cancelButton: CupertinoActionSheetAction(
                          child: Text(AppLocalizations.of(context).translate('cancel'),
                              style: TextStyle(
                                fontFamily: 'SFProText',
                                color: Color(0xff000000),
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop();
                          },
                        ));
                    showCupertinoModalPopup(
                        context: context,
                        builder: (BuildContext context) => act);
  }
  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(AppLocalizations.of(context).translate('no')),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop(); // dismiss dialog
      },
    );
    Widget continueButton = FlatButton(
      child: Text(AppLocalizations.of(context).translate('yes')),
      onPressed: () {
        print("idMessage"+detailMessages.id.toString());
        
   widget.deleteMessage();
 
        Navigator.of(context, rootNavigator: true).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(AppLocalizations.of(context).translate('confirmation_deletion_title')),
      content: Text(AppLocalizations.of(context).translate('confirmation_deletion_message')),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  
  }
