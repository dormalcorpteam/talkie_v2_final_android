import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:voicechanger/Page/createPodcastPage1.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/MyWorld.dart';
import 'package:voicechanger/data/network/Model/Podcasts.dart';
import 'package:voicechanger/data/network/Model/data.dart';
import 'package:file_picker/file_picker.dart';

import 'PodcastWidget.dart' as widget;

class MyWorldPage extends StatefulWidget {
  @override
  Myworld createState() => Myworld();
}

class Myworld extends State<MyWorldPage> {
  Future<MyWorld> myWorld;
  MyWorlEntete myWorlEntete;
  Future<Data> data;
  bool show = false;
  var listPodcast = new List<PodcastsData>();
  int nbr_listeners;
  int nbr_talkers;
  String language;

  getmyWorld(String token) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    myWorld = ClientApi.getMyWorld(token);
    myWorld
        .then((value) => {
              EasyLoading.dismiss(),
              setState(() {
                myWorlEntete = value.entete;
                listPodcast = value.data;
                show = true;
                nbr_talkers = value.nbrTalkers;
                nbr_listeners = value.nbrListeners;
              })
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }

  @override
  void initState() {
    super.initState();
    getToken().then((cleapi) => {getmyWorld(cleapi)});
    Devicelocale.currentLocale.then((value) => {
          setState(() {
            language = value.split(Platform.isAndroid ? '_' : '-')[0];
          })
        });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Color(0xff25a996),
        centerTitle: true,
        title: RichText(
          textAlign: TextAlign.center,
          text: (myWorlEntete != null)
              ? TextSpan(
                  text: '${myWorlEntete.nom}',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: Color(0xffffffff),
                    fontSize: 17,
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,
                  ),
                  children: <TextSpan>[
                      TextSpan(
                        text: '\n${myWorlEntete.username}',
                        style: TextStyle(
                          fontFamily: 'Roboto-thin',
                          color: Color(0xffffffff),
                          fontSize: 14,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ])
              : TextSpan(),
        ),
        actions: <Widget>[
          StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return IconButton(
                icon: SvgPicture.asset(
                  "assets/icon/more.svg",
                ),
                onPressed: () {
                  final act = CupertinoActionSheet(
                      actions: <Widget>[
                        CupertinoActionSheetAction(
                          child: Text(
                              AppLocalizations.of(context)
                                  .translate('edit_info'),
                              style: TextStyle(
                                fontFamily: 'SFProText',
                                color: Color(0xff262626),
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop();
                            Navigator.pushNamed(context, "/profile");
                          },
                        ),
                        CupertinoActionSheetAction(
                          child: Text(
                              AppLocalizations.of(context)
                                  .translate('invite_friends'),
                              style: TextStyle(
                                fontFamily: 'SFProText',
                                color: Color(0xff262626),
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                          onPressed: () async {
                            Navigator.of(context, rootNavigator: true).pop();
                            if (Platform.isAndroid) {
                              String uri = (language == "fr")
                                  ? 'sms:?body=Inviter%20vos%20amis%20via:\nlien%20Android:%20https://play.google.com/store/apps/details?id=com.dormalcorp.talkie\nlien%20Ios:%20https://apps.apple.com/us/app/talkie/id1506181100'
                                  : 'sms:?body=Invite%20your%20friends%20via:\nlien%20Android:%20https://play.google.com/store/apps/details?id=com.dormalcorp.talkie\nlien%20Ios:%20https://apps.apple.com/us/app/talkie/id1506181100';
                              if (await canLaunch(uri)) {
                                await launch(uri);
                              } else {
                                throw 'Could not launch $uri';
                              }
                            } else if (Platform.isIOS) {
                              // iOS
                              String uri = (language == "fr")
                                  ? 'sms:00&body=Inviter%20vos%20amis%20via:%20lien%20Android:%20https://play.google.com/store/apps/details?id=com.dormalcorp.talkie%20lien%20Ios:%20https://apps.apple.com/us/app/talkie/id1506181100'
                                  : 'sms:00&body=Invite%20your%20friends%20via:%20lien%20Android:%20https://play.google.com/store/apps/details?id=com.dormalcorp.talkie%20lien%20Ios:%20https://apps.apple.com/us/app/talkie/id1506181100';
                              if (await canLaunch(uri)) {
                                await launch(uri);
                              } else {
                                throw 'Could not launch $uri';
                              }
                            }
                          },
                        ),
                        CupertinoActionSheetAction(
                          child: Text(
                              AppLocalizations.of(context)
                                  .translate('privacy_policy'),
                              style: TextStyle(
                                fontFamily: 'SFProText',
                                color: Color(0xff262626),
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop();
                            FlutterWebBrowser.openWebPage(
                                url:
                                    "http://ns3122723.ip-51-68-37.eu/talkie-dev/public/uploads/cgu_talkie.pdf",
                               );
                          },
                        ),
                      ],
                      cancelButton: CupertinoActionSheetAction(
                        child: Text(
                            AppLocalizations.of(context).translate('cancel'),
                            style: TextStyle(
                              fontFamily: 'SFProText',
                              color: Color(0xff000000),
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0,
                            )),
                        onPressed: () {
                          Navigator.of(context, rootNavigator: true).pop();
                        },
                      ));
                  showCupertinoModalPopup(
                      context: context, builder: (BuildContext context) => act);
                });
          })
        ],
      ),
      body: Container(
          color: Color(0xfff8f9f9),
          child: Column(children: [
            if (myWorlEntete != null)
              ListTile(
                  contentPadding: EdgeInsets.fromLTRB(25, 25, 9, 0),
                  leading: Container(
                    width: 52,
                    height: 52,
                    decoration: new BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              Endpoints.baseUrlImage + myWorlEntete.photo),
                        ),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x33000000),
                              offset: Offset(0, 0),
                              blurRadius: 5,
                              spreadRadius: 0)
                        ],
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  title: Padding(
                      padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                      child: Text("${myWorlEntete.nom}",
                          style: TextStyle(
                            fontFamily: 'Ubuntu',
                            color: Color(0xff3f3c3c),
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0.4,
                          ))),
                  subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(// use whichever suits your need
                            children: <Widget>[
                          InkWell(
                              onTap: () {
                                Navigator.pushNamed(
                                  context,
                                  "/listtalkslisteners",
                                  arguments: {
                                    'id': myWorlEntete.id,
                                    'username': myWorlEntete.nom,
                                    'page': 0
                                  },
                                );
                              },
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text("$nbr_listeners listeners",
                                      style: TextStyle(
                                        fontFamily: 'Ubuntu',
                                        color: Color(0xff949494),
                                        fontSize: 13,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0.2708333333333333,
                                      )))),
                          SizedBox(width: 25),
                          InkWell(
                              onTap: () {
                                Navigator.pushNamed(
                                  context,
                                  "/listtalkslisteners",
                                  arguments: {
                                    'id': myWorlEntete.id,
                                    'username': myWorlEntete.nom,
                                    'page': 1
                                  },
                                );
                              },
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text("$nbr_talkers talkers",
                                      style: TextStyle(
                                        fontFamily: 'Ubuntu',
                                        color: Color(0xff949494),
                                        fontSize: 13,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0.2708333333333333,
                                      )))),
                        ]),
                        SizedBox(
                          height: 3,
                        ),
                        Text(
                            (myWorlEntete.nbrPodcast > 1)
                                ? "${myWorlEntete.nbrPodcast} Talks"
                                : "${myWorlEntete.nbrPodcast} Talk",
                            style: TextStyle(
                              fontFamily: 'Ubuntu',
                              color: Color(0xff949494),
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0.2708333333333333,
                            ))
                      ])),
            Padding(
                padding: EdgeInsets.fromLTRB(9, 14, 8, 16),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: ButtonTheme(
                          height: 25,
                          child: RaisedButton(
                            elevation: 0,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                              side: BorderSide(color: Color(0xff949494)),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            onPressed: () {
                              createPodcast(context);
                            },
                            child: Text(
                                AppLocalizations.of(context)
                                    .translate('create_talk'),
                                style: TextStyle(
                                  fontFamily: 'Ubuntu',
                                  color: Color(0xff3f3c3c),
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  fontStyle: FontStyle.normal,
                                )),
                          )),
                    ),
                    SizedBox(width: 14),
                    Expanded(
                        child: ButtonTheme(
                      height: 25,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Color(0xff949494)),
                            borderRadius: BorderRadius.circular(4)),
                        color: Colors.white,
                        child: Text("upload",
                            style: TextStyle(
                              fontFamily: 'Ubuntu',
                              color: Color(0xff3f3c3c),
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            )),
                        onPressed: () {
                          pickFile();
                        },
                      ),
                    ))
                  ],
                )),
            Expanded(
                child: (listPodcast.isEmpty && show)
                    ? Padding(
                        padding: EdgeInsets.symmetric(horizontal: 38),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SvgPicture.asset(
                              ("assets/icon/mic2.svg"),
                              height: 78,
                              width: 78,
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            RichText(
                                textAlign: TextAlign.center,
                                text: new TextSpan(children: [
                                  new TextSpan(
                                      text: AppLocalizations.of(context)
                                          .translate('free_yourself'),
                                      style: TextStyle(
                                        fontFamily: 'Ubuntu',
                                        color: Color(0xff9b9b9b),
                                        fontSize: 21,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      )),
                                  new TextSpan(
                                      text: AppLocalizations.of(context)
                                          .translate('join_community'),
                                      style: TextStyle(
                                        fontFamily: 'Ubuntu',
                                        color: Color(0xff9b9b9b),
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      )),
                                ])),
                            SizedBox(
                              height: 40,
                            ),
                            ButtonTheme(
                              minWidth: MediaQuery.of(context).size.width,
                              height: 45,
                              child: RaisedButton(
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(color: Color(0xff25a996)),
                                    borderRadius: BorderRadius.circular(4)),
                                color: Colors.white,
                                child: Text(
                                    AppLocalizations.of(context)
                                        .translate('create_talk'),
                                    style: TextStyle(
                                      fontFamily: 'Ubuntu',
                                      color: Color(0xff25a996),
                                      fontSize: 15,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    )),
                                onPressed: () {
                                  createPodcast(context);
                                },
                              ),
                            )
                          ],
                        ))
                    : ListView.builder(
                        itemCount: listPodcast.length,
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext ctxt, int index) {
                          return GestureDetector(
                              onTap: () {
                                Navigator.pushNamed(context, "/detailTalks",
                                    arguments: {
                                      'id': listPodcast[index].id,
                                      'titre': listPodcast[index].nom,
                                    }).then((value) => {
                                      Navigator.of(context)
                                          .pushNamedAndRemoveUntil('/home',
                                              (Route<dynamic> route) => false,
                                              arguments: {'selectedindex': 1}),
                                    });
                              },
                              child: widget.PodcastWidget(listPodcast[index],
                                  selectedindex: 2,
                                  options: true,
                                  reset: () => reset(listPodcast[index].id),
                                  refreshPodcast: () {
                                    Navigator.of(context)
                                        .pushNamedAndRemoveUntil('/home',
                                            (Route<dynamic> route) => false,
                                            arguments: {'selectedindex': 1});
                                  },
                                  deletePodcast: (int id) {
                                    getToken().then((token) => {
                                          EasyLoading.instance
                                            ..loadingStyle =
                                                EasyLoadingStyle.custom
                                            ..backgroundColor = Colors.white
                                            ..progressColor = Colors.white
                                            ..indicatorType =
                                                EasyLoadingIndicatorType.wave
                                            ..indicatorColor = Color(0xff3bc0ae)
                                            ..textColor = Color(0xff3bc0ae)
                                            ..userInteractions = false
                                            ..maskType =
                                                EasyLoadingMaskType.black,
                                          EasyLoading.show(
                                            status: AppLocalizations.of(context)
                                                .translate('loading'),
                                          ),
                                          data = ClientApi.deletePodcast(
                                              token, id),
                                          data
                                              .then((value) => {
                                                    EasyLoading.dismiss(),
                                                    EasyLoading.showSuccess(
                                                        value.data),
                                                    setState(() {
                                                      listPodcast.removeWhere(
                                                          (item) =>
                                                              item.id == id);
                                                      myWorlEntete.nbrPodcast =
                                                          myWorlEntete
                                                                  .nbrPodcast -
                                                              1;
                                                    }),
                                                  })
                                              .catchError((error) => {
                                                    EasyLoading.dismiss(),
                                                    print("erreur : " +
                                                        error.toString())
                                                  }),
                                        });
                                  }));
                        })),
          ])),
    );
  }

  reset(id) {
    for (int i = 0; i < listPodcast.length; i++) {
      if (listPodcast[i].playerState == widget.PlayerState.playing)
        setState(() {
          listPodcast[i].playerState = widget.PlayerState.stopped;
        });
    }
  }

  pickFile() async {
    FilePickerResult result = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: ["mp3", "mp4"]);
    if (result != null) {
      AudioPlayer audioPlayer = AudioPlayer();

      Duration d;
      PlatformFile file = result.files.first;
      File myFile = File(file.path);
      audioPlayer.setUrl(file.path, isLocal: true);
      audioPlayer.onDurationChanged.listen((Duration duree) {
        d = duree;
        print('Max duration: $d');
        List<int> imageBytes = myFile.readAsBytesSync();
        print(base64Encode(imageBytes));
        String base64audio = base64Encode(imageBytes);

        Navigator.pushNamed(context, "/createpodcast2", arguments: {
          'file': base64audio,
          'duree': d.inMinutes.remainder(60).toString() +
              ":" +
              d.inSeconds.remainder(60).toString()
        });
      });
    } else {
      print("pas de fichier");
      // User canceled the picker
    }
  }
}

createPodcast(context) {
  Navigator.push(
    context,
    MaterialPageRoute(
        builder: (context) => CreatePodcastPage1(), fullscreenDialog: true),
  );
}

Future<String> getToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("apikey");
}
