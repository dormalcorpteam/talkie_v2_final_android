import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/data.dart';
import 'package:voicechanger/data/network/Model/listComments.dart';

import '../app_localizations.dart';

enum PlayerState { stopped, playing, paused }

class CommentWidget extends StatefulWidget {
  const CommentWidget(this.comment, this.id_user,{this.reset,this.refreshComment});
  final Comment comment;
  final VoidCallback reset;
  final VoidCallback refreshComment;
  final int id_user;

  @override
  _CommentWidgetState createState() => _CommentWidgetState();
}

class _CommentWidgetState extends State<CommentWidget> {
  bool like = false;

  bool dislike = false;
  int nbr_like = 0;
  int nbr_dislike = 0;
  Future<Data> data;

  AudioPlayer _audioPlayer;
  AudioPlayerState _audioPlayerState;
  Duration _duration;
  Duration _position;

  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  StreamSubscription _playerCompleteSubscription;
  StreamSubscription _playerErrorSubscription;
  StreamSubscription _playerStateSubscription;
  bool seekDone;

  get _isPlaying => widget.comment.playerState == PlayerState.playing;
  get _isPaused => widget.comment.playerState == PlayerState.paused;
  get _durationText =>
      "${_duration?.inMinutes?.remainder(60) ?? ''}:${(_duration?.inSeconds?.remainder(60)) ?? ''}";
  get _positionText =>
      "${_position?.inMinutes?.remainder(60) ?? ''}:${(_position?.inSeconds?.remainder(60)) ?? ''}";

  void initState() {
    super.initState();
    like = widget.comment.liked;
    dislike = widget.comment.disliked;
    nbr_like = widget.comment.nbrLike;
    nbr_dislike = widget.comment.nbrDislike;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          print(widget.comment.playerState);
          if (widget.comment.playerState == PlayerState.paused ||
              widget.comment.playerState == PlayerState.playing) {
            await _audioPlayer.release();
          }
          Navigator.pop(context);
          return false;
        },
        child: Container(
            margin: EdgeInsets.fromLTRB(8, 0, 8, 15),
            height: 110,
            decoration: new BoxDecoration(
              color: Color(0xffffffff),
              borderRadius: BorderRadius.circular(8),
              boxShadow: [
                BoxShadow(
                    color: Color(0x0d000000),
                    offset: Offset(0, 0),
                    blurRadius: 20,
                    spreadRadius: 0)
              ],
            ),
            child: Column(children: [
              ListTile(
                  leading: GestureDetector(
                      onTap: () {},
                      child: Container(
                        width: 30,
                        height: 30,
                        decoration: new BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(Endpoints.baseUrlImage +
                                  widget.comment.user.photo),
                            ),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0x33000000),
                                  offset: Offset(0, 0),
                                  blurRadius: 5,
                                  spreadRadius: 0)
                            ],
                            borderRadius: BorderRadius.circular(8)),
                      )),
                  title: Transform.translate(
                      offset: Offset(-16, 0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Flexible(
                                child: Text(widget.comment.user.nom,
                                    style: TextStyle(
                                      fontFamily: 'Helvetica',
                                      color: Color(0xff3F3C3C),
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    ))),
                            Padding(
                                padding: EdgeInsets.symmetric(vertical: 5),
                                child: VerticalDivider(
                                  color: Color(0xFFC4C4C4),
                                  thickness: 1.0,
                                )),
                            ElevatedButton.icon(
                                style: ElevatedButton.styleFrom(
                                  elevation: 0,
                                  primary: Colors.white,
                                  onPrimary: Colors.white,
                                  shadowColor: Colors.transparent,
                                ),
                                onPressed: () {
                                  if (dislike) {
                                    likeApi();
                                    disLikeApi();
                                  } else {
                                    likeApi();
                                  }
                                },
                                icon: InkWell(
                                    child: SvgPicture.asset(
                                        ("assets/icon/like.svg"),
                                        width: 19.44,
                                        height: 20,
                                        color: like
                                            ? Color(0xFF25A996)
                                            : Color(0xFF949494))),
                                label: InkWell(
                                    child: Text(
                                  nbr_like.toString(),
                                  style: TextStyle(
                                      color: like
                                          ? Color(0xFF25A996)
                                          : Color(0xFF949494),
                                      fontSize: 18,
                                      fontFamily: "Helvetica",
                                      fontWeight: FontWeight.normal),
                                ))),
                            ElevatedButton.icon(
                                style: ElevatedButton.styleFrom(
                                  elevation: 0,
                                  primary: Colors.white,
                                  onPrimary: Colors.white,
                                  shadowColor: Colors.transparent,
                                ),
                                onPressed: () {
                                  if (like) {
                                    disLikeApi();
                                    likeApi();
                                  } else {
                                    disLikeApi();
                                  }
                                },
                                icon: InkWell(
                                    child: SvgPicture.asset(
                                        ("assets/icon/dislike.svg"),
                                        width: 19.44,
                                        height: 20,
                                        color: dislike
                                            ? Color(0xFFE15656)
                                            : Color(0xFF949494))),
                                label: InkWell(
                                    child: Text(
                                  nbr_dislike.toString(),
                                  style: TextStyle(
                                      color: dislike
                                          ? Color(0xFFE15656)
                                          : Color(0xFF949494),
                                      fontSize: 18,
                                      fontFamily: "Helvetica",
                                      fontWeight: FontWeight.normal),
                                ))),
                             if(widget.id_user==widget.comment.user.id)
                             IconButton(
                          icon: Icon(
                            Icons.delete,
                            color: Color(0xFFC4C4C4),
                            size: 20.0,
                          ),
                          onPressed: ()  {
                          showAlertDialogDelete(context,widget.comment.id);
                            
                          }),
                          ]))),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  new InkWell(
                    onTap: () => {
                      initPlayer(),
                      print(_isPlaying),
                      if (_isPlaying) {_pause()} else {_play()}
                    },
                    child: Container(
                        margin: EdgeInsets.only(left: 15),
                        width: 30,
                        height: 30,
                        decoration: new BoxDecoration(
                          border: new Border.all(color: Colors.white, width: 2),
                          shape: BoxShape.circle,
                          color: Color(0xff25A996),
                          boxShadow: [
                            BoxShadow(
                                color: Color(0x2b2d2d2d),
                                offset: Offset(0, 8),
                                blurRadius: 20,
                                spreadRadius: 0)
                          ],
                        ),
                        child: _isPlaying
                            ? Padding(
                                padding: EdgeInsets.all(7),
                                child:
                                    SvgPicture.asset(("assets/icon/stop.svg")))
                            : Padding(
                                padding: EdgeInsets.all(7),
                                child: SvgPicture.asset(
                                    ("assets/icon/play.svg")))),
                  ),
                  Expanded(
                    child: Stack(children: [
                      SliderTheme(
                          data: SliderThemeData(
                              trackHeight: 4,
                              thumbShape:
                                  RoundSliderThumbShape(enabledThumbRadius: 0)),
                          child: Slider(
                            activeColor: Color(0xff25A996),
                            inactiveColor: Color(0xffC4C4C4),
                            onChanged: (v) {
                              if (_duration != null) {
                                final Position = v * _duration.inMilliseconds;
                                _audioPlayer.seek(
                                    Duration(milliseconds: Position.round()));
                              }
                            },
                            value: (_position != null &&
                                    _duration != null &&
                                    _position.inMilliseconds > 0 &&
                                    _position.inMilliseconds <
                                        _duration.inMilliseconds)
                                ? _position.inMilliseconds /
                                    _duration.inMilliseconds
                                : 0.0,
                          )),
                      Positioned(
                          top: 30,
                          left: 27,
                          child: Text(_position != null ? _positionText : "0:0",
                              style: TextStyle(
                                fontFamily: 'Helvetica',
                                color: Color(0xff949494),
                                fontSize: 11,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0.2708333333333333,
                              ))),
                      Positioned(
                          top: 30,
                          right: 27,
                          child: Text(
                              _duration != null
                                  ? _durationText
                                  : widget.comment.duree,
                              style: TextStyle(
                                fontFamily: 'Helvetica',
                                color: Color(0xff949494),
                                fontSize: 11,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0.2708333333333333,
                              ))),
                    ]),
                  ),
                ],
              ),
            ])));
  }

  likeApi() {
    getToken().then((token) => {
          data = ClientApi.likeComment(token, widget.comment.id),
          data
              .then((value) => {
                    setState(() {
                      if (like) {
                        nbr_like = nbr_like - 1;
                      } else {
                        nbr_like = nbr_like + 1;
                      }
                      like = !like;
                    })
                  })
              .catchError((error) => {print("erreur : " + error.toString())}),
        });
  }

  disLikeApi() {
    getToken().then((token) => {
          data = ClientApi.dislikeComment(token, widget.comment.id),
          data
              .then((value) => {
                    setState(() {
                      if (dislike) {
                        nbr_dislike = nbr_dislike - 1;
                      } else {
                        nbr_dislike = nbr_dislike + 1;
                      }
                      dislike = !dislike;
                    })
                  })
              .catchError((error) => {print("erreur : " + error.toString())}),
        });
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  Future<void> initPlayer() async {
    _audioPlayer = AudioPlayer(playerId: 'my_unique_playerId');
    seekDone = false;

    ///await _audioPlayer.setUrl(url);

// prepare the player with this audio but do not start playing
    await _audioPlayer.setReleaseMode(
        ReleaseMode.STOP); // set release mode so that it never releases

    _durationSubscription = _audioPlayer.onDurationChanged.listen((duration) {
      setState(() => {_duration = duration});
      print(duration);
    });

    _positionSubscription =
        _audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
              _position = p;
            }));

    _playerCompleteSubscription =
        _audioPlayer.onPlayerCompletion.listen((event) {
      _onComplete();
      setState(() {
        _position = null;
      });
    });

    _playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        widget.comment.playerState = PlayerState.stopped;
        _duration = Duration(seconds: 0);
        _position = Duration(seconds: 0);
      });
    });

    _audioPlayer.onPlayerStateChanged.listen((state) {
      setState(() {
        _audioPlayerState = state;
      });
    });

    _audioPlayer.onNotificationPlayerStateChanged.listen((state) {
      setState(() => _audioPlayerState = state);
    });
  }

  Future<int> _play() async {
    widget.reset();

    if (seekDone == true) {
      seekDone = false;
    }
    final playPosition = (_position != null &&
            _duration != null &&
            _position.inMilliseconds > 0 &&
            _position.inMilliseconds < _duration.inMilliseconds)
        ? _position
        : null;
    final result = await _audioPlayer.play(
        Endpoints.baseUrlImage + widget.comment.file,
        position: playPosition);

    if (result == 1) {
      setState(() => widget.comment.playerState = PlayerState.playing);
    }
    print(widget.comment.playerState);

    // default playback rate is 1.0
    // this should be called after _audioPlayer.play() or _audioPlayer.resume()
    // this can also be called everytime the user wants to change playback rate in the UI
    _audioPlayer.setPlaybackRate(playbackRate: 1.0);

    return result;
  }

  Future<int> _pause() async {
    final result = await _audioPlayer.pause();
    if (result == 1)
      setState(() => widget.comment.playerState = PlayerState.paused);
    return result;
  }

  Future<int> _stop() async {
    final result = await _audioPlayer.stop();
    if (result == 1) {
      setState(() {
        widget.comment.playerState = PlayerState.stopped;
        _position = Duration();
      });
    }
    return result;
  }

  void _onComplete() {
    setState(() => {
          widget.comment.playerState = PlayerState.stopped,
          seekDone = true,
        });
  }

  Future<int> _stopPlayer() async {
    final result = await _audioPlayer.stop();
    if (result == 1) {
      setState(() {
        widget.comment.playerState = PlayerState.stopped;
        _position = Duration();
      });
      _onComplete();
    }
    return result;
  }

  Future<int> getID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt("id");
  }
showAlertDialogDelete(BuildContext context,int id) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(AppLocalizations.of(context).translate('no')),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop(); // dismiss dialog
      },
    );
    Widget continueButton = FlatButton(
      child: Text(AppLocalizations.of(context).translate('yes')),
      onPressed: () async {
        deleteMessage(id);
        Navigator.of(context, rootNavigator: true).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(AppLocalizations.of(context).translate('confirmation_deletion_title')),
      content: Text(AppLocalizations.of(context).translate('confirmation_deletion_comment')),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  deleteMessage(int id){
                                getToken().then((token)=>{

    
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black,

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    ),
    
    data = ClientApi.deleteComment(token, id),
    data
        .then((value) async => {
              EasyLoading.dismiss(),
              EasyLoading.showSuccess(value.data),
             widget.refreshComment(),


                




             
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())}),
  });
                            }
}
