import 'dart:io';

import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Model/categories.dart';
import 'package:voicechanger/data/network/Model/data.dart';

class CreatePodcastPage2 extends StatefulWidget {
  CreatePodcast2 createState() => CreatePodcast2();
}

class CreatePodcast2 extends State<CreatePodcastPage2> {
  TextEditingController titreController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();
  String file;
  String duree;
  Future<Categories> categories;
  var listCategories = new List<Categorie>();
  Categorie categorie;
  final GlobalKey<TagsState> _tagStateKey = GlobalKey<TagsState>();
  List _items = new List();
  Future<Data> data;
  String language;

  void onValueChange() {
    setState(() {
      descriptionController.text;
    });
  }

  @override
  void initState() {
    super.initState();
    descriptionController.addListener(onValueChange);
    getToken().then((cleapi) => {getCategories(cleapi)});
    Devicelocale.currentLocale.then((value) => {
          setState(() {
            language = value.split(Platform.isAndroid ? '_' : '-')[0];
          })
        });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Map arguments;
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    if (arguments != null) {
      print(arguments['duree']);
      duree = arguments['duree'];
      print(arguments['file']);
      file = arguments['file'];
    }

    return WillPopScope(
        onWillPop: () async {
          FocusScope.of(context).requestFocus(new FocusNode());

          Navigator.pop(context);
          return false;
        },
        child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Scaffold(
              resizeToAvoidBottomInset: false,
              backgroundColor: Color(0xfff8f9f9),
              appBar: AppBar(
                                  backgroundColor: Color(0xff25a996),
                elevation: 0,
                centerTitle: true,
                title:
                    Text(AppLocalizations.of(context).translate('create_talk'),
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          color: Color(0xffffffff),
                          fontSize: 17,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        )),
              ),
              body: SingleChildScrollView(
                  child: Column(children: [
                Padding(
                    padding: EdgeInsets.fromLTRB(8, 35, 8, 0),
                    child: TextField(
                      controller: titreController,
                      decoration: InputDecoration(
                        hintText: AppLocalizations.of(context)
                            .translate('title_talk'),
                        labelText: AppLocalizations.of(context)
                            .translate('title_talk'),
                        labelStyle: TextStyle(
                          fontFamily: 'Ubuntu',
                          color: Color(0xff000000),
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                        ),
                        hintStyle: TextStyle(
                          fontFamily: 'Ubuntu',
                          color: Color(0xff3f3c3c),
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                        ),
                      ),
                    )),
                Padding(
                    padding: EdgeInsets.fromLTRB(8, 35, 8, 0),
                    child: TextField(
                      controller: descriptionController,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          hintText: "Description",
                          labelText: "Description",
                          counter: Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                  height: 38,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: new BoxDecoration(
                                      color: Color(0xffdddddd)),
                                  child: Padding(
                                      padding:
                                          EdgeInsets.fromLTRB(15, 7, 0, 11),
                                      child: Text(
                                          "${140 - descriptionController.text.length} " +
                                              AppLocalizations.of(context)
                                                  .translate(
                                                      'remaining_characters'),
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontFamily: 'Ubuntu',
                                            color: Color(0xff000000),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: 0,
                                          ))))),
                          labelStyle: TextStyle(
                            fontFamily: 'Ubuntu',
                            color: Color(0xff000000),
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          ),
                          hintStyle: TextStyle(
                            fontFamily: 'Ubuntu',
                            color: Color(0xff555555),
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          )),
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      minLines: 5,
                      maxLength: 140,
                    )),
                SizedBox(height: 15),
                if (categorie != null)
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(9, 0, 8, 0),
                          child: Text(
                              AppLocalizations.of(context)
                                  .translate('category'),
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                color: Color(0xff000000),
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )))),
                Padding(
                    padding: EdgeInsets.fromLTRB(9, 0, 8, 0),
                    child: DropdownButton(
                      hint: categorie == null
                          ? Text(
                              AppLocalizations.of(context)
                                  .translate('category'),
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                color: Color(0xff555555),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              ))
                          : Text(
                              (language == "fr")
                                  ? categorie.nom_fr
                                  : categorie.nom_en,
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                color: Color(0xff3f3c3c),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                      isExpanded: true,
                      iconSize: 24.0,
                      items: listCategories.map(
                        (val) {
                          return DropdownMenuItem<Categorie>(
                            value: val,
                            child: Text(
                                (language == "fr") ? val.nom_fr : val.nom_en),
                          );
                        },
                      ).toList(),
                      onChanged: (val) {
                        setState(
                          () {
                            categorie = val;
                          },
                        );
                      },
                    )),
                Padding(
                    padding: EdgeInsets.fromLTRB(8, 25, 8, 0),
                    child: Tags(
                      key: _tagStateKey,

                      textField: TagsTextField(
                        hintText: "Hashtag",
                        hintTextColor: Color(0xff3f3c3c),
                        width: MediaQuery.of(context).size.width,
                        textStyle: TextStyle(
                          fontFamily: 'Ubuntu',
                          color: Color(0xff3f3c3c),
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                        ),
                        constraintSuggestion: false,
                        onSubmitted: (String str) {
                          // Add item to the data source.
                          setState(() {
                            // required
                            _items.add(str);
                          });
                        },
                      ),
                      itemCount: _items.length, // required
                      itemBuilder: (int index) {
                        final item = _items[index];

                        return ItemTags(
                          elevation: 0, textActiveColor: Color(0xff3f3c3c),
                          activeColor: Colors.white,
                          borderRadius: BorderRadius.circular(4),
                          border: new Border.all(
                              color: Color(0xff949494), width: 1),

                          // Each ItemTags must contain a Key. Keys allow Flutter to
                          // uniquely identify widgets.
                          key: Key(index.toString()),
                          index: index, // required
                          title: "#" + item,
                          textStyle: TextStyle(
                            fontFamily: 'Ubuntu',
                            color: Color(0xff3f3c3c),
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.normal,
                          ),
                          combine: ItemTagsCombine.withTextBefore,
                          removeButton: ItemTagsRemoveButton(
                            icon: Icons.close,
                            size: 20,
                            color: Color(0xff949494),
                            onRemoved: () {
                              // Remove the item from the data source.
                              setState(() {
                                // required
                                _items.removeAt(index);
                              });
                              //required
                              return true;
                            },
                          ), // OR null,
                        );
                      },
                    )),
              ])),
              bottomNavigationBar: BottomAppBar(
                color: Colors.transparent,
                child: Padding(
                    padding: EdgeInsets.fromLTRB(38, 30, 37, 30),
                    child: ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width,
                        height: 45,
                        child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Color(0xff25a996)),
                              borderRadius: BorderRadius.circular(4)),
                          color: Colors.white,
                          child: Text(
                              AppLocalizations.of(context).translate('publish'),
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                color: Color(0xff25a996),
                                fontSize: 15,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )),
                          onPressed: () {
                            if(descriptionController.text == '' || titreController.text == '' || _items.isEmpty  || categorie==null) {
                              EasyLoading.instance.loadingStyle =
                                  EasyLoadingStyle.custom;
                              EasyLoading.instance.backgroundColor = Colors.red;
                              EasyLoading.instance.indicatorColor =
                                  Colors.white;
                              EasyLoading.instance.progressColor = Colors.white;
                              EasyLoading.instance.textColor = Colors.white;
                              EasyLoading.instance.displayDuration =
                                  const Duration(milliseconds: 2000);
                              EasyLoading.showError(AppLocalizations.of(context).translate('empty_fields'));
                             
                            }
                            else{
                            print(_items.toString());
                            print(titreController.text);
                            print(descriptionController.text);
                            print(categorie.nom_fr);
                          getToken().then((token) {
                              createPodcast(
                                  token,
                                  titreController.text,
                                  categorie.id,
                                  _items,
                                  file,
                                  duree,
                                  descriptionController.text);
                            });}
                          },
                        ))),
                elevation: 0,
              ),
            )));
// Allows you to get a list of all the ItemTags
  }

  getCategories(String token) {
    // here you "register" to get "notifications" when the getEmail method is done.
    categories = ClientApi.getListeCategories(token);
    categories
        .then((value) => {
              setState(() {
                print(value.data);
                listCategories = value.data;
              })
            })
        .catchError((error) => {print("erreur : " + error.toString())});
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  createPodcast(String token, String nom, int category, var list, file, duree,
      description) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    data = ClientApi.createPodcast(
        token, nom, category, list, file, duree, description);
    data
        .then((value) => {
              EasyLoading.dismiss(),
              EasyLoading.showSuccess(
                  AppLocalizations.of(context).translate('talk_created')),
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/home', (Route<dynamic> route) => false,
                  arguments: {'selectedindex': 2}),
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }
}
