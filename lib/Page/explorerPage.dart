import 'dart:io';

import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/Page/ExplorerSearchActivePage.dart';
import 'package:voicechanger/Page/discussionPage.dart';
import 'package:voicechanger/Page/myWorldPage.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/categories.dart';

import 'AppBarHeader.dart';
import 'Footer.dart';

class ExplorerPage extends StatefulWidget {
  Explorer createState() => Explorer();
}

class Explorer extends State<ExplorerPage> {
  int _selectedIndex = 1;
  Widget appBar = AppBarHeader(
      name: '', buttonback: true, actionTalk: false, actionmessage: false);
  Future<Categories> categories;
  var listCategories = new List<Categorie>();
  String language;
  @override
  void initState() {
    super.initState();
    getToken().then((cleapi) => {getCategories(cleapi)});
    Devicelocale.currentLocale.then((value) => {
          setState(() {
            language = value.split(Platform.isAndroid ? '_' : '-')[0];
          })
        });
    SchedulerBinding.instance.addPostFrameCallback((_) {
      appBar = AppBarHeader(
          name: AppLocalizations.of(context).translate('explore'),
          buttonback: true,
          actionTalk: false,
          actionmessage: false);
    });
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      if (index == 0) {
        appBar = AppBarHeader(
            name: "Discussions",
            buttonback: false,
            actionTalk: false,
            actionmessage: true);
      }

      if (index == 1) {
        appBar = AppBarHeader(
            name: AppLocalizations.of(context).translate('explore'),
            buttonback: true,
            actionTalk: false,
            actionmessage: false);
      }
      if (index == 2) {
        appBar = null;
      }
    });
  }

  getCategories(String token) {
    // here you "register" to get "notifications" when the getEmail method is done.
    categories = ClientApi.getListeCategories(token);
    categories
        .then((value) => {
              setState(() {
                print(value.data);
                listCategories = value.data;
              })
            })
        .catchError((error) => {print("erreur : " + error.toString())});
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _widgetOptions = <Widget>[
      DiscussionPage(),
      Scaffold(
          body: Container(
              child: Column(children: [
        Padding(
            padding: EdgeInsets.fromLTRB(10, 15, 8, 0),
            child: Theme(
                data:
                    Theme.of(context).copyWith(splashColor: Colors.transparent),
                child: Container(
                  height: 36,
                  child: TextField(
                      onTap: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        Navigator.push(context, MaterialPageRoute(builder: (_) {
                          return ExplorerSearchActivePage();
                        }));
                      },
                      style: TextStyle(
                        fontFamily: 'SFProText',
                        color: Color(0x7f9b9b9b),
                        fontSize: 17,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        letterSpacing: -0.408,
                      ),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0x129b9b9b),
                          hintStyle: TextStyle(
                            fontFamily: 'SFProText',
                            color: Color(0x7f9b9b9b),
                            fontSize: 17,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: -0.408,
                          ),
                          hintText:
                              AppLocalizations.of(context).translate('search'),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 0, horizontal: 15.0),
                          prefixIcon:
                              Icon(Icons.search, color: Color(0xff949494)),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                BorderSide(width: 1, color: Color(0x129b9b9b)),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                BorderSide(width: 1, color: Color(0x129b9b9b)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                BorderSide(width: 1, color: Color(0x129b9b9b)),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                BorderSide(width: 1, color: Color(0x129b9b9b)),
                          ))),
                ))),
        Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.fromLTRB(10, 15, 74, 0),
              child:
                  new Text(AppLocalizations.of(context).translate('category'),
                      style: TextStyle(
                        fontFamily: 'Salome',
                        color: Color(0xff3f3c3c),
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0.4166666666666666,
                      )),
            )),
        Expanded(
            child: Padding(
                padding: EdgeInsets.fromLTRB(10, 15, 9, 0),
                child: GridView.builder(
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    // number of items per row
                    crossAxisCount: 3,
                    // vertical spacing between the items
                    mainAxisSpacing: 46.0,
                    // horizontal spacing between the items
                    crossAxisSpacing: 10.0,
                    childAspectRatio: 1,
                  ),
                  itemBuilder: (BuildContext context, int i) {
                    return CategorieWidget(listCategories[i]);
                  },
                  itemCount: listCategories.length,
                ))),
      ]))),
      MyWorldPage(),
    ];

    return Scaffold(
        appBar: appBar,
        body: Container(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: Footer(_selectedIndex, _onItemTapped));
  }

  CategorieWidget(Categorie categorie) {
    return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, "/detailCategorie", arguments: {
            'id': categorie.id,
            'nom': (language == "fr") ? categorie.nom_fr : categorie.nom_en
          });
        },
        child: Column(children: <Widget>[
          Expanded(
              child: Container(
            width: 77,
            height: 77,
            child: Padding(
                padding: EdgeInsets.all(15),
                child: SvgPicture.network(
                  Endpoints.baseUrlImage + categorie.image,
                  placeholderBuilder: (BuildContext context) => Container(
                      padding: const EdgeInsets.all(15.0),
                      child: const CupertinoActivityIndicator()),
                )),
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              color: Color(0xffffffff),
              boxShadow: [
                BoxShadow(
                    color: Color(0x0c000000),
                    offset: Offset(0, 0),
                    blurRadius: 20,
                    spreadRadius: 0)
              ],
            ),
          )),
          SizedBox(
            height: 5,
          ),
          Text((language == "fr") ? categorie.nom_fr : categorie.nom_en,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xff3f3c3c),
                fontSize: 13,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.normal,
                letterSpacing: 0.1559999999999999,
              ))
        ]));
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }
}
