import 'dart:async';
import 'dart:ui';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Model/call.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:agora_rtc_engine/rtc_engine.dart' as rtc;

import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:voicechanger/data/network/Model/data.dart';

class CallScreen extends StatefulWidget {
  final Call call;

  CallScreen({
    @required this.call,
  });

  @override
  _CallScreenState createState() => _CallScreenState();
}

class _CallScreenState extends State<CallScreen> {
  //final CallMethods callMethods = CallMethods();

  //UserProvider userProvider;
  StreamSubscription callStreamSubscription;
  static AudioCache musicCache;
  static AudioPlayer instance;

  static final _users = <int>[];
  final _infoStrings = <String>[];
  bool muted = false;
  bool speakerMode = false;
  rtc.RtcEngine _engine;
  Timer _timer;
  int seconds = 0;
  int minutes = 0;
  int hours = 0;

  String duree;
  Future<Data> data;

  @override
  void initState() {
    super.initState();
    // addPostFrameCallback();
    playLoopedMusic();
    initializeAgora();
  }

  void playLoopedMusic() async {
    musicCache = AudioCache(prefix: "assets/audio/");
    instance = await musicCache.loop("ringring.mp3");
  }

  void pauseMusic() {
    if (instance != null) {
      instance.pause();
    }
  }

  String formatTime(int timeNum) {
    return timeNum < 10 ? "0" + timeNum.toString() : timeNum.toString();
  }

  void cancelTimer() {
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    }
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (seconds < 0) {
            timer.cancel();
          } else {
            seconds = seconds + 1;
            if (seconds > 59) {
              minutes += 1;
              seconds = 0;
              if (minutes > 59) {
                hours += 1;
                minutes = 0;
              }
            }
          }
        },
      ),
    );
  }

  Future<void> initializeAgora() async {
    if (Endpoints.APP_ID.isEmpty) {
      setState(() {
        _infoStrings.add(
          'APP_ID missing, please provide your APP_ID in settings.dart',
        );
        _infoStrings.add('Agora Engine is not starting');
      });
      return;
    }

    await _initAgoraRtcEngine();
    _addAgoraEventHandlers();
    await _engine.enableWebSdkInteroperability(true);
    rtc.VideoEncoderConfiguration configuration =
        rtc.VideoEncoderConfiguration();
    configuration.dimensions = rtc.VideoDimensions(1920, 1080);
    await _engine.setVideoEncoderConfiguration(configuration);

    await _engine.joinChannel(null, widget.call.channelId, null, 0);
    print("channnnelid " + widget.call.channelId);
  }

  /*addPostFrameCallback() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      userProvider = Provider.of<UserProvider>(context, listen: false);

      callStreamSubscription = callMethods
          .callStream(uid: userProvider.getUser.uid)
          .listen((DocumentSnapshot ds) {
        // defining the logic
        switch (ds.data) {
          case null:
            // snapshot is null which means that call is hanged and documents are deleted
            Navigator.pop(context);
            break;

          default:
            break;
        }
      });
    });
  }*/

  /// Create agora sdk instance and initialize
  Future<void> _initAgoraRtcEngine() async {
    _engine = await rtc.RtcEngine.create(Endpoints.APP_ID);
    await _engine.disableVideo();
    await _engine.setChannelProfile(rtc.ChannelProfile.Communication);
  }

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    _engine.setEventHandler(rtc.RtcEngineEventHandler(error: (code) {
      setState(() {
        final info = 'onError: $code';
        _infoStrings.add(info);
      });
    }, joinChannelSuccess: (channel, uid, elapsed) {
      setState(() {
        final info = 'onJoinChannel: $channel, uid: $uid';
        _infoStrings.add(info);
      });
    }, leaveChannel: (stats) {
      setState(() {
        _infoStrings.add('onLeaveChannel');
        _users.clear();
      });
    }, userJoined: (uid, elapsed) {
      print("aaaa");
      pauseMusic();
      startTimer();
      setState(() {
        final info = 'userJoined: $uid';
        _infoStrings.add(info);
        _users.add(uid);
      });
    }, userOffline: (uid, elapsed) {
      // Navigator.pop(context);
      print("quit");
      setState(() {
        final info = 'userOffline: $uid';
        _infoStrings.add(info);
        _users.remove(uid);
      });
    }, firstRemoteVideoFrame: (uid, width, height, elapsed) {
      setState(() {
        final info = 'firstRemoteVideo: $uid ${width}x $height';
        _infoStrings.add(info);
      });
    }));
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final List<StatefulWidget> list = [];
    list.add(RtcLocalView.SurfaceView());

    _users.forEach((int uid) => list.add(RtcRemoteView.SurfaceView(uid: uid)));
    return list;
  }

  /// Video view wrapper
  Widget _videoView(view) {
    return Expanded(child: Container(child: view));
  }

  /// Video view row wrapper
  Widget _expandedVideoRow(List<Widget> views) {
    final wrappedViews = views.map<Widget>(_videoView).toList();
    return Expanded(
      child: Row(
        children: wrappedViews,
      ),
    );
  }

  /// Video layout wrapper
  Widget _viewRows() {
    final views = _getRenderViews();
    switch (views.length) {
      case 1:
        return Container(
            child: Column(
          children: <Widget>[_videoView(views[0])],
        ));
      case 2:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow([views[0]]),
            _expandedVideoRow([views[1]])
          ],
        ));
      case 3:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 3))
          ],
        ));
      case 4:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 4))
          ],
        ));
      default:
    }
    return Container();
  }

  /// Info panel to show logs
  Widget _panel() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 48),
      alignment: Alignment.bottomCenter,
      child: FractionallySizedBox(
        heightFactor: 0.5,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 48),
          child: ListView.builder(
            reverse: true,
            itemCount: _infoStrings.length,
            itemBuilder: (BuildContext context, int index) {
              if (_infoStrings.isEmpty) {
                return null;
              }
              return Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 3,
                  horizontal: 10,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                          horizontal: 5,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.yellowAccent,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          _infoStrings[index],
                          style: TextStyle(color: Colors.blueGrey),
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void _onToggleMute() {
    setState(() {
      muted = !muted;
    });
    _engine.muteLocalAudioStream(muted);
  }

  void _onEnableSpeakerphone() {
    setState(() {
      speakerMode = !speakerMode;
    });
    _engine.setEnableSpeakerphone(speakerMode);
  }

  /// Toolbar layout
  Widget _toolbar() {
    return Container(
      alignment: Alignment.bottomCenter,
      padding: const EdgeInsets.symmetric(vertical: 48),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          /*   RawMaterialButton(
            onPressed: _onToggleMute,
            child: Icon(
              muted ? Icons.mic : Icons.mic_off,
              color: muted ? Colors.white : Colors.blueAccent,
              size: 20.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor: muted ? Colors.blueAccent : Colors.white,
            padding: const EdgeInsets.all(12.0),
          ),*/

          /* RawMaterialButton(
            onPressed: _onSwitchCamera,
            child: Icon(
              Icons.switch_camera,
              color: Colors.blueAccent,
              size: 20.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.white,
            padding: const EdgeInsets.all(12.0),
          )*/
        ],
      ),
    );
  }

  @override
  void dispose() {
    // clear users
    _users.clear();
    // destroy sdk
    _engine?.leaveChannel();
    _engine?.destroy();
    pauseMusic();
    super.dispose();
    cancelTimer();
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  endCall() {
    getToken().then((token) => {
          data = ClientApi.endCall(token, widget.call.receiverId,
              widget.call.channelId, widget.call.call_id),
          data
              .then((value) => {
                    EasyLoading.dismiss(),
                    Navigator.pop(context),
                  })
              .catchError((error) => {
                    EasyLoading.dismiss(),
                    Fluttertoast.showToast(
                        msg: error.toString(),
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.CENTER,
                        backgroundColor: Colors.black,
                        textColor: Colors.white,
                        fontSize: 16.0),
                    print("erreur : " + error.toString())
                  }),
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF757575),
      body: Column(
        children: <Widget>[
          SizedBox(height: 102),
          Center(
              child: Text(
            widget.call.receiverName,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              color: Colors.white,
              fontSize: 26,
              fontFamily: "SFProText",
            ),
          )),
          SizedBox(height: 57),
          Container(
            width: 195,
            height: 195,
            decoration: new BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(
                    Endpoints.baseUrlImage + widget.call.receiverPhoto),
              ),
              color: Colors.grey[400],
              shape: BoxShape.circle,
            ),
          ),
          SizedBox(height: 21),
          Text(
            AppLocalizations.of(context).translate('end_encrypted'),
            style: TextStyle(
              fontFamily: "SFProText",
              fontWeight: FontWeight.w400,
              fontSize: 12,
              color: Colors.white,
            ),
          ),
          SizedBox(height: 10),
          Text(
            (seconds != 0 || minutes != 0)
                ? formatTime(minutes) + ":" + formatTime(seconds)
                : "",
            style: TextStyle(
              fontFamily: "SFProText",
              fontWeight: FontWeight.w400,
              fontSize: 12,
              color: Colors.white,
            ),
          ),
        ],
      ),
      bottomNavigationBar: Padding(
          padding: EdgeInsets.only(bottom: 62),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment:
                CrossAxisAlignment.end, //Center Row contents vertically,
            children: <Widget>[
              Expanded(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment
                          .end, //Center Column contents vertically,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                    SizedBox.fromSize(
                        size: Size(36, 36), // button width and height
                        child: ClipOval(
                          child: Material(
                            color: Color(0xffEEEEEE), // button color
                            child: InkWell(
                                splashColor: Colors.green, // splash color
                                onTap: () {
                                  _onEnableSpeakerphone();
                                }, // button pressed
                                child: new Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      SvgPicture.asset(
                                          ("assets/icon/volume-up.svg"),
                                          color: Colors.grey[800]),
                                      if (!speakerMode)
                                        SvgPicture.asset(
                                            ("assets/icon/line.svg"),
                                            color: Colors.grey[800])
                                    ])
                                // icon

                                ),
                          ),
                        )),
                    SizedBox(height: 27),
                    Text(AppLocalizations.of(context).translate('speaker'),
                        style: TextStyle(
                          fontFamily: "SFProText",
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                          color: Colors.white,
                        )),
                  ])),
              Padding(
                  padding: EdgeInsets.only(bottom: 20),
                  child: SizedBox.fromSize(
                      size: Size(68, 68), // button width and height
                      child: ClipOval(
                        child: Material(
                          color: Color(0xffFE3D2F), // button color
                          child: InkWell(
                              splashColor: Colors.green, // splash color
                              onTap: () {
                                endCall();
                              }, // button pressed
                              child: Padding(
                                  padding: EdgeInsets.symmetric(
                                    vertical: 12,
                                    horizontal: 10,
                                  ),
                                  child: SvgPicture.asset(
                                      ("assets/icon/end-call.svg"),
                                      color: Colors.white))),
                        ),
                      ))),
              Expanded(
                  child: Container(
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment
                              .end, //Center Column contents vertically,

                          children: <Widget>[
                    SizedBox.fromSize(
                        size: Size(36, 36), // button width and height
                        child: ClipOval(
                          child: Material(
                            color: Color(0xffEEEEEE), // button color
                            child: InkWell(
                              splashColor: Colors.green, // splash color
                              onTap: () {
                                _onToggleMute();
                              }, // button pressed
                              child: new Stack(
                                  alignment: Alignment.center,
                                  children: <Widget>[
                                    SvgPicture.asset(
                                        ("assets/icon/dictation-mic-filled.svg"),
                                        color: Colors.grey[800]),
                                    if (muted)
                                      SvgPicture.asset(("assets/icon/line.svg"),
                                          color: Colors.grey[800])
                                  ]),

                              // icon
                            ),
                          ),
                        )),
                    SizedBox(height: 27),
                    Text(AppLocalizations.of(context).translate('silence'),
                        style: TextStyle(
                          fontFamily: "SFProText",
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                          color: Colors.white,
                        )),
                  ]))),
            ],
          )),

      /*Center(
        child: Stack(
          children: <Widget>[
            Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(vertical: 100),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Appel en cours...",
              style: TextStyle(
                fontSize: 30,
              ),
            ),
            SizedBox(height: 50),
             Container(
              width: 52,
              height: 52,
              decoration: new BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        offset: Offset(0, 0),
                        blurRadius: 5,
                        spreadRadius: 0)
                  ],
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(Endpoints.baseUrlImage+widget.call.receiverPhoto),
                  ),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8)),
            ),
            SizedBox(height: 15),
            Text(
              widget.call.receiverName,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            SizedBox(height: 75),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                    RawMaterialButton(
            onPressed: () => { endCall(),                         
},
            child: Icon(
              Icons.call_end,
              color: Colors.white,
              size: 35.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.redAccent,
            padding: const EdgeInsets.all(15.0),
          ),
                SizedBox(width: 25),
             RawMaterialButton(
            onPressed: _onToggleMute,
            child: Icon(
              muted ? Icons.mic : Icons.mic_off,
              color: muted ? Colors.white : Colors.blueAccent,
              size: 20.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor: muted ? Colors.blueAccent : Colors.white,
            padding: const EdgeInsets.all(12.0),
          )
              ],
            ),
          ],
        ),
      ),
          ],
        ),
      ),*/
    );
  }
}
