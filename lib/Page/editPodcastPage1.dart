import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'dart:io' as io;
import 'dart:convert';

import 'package:file/file.dart';
import 'package:file/local.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/DetailPodcast.dart';
import 'package:voicechanger/data/network/Model/categories.dart';

enum PlayerState { stopped, playing, paused }

class EditPodcastPage1 extends StatefulWidget {
  EditPodcast1 createState() => EditPodcast1();
}

class EditPodcast1 extends State<EditPodcastPage1> {
  @override
  Widget build(BuildContext context) {
    Map arguments;
    int id;
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    if (arguments != null) {
      id = arguments['id'];
    }
    return EditTalks(id: id);
  }
}

class EditTalks extends StatefulWidget {
  final int id;
  final LocalFileSystem localFileSystem;
  EditTalks({Key key, this.id, localFileSystem})
      : this.localFileSystem = localFileSystem ?? LocalFileSystem();

  @override
  _EditPodcastPage1State createState() => _EditPodcastPage1State();
}

class _EditPodcastPage1State extends State<EditTalks> {
  String filechange;
  String base64Image;
  Duration _duration;
  Duration _position;
  FlutterAudioRecorder _recorder;
  Recording _current;
  RecordingStatus _currentStatus = RecordingStatus.Unset;
  bool recorder = false;
  String timer;
  File file;
  Timer _timer;
  String duree;
  int start = 60;
  AudioPlayer _audioPlayer;
  AudioPlayerState _audioPlayerState;
  Future<DetailPodcast> detailPodcast;
  DetailPodcastData detailPodcastData;
  bool isLocale = true;
  get _durationText =>
      "${_duration?.inMinutes?.remainder(60) ?? ''}:${(_duration?.inSeconds?.remainder(60)) ?? ''}";
  get _positionText =>
      "${_position?.inMinutes?.remainder(60) ?? ''}:${(_position?.inSeconds?.remainder(60)) ?? ''}";

  PlayerState _playerState = PlayerState.stopped;
  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  StreamSubscription _playerCompleteSubscription;
  StreamSubscription _playerErrorSubscription;
  StreamSubscription _playerStateSubscription;
  bool seekDone;
  void startTimer(int timerDuration) {
    setState(() {
      start = timerDuration;
    });

    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (start < 1) {
            timer.cancel();
            _stop();
          } else {
            start = start - 1;
          }
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _init();
    getToken().then((cleapi) => {getDetailPodcast(cleapi, widget.id)});
  }

  @override
  dispose() {
    super.dispose();

    if (_playerState == PlayerState.playing) {
      _audioPlayer.stop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          if (_playerState == PlayerState.paused ||
              _playerState == PlayerState.playing) {
            await _audioPlayer.release();
          }
          if (status() == 'Start' || status() == 'Pause') {
            EasyLoading.showError(
                AppLocalizations.of(context).translate('stop_recorder'));
          } else {
            Navigator.pop(context);
          }
          return false;
        },
        child: Container(
          color: Colors.black,
          padding: EdgeInsets.only(top: 54),
          child: Material(
              color: Color(0xff25a996),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  topRight: Radius.circular(10.0)),
              child: Stack(children: <Widget>[
                Column(children: <Widget>[
                  Align(
                      alignment: Alignment.topLeft,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 24.0,
                          ),
                          onPressed: () async {
                            if (_playerState == PlayerState.paused ||
                                _playerState == PlayerState.playing) {
                              await _audioPlayer.release();
                            }
                            if (status() == 'Start' || status() == 'Pause') {
                              EasyLoading.showError(AppLocalizations.of(context)
                                  .translate('stop_recorder'));
                            } else {
                              Navigator.pop(context);
                            }
                          })),
                  SizedBox(
                    height: 20,
                  ),
                  Text(AppLocalizations.of(context).translate('modify_talk'),
                      style: TextStyle(
                        fontFamily: 'Salome',
                        color: Color(0xffffffff),
                        fontSize: 30,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0.625,
                      )),
                ]),
                Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                      if (status() == 'Init')
                        Row(children: [
                          Expanded(
                              child: IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    if (_playerState == PlayerState.playing) {
                                      _pause();
                                    } else {
                                      _play(
                                          Endpoints.baseUrlImage +
                                              detailPodcastData.file,
                                          false);
                                    }
                                  },
                                  icon: (_playerState == PlayerState.playing)
                                      ? SvgPicture.asset(
                                          ("assets/icon/stop.svg"))
                                      : SvgPicture.asset(
                                          ("assets/icon/play.svg")))),
                          Expanded(
                              child: SliderTheme(
                                  data: SliderThemeData(
                                      trackHeight: 4,
                                      thumbShape: RoundSliderThumbShape(
                                          enabledThumbRadius: 0)),
                                  child: Slider(
                                    activeColor: Colors.white,
                                    inactiveColor: Color(0xffc6c6c6),
                                    onChanged: (v) {
                                      if (_duration != null) {
                                        final Position =
                                            v * _duration.inMilliseconds;
                                        _audioPlayer.seek(Duration(
                                            milliseconds: Position.round()));
                                      }
                                    },
                                    value: (_position != null &&
                                            _duration != null &&
                                            _position.inMilliseconds > 0 &&
                                            _position.inMilliseconds <
                                                _duration.inMilliseconds)
                                        ? _position.inMilliseconds /
                                            _duration.inMilliseconds
                                        : 0.0,
                                  )),
                              flex: 7),
                          Expanded(
                            child: Text("${detailPodcastData?.duree} s",
                                style: TextStyle(
                                  fontFamily: 'Ubuntu',
                                  color: Color(0xffffffff),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0.4166666666666667,
                                )),
                          ),
                        ]),
                      if (_playerState == PlayerState.paused ||
                          _playerState == PlayerState.playing)
                        Text("$_positionText s /$_durationText s",
                            style: TextStyle(
                              fontFamily: 'Ubuntu',
                              color: Color(0xffffffff),
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0.4166666666666667,
                            )),
                      if (status() == 'Start')
                        Text("$timer s / 60s",
                            style: TextStyle(
                              fontFamily: 'Ubuntu',
                              color: Color(0xffffffff),
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0.4166666666666667,
                            )),
                      if (status() == 'Init')
                        IconButton(
                            iconSize: 132,
                            onPressed: () {
                              _start();
                            },
                            icon: SvgPicture.asset(
                              "assets/icon/group-2-copy-9.svg",
                            )),
                      if (status() == 'Stop')
                        Row(children: [
                          Expanded(
                              child: IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    if (_playerState == PlayerState.playing) {
                                      _pause();
                                    } else {
                                      _play(_current.path, true);
                                    }
                                  },
                                  icon: (_playerState == PlayerState.playing)
                                      ? SvgPicture.asset(
                                          ("assets/icon/stop.svg"))
                                      : SvgPicture.asset(
                                          ("assets/icon/play.svg")))),
                          Expanded(
                              child: SliderTheme(
                                  data: SliderThemeData(
                                      trackHeight: 4,
                                      thumbShape: RoundSliderThumbShape(
                                          enabledThumbRadius: 0)),
                                  child: Slider(
                                    activeColor: Colors.white,
                                    inactiveColor: Color(0xffc6c6c6),
                                    onChanged: (v) {
                                      if (_duration != null) {
                                        final Position =
                                            v * _duration.inMilliseconds;
                                        _audioPlayer.seek(Duration(
                                            milliseconds: Position.round()));
                                      }
                                    },
                                    value: (_position != null &&
                                            _duration != null &&
                                            _position.inMilliseconds > 0 &&
                                            _position.inMilliseconds <
                                                _duration.inMilliseconds)
                                        ? _position.inMilliseconds /
                                            _duration.inMilliseconds
                                        : 0.0,
                                  )),
                              flex: 8),
                          Expanded(
                              child: IconButton(
                                  iconSize: 23,
                                  onPressed: () {
                                    cancelMessage();
                                  },
                                  icon: SvgPicture.asset(
                                      ("assets/icon/carbage.svg")))),
                        ]),
                    ])),
                if (status() == 'Start')
                  Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: Align(
                          alignment: Alignment.bottomCenter,
                          child: GestureDetector(
                              onTap: () {
                                _stop();
                              },
                              child: Container(
                                  width: 54,
                                  height: 54,
                                  decoration: new BoxDecoration(
                                    border: new Border.all(
                                        color: Colors.white, width: 2),
                                    shape: BoxShape.circle,
                                    color: Color(0xff25a996),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x2b2d2d2d),
                                          offset: Offset(0, 0),
                                          blurRadius: 0,
                                          spreadRadius: 5)
                                    ],
                                  ),
                                  child: Padding(
                                      padding: EdgeInsets.all(14),
                                      child: SvgPicture.asset(
                                          ("assets/icon/stop.svg"))))))),
                if (status() == "Stop" || status() == "Init")
                  Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: Align(
                          alignment: Alignment.bottomRight,
                          child: new GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                Navigator.pushNamed(context, "/editpodcast2",
                                    arguments: {
                                      'duree': duree,
                                      'file': base64Image,
                                      'detailpocast': detailPodcastData,
                                    });
                                print(duree);
                                print(base64Image);
                              },
                              child: Container(
                                  width: 54,
                                  height: 54,
                                  decoration: new BoxDecoration(
                                    border: new Border.all(
                                        color: Colors.white, width: 2),
                                    shape: BoxShape.circle,
                                    color: Color(0xff25a996),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x2b2d2d2d),
                                          offset: Offset(0, 8),
                                          blurRadius: 20,
                                          spreadRadius: 0)
                                    ],
                                  ),
                                  child: Padding(
                                      padding: EdgeInsets.all(11),
                                      child: SvgPicture.asset(
                                          ("assets/icon/arrow-right.svg"))))))),
              ])),
        ));
  }

  Future<int> _stopPlayer() async {
    final result = await _audioPlayer.stop();
    if (result == 1) {
      setState(() {
        _playerState = PlayerState.stopped;
        _position = Duration();
      });
      _onComplete();
    }
    return result;
  }

  Future<int> _pause() async {
    final result = await _audioPlayer.pause();
    if (result == 1) setState(() => _playerState = PlayerState.paused);
    return result;
  }

  cancelMessage() {
    if (_playerState == PlayerState.paused ||
        _playerState == PlayerState.playing) {
      _stopPlayer();
    }

    _init();
    if (_timer != null) {
      _timer.cancel();
    }
  }

  _init() async {
    timer = "";

    start = 60;

    try {
      if (await FlutterAudioRecorder.hasPermissions) {
        String customPath = '/flutter_audio_recorder_';
        io.Directory appDocDirectory;
//        io.Directory appDocDirectory = await getApplicationDocumentsDirectory();
        if (io.Platform.isIOS) {
          appDocDirectory = await getApplicationDocumentsDirectory();
        } else {
          appDocDirectory = await getExternalStorageDirectory();
        }

        // can add extension like ".mp4" ".wav" ".m4a" ".aac"
        customPath = appDocDirectory.path +
            customPath +
            DateTime.now().millisecondsSinceEpoch.toString();

        // .wav <---> AudioFormat.WAV
        // .mp4 .m4a .aac <---> AudioFormat.AAC
        // AudioFormat is optional, if given value, will overwrite path extension when there is conflicts.
        _recorder =
            FlutterAudioRecorder(customPath, audioFormat: AudioFormat.WAV);

        await _recorder.initialized;
        // after initialization
        var current = await _recorder.current(channel: 0);
        print(current);
        // should be "Initialized", if all working fine
        setState(() {
          _current = current;
          _currentStatus = current.status;
          filechange = null;
          base64Image = null;
          duree = null;
        });
      } else {
        Scaffold.of(context).showSnackBar(
            new SnackBar(content: new Text("You must accept permissions")));
      }
    } catch (e) {
      print(e);
    }
  }

  _start() async {
    try {
      startTimer(60);
      await _recorder.start();
      var recording = await _recorder.current(channel: 0);
      setState(() {
        _current = recording;
      });

      const tick = const Duration(seconds: 1);
      new Timer.periodic(tick, (Timer t) async {
        if (_currentStatus == RecordingStatus.Stopped) {
          t.cancel();
        }

        var current = await _recorder.current(channel: 0);
        // print(current.status);
        setState(() {
          _current = current;
          _currentStatus = _current.status;
          recorder = true;
          timer = _current?.duration.inMinutes.remainder(60).toString() +
              ":" +
              current?.duration.inSeconds.remainder(60).toString();
        });
      });
    } catch (e) {
      print(e);
    }
  }

  _stop() async {
    var result = await _recorder.stop();
    print("Stop recording: ${result.path}");
    filechange = result.path;
    print("Stop recording: ${result.duration}");
    file = widget.localFileSystem.file(result.path);
    print("File length: ${await file.length()}");
    List<int> imageBytes = file.readAsBytesSync();
    base64Image = base64Encode(imageBytes);
    print(base64Image);

    _timer.cancel();

    setState(() {
      _current = result;
      _currentStatus = _current.status;
      recorder = false;
      duree = _current.duration.inMinutes.remainder(60).toString() +
          ":" +
          _current.duration.inSeconds.remainder(60).toString();
      timer = "";
    });
    initPlayer(_current.path);
  }

  String status() {
    var text = "";
    switch (_currentStatus) {
      case RecordingStatus.Initialized:
        {
          text = 'Init';
          break;
        }
      case RecordingStatus.Recording:
        {
          text = 'Start';
          break;
        }
      case RecordingStatus.Paused:
        {
          text = 'Pause';
          break;
        }
      case RecordingStatus.Stopped:
        {
          text = 'Stop';
          break;
        }
      default:
        break;
    }
    return text;
  }

  getDetailPodcast(String token, int id) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    detailPodcast = ClientApi.getDetailPodcast(token, id);
    detailPodcast
        .then((value) => {
              EasyLoading.dismiss(),
              setState(() {
                detailPodcastData = value.data;
                initPlayer(Endpoints.baseUrlImage + value.data.file);
              })
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }

  Future<void> initPlayer(String url) async {
    _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);

    seekDone = false;

// prepare the player with this audio but do not start playing
    await _audioPlayer.setReleaseMode(
        ReleaseMode.STOP); // set release mode so that it never releases

    _durationSubscription = _audioPlayer.onDurationChanged.listen((duration) {
      setState(() => _duration = duration);
    });

    _positionSubscription =
        _audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
              _position = p;
            }));

    _playerCompleteSubscription =
        _audioPlayer.onPlayerCompletion.listen((event) {
      _onComplete();
      setState(() {
        _position = null;
      });
    });

    _playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        _playerState = PlayerState.stopped;
        _duration = Duration(seconds: 0);
        _position = Duration(seconds: 0);
      });
    });

    _audioPlayer.onPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() {
        _audioPlayerState = state;
      });
    });

    _audioPlayer.onNotificationPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() => _audioPlayerState = state);
    });
  }

  Future<int> _play(String path, bool isLocal) async {
    setState(() {
      isLocale = isLocal;
    });
    if (seekDone == true) {
      seekDone = false;
    }
    final playPosition = (_position != null &&
            _duration != null &&
            _position.inMilliseconds > 0 &&
            _position.inMilliseconds < _duration.inMilliseconds)
        ? _position
        : null;
    final result =
        await _audioPlayer.play(path, position: playPosition, isLocal: isLocal);

    if (result == 1) {
      setState(() => _playerState = PlayerState.playing);
    }

    // default playback rate is 1.0
    // this should be called after _audioPlayer.play() or _audioPlayer.resume()
    // this can also be called everytime the user wants to change playback rate in the UI
    _audioPlayer.setPlaybackRate(playbackRate: 1.0);

    return result;
  }

  void _onComplete() {
    setState(() => {
          _playerState = PlayerState.stopped,
          seekDone = true,
        });
    if (!isLocale) {
      _init();
    }
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }
}
