import 'dart:async';
import 'dart:convert';

import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:mercure_client/mercure_client.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/Page/filterRecordPage.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/data.dart';
import 'package:voicechanger/data/network/Model/message.dart';
import 'dart:io' as io;
import 'package:flutter/services.dart';

import 'package:file/file.dart';
import 'package:file/local.dart';
import 'package:path_provider/path_provider.dart';
import 'package:intl/date_symbol_data_local.dart';

class DiscussionPage extends StatefulWidget {
  final LocalFileSystem localFileSystem;
  DiscussionPage({Key key, localFileSystem})
      : this.localFileSystem = localFileSystem ?? LocalFileSystem();

  Discussion createState() => Discussion();
}

class Discussion extends State<DiscussionPage> with WidgetsBindingObserver {
  int currentPage = 1;
  int maxPage;
  ScrollController _scrollController;
  bool show = false;
  double spreadradius = 0;
  double opacity = 0;
  FlutterAudioRecorder _recorder;
  Recording _current;
  RecordingStatus _currentStatus = RecordingStatus.Unset;
  bool recorder = false;
  String timer = "";
  File file;
  Timer _timer;
  String duree;
  int start = 60;
  String base64audio;
  String user_id;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  void startTimer(int timerDuration) {
    setState(() {
      start = timerDuration;
    });

    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (start < 1) {
            timer.cancel();
            _stop();
          } else {
            start = start - 1;
          }
        },
      ),
    );
  }

  void initState() {
    WidgetsBinding.instance.addObserver(this);

    super.initState();
    timer = "";
    Devicelocale.currentLocale.then((value) => {
          Intl.defaultLocale = value,
          initializeDateFormatting(value, null),
        });

    _scrollController = ScrollController();
    _init();
    getToken().then((cleapi) {
      // here you "register" to get "notifications" when the getEmail method is done.
      _getMessages(cleapi, currentPage);
    });
    getToken().then((id) {
      // here you "register" to get "notifications" when the getEmail method is done.
      getUserID(id);
    });
    socketMessage();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent &&
          currentPage < maxPage) {
        print(currentPage);
        print(maxPage);
        _getMoreData();
      }
    });
  }

  @override
  void deactivate() {
    cancelMessage();
    super.deactivate();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    /*if(AppLifecycleState.resumed == state) {
     print("resume");
     getToken().then((cleapi) {
      // here you "register" to get "notifications" when the getEmail method is done.
      _getMessages(cleapi, currentPage);
    });
    }*/
    print(state);
  }

  void notifMessage(
      int conv_id, String sender, String sender_photo, String date) {
    Messages detailMessages = new Messages(
        id: conv_id, nom: sender, photo: sender_photo, date: date, notif: true);
    if (checkIfMessageExist(conv_id)) {
      if (!mounted) return;

      setState(() {
//listMessages[listMessages.indexWhere((element) => element.id == detailMessages.id)] = detailMessages;
        listMessages.removeAt(
            listMessages.indexWhere((element) => element.id == conv_id));
        listMessages.insert(1, detailMessages);
      });
      print("exist");
    } else {
      if (!mounted) return;
      setState(() {
        listMessages.add(detailMessages);
      });

      print("not exist");
    }
  }

  _getMoreData() {
    currentPage++;
    getToken().then((cleapi) {
      messages = ClientApi.getListemessages(cleapi, currentPage);
      messages
          .then((value) => {
                EasyLoading.dismiss(),
                setState(() {
                  listMessages = listMessages + value.data;
                })
              })
          .catchError((error) =>
              {EasyLoading.dismiss(), print("erreur : " + error.toString())});
    });
  }

  _getMessages(String token, int page) {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    // here you "register" to get "notifications" when the getEmail method is done.
    messages = ClientApi.getListemessages(token, page);
    messages
        .then((value) => {
              EasyLoading.dismiss(),
              setState(() {
                listMessages = value.data;
                currentPage = 1;
                maxPage = value.pagination.pages;
                show = true;
              }),
            })
        .catchError((error) => {
              EasyLoading.dismiss(),
              print("erreur : " + error.toString()),
              if (error == 401) {logoutUser()},
              print("erreur : " + error.toString())
            });
  }

  Future<void> logoutUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear().then((onValue) => {
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/login', (Route<dynamic> route) => false)
        });
  }

  getUserID(String token) {
    // here you "register" to get "notifications" when the getEmail method is done.
    data = ClientApi.getUserID(token);
    data
        .then((value) => {
              setState(() {
                user_id = value.data;
                pushUserID(int.parse(value.data));
              })
            })
        .catchError((error) => {print("erreur : " + error.toString())});
  }

  Future<String> pushUserID(int id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('id', id);
  }

  filterSearchResults(String value) {
    getToken().then((cleapi) {
      messages = ClientApi.searchMessage(cleapi, value);
      messages
          .then((value) => {
                setState(() {
                  listMessages = value.data;
                  show = false;
                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }

  messageDetail(int id, String nom) {
    for (var i = 0; i < listMessages.length; i++) {
      if (listMessages[i].id == id) {
        setState(() {
          listMessages[i].notif = false;
        });
      }
    }
    Navigator.pushNamed(context, "/detailmessage",
        arguments: {'id': id, 'nom': nom});
  }

  Future<void> socketMessage() async {
    final mercure = Mercure(
      Endpoints.hub_url, // your mercure hub url
      Endpoints.topic_send_message, // your mercure topic
      token: Endpoints.token, // Bearer authorization
      // in case your stored last recieved event
      //showLogs: true, // Default to false
    );
    await mercure.subscribe((event) {
      print("socket :" + event.data);
      var data = Map<String, dynamic>.from(json.decode(event.data));
      print("test " +
          data["message"]["receiver_id"]
              .contains(int.parse(user_id))
              .toString());
      if (data["message"]["receiver_id"].contains(int.parse(user_id))) {
        Messages detailMessages = new Messages(
            id: data["conversation"]["id"],
            nom: data["message"]["sender"],
            photo: data["message"]["sender_photo"],
            date: data["message"]["date"]["date"],
            notif: true);
        if (checkIfMessageExist(data["conversation"]["id"])) {
          setState(() {
//listMessages[listMessages.indexWhere((element) => element.id == detailMessages.id)] = detailMessages;
            listMessages.removeAt(listMessages.indexWhere(
                (element) => element.id == data["conversation"]["id"]));
            listMessages.insert(0, detailMessages);
          });
          print("exist");
        } else {
          setState(() {
            listMessages.add(detailMessages);
          });

          print("not exist");
        }
      }
    });
  }

  bool checkIfMessageExist(int uid) {
    bool test = false;
    for (var i = 0; i < listMessages.length; i++) {
      if (listMessages[i].id == uid) {
        test = true;
        break;
      }
    }
    return test;
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  Color backgroundPanel = Color(0xfff5f8f7);
  TextEditingController editingController = TextEditingController();
  Future<Data> data;
  var listMessages = new List<Messages>();
  Future<Messagee> messages;
  var _key = GlobalKey();

  // Platform messages are asynchronous, so we initialize in an async method.

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      ColorFiltered(
          colorFilter: ColorFilter.mode(
              Colors.black.withOpacity(opacity), BlendMode.darken),
          child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Scaffold(
                resizeToAvoidBottomInset: false,
                body: Column(
                  children: [
                    Padding(
                        padding: EdgeInsets.fromLTRB(10, 15, 8, 0),
                        child: Theme(
                            data: Theme.of(context)
                                .copyWith(splashColor: Colors.transparent),
                            child: Container(
                              height: 36,
                              child: (listMessages.isEmpty && show)
                                  ? Container()
                                  : TextField(
                                      onChanged: (query) => {
                                            if (query != "")
                                              filterSearchResults(query)
                                            else
                                              {
                                                getToken().then((cleapi) {
                                                  _getMessages(cleapi, 1);
                                                })
                                              }
                                          },
                                      style: TextStyle(
                                        fontFamily: 'SFProText',
                                        color: Color(0x7f9b9b9b),
                                        fontSize: 17,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: -0.408,
                                      ),
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Color(0x129b9b9b),
                                          hintStyle: TextStyle(
                                            fontFamily: 'SFProText',
                                            color: Color(0x7f9b9b9b),
                                            fontSize: 17,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: -0.408,
                                          ),
                                          hintText: AppLocalizations.of(context)
                                              .translate('search'),
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 0,
                                                  horizontal: 15.0),
                                          prefixIcon: Icon(Icons.search,
                                              color: Color(0xff949494)),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          disabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ))),
                            ))),
                    Expanded(
                      child: (listMessages.isEmpty && show)
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                SvgPicture.asset("assets/icon/bubble.svg",
                                    width: 81,
                                    height: 78,
                                    color: Color(0xffc4c4c4)),
                                Padding(
                                    padding: EdgeInsets.fromLTRB(10, 13, 10, 0),
                                    child: RichText(
                                        textAlign: TextAlign.center,
                                        text: new TextSpan(children: [
                                          new TextSpan(
                                              text: AppLocalizations.of(context)
                                                  .translate('no_discussion'),
                                              style: TextStyle(
                                                fontFamily: 'Ubuntu',
                                                color: Color(0xff9b9b9b),
                                                fontSize: 21,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                              )),
                                          new TextSpan(
                                              text: AppLocalizations.of(context)
                                                  .translate('send_first_talk'),
                                              style: TextStyle(
                                                fontFamily: 'Ubuntu',
                                                color: Color(0xff9b9b9b),
                                                fontSize: 18,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                              )),
                                        ]))),
                                Padding(
                                    padding: EdgeInsets.only(top: 81),
                                    child: Align(
                                        alignment: Alignment.bottomRight,
                                        child: SvgPicture.asset(
                                            "assets/icon/arrow.svg")))
                              ],
                            )
                          : GridView.builder(
                              key: _key,
                              shrinkWrap: true,
                              controller: _scrollController,
                              padding: EdgeInsets.only(top: 20),
                              physics: const AlwaysScrollableScrollPhysics(),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                // number of items per row
                                crossAxisCount: 3,
                                // vertical spacing between the items
                                mainAxisSpacing: 10.0,
                                // horizontal spacing between the items
                                crossAxisSpacing: 4.0,
                                childAspectRatio: 1.0,
                              ),
                              itemBuilder: (BuildContext context, int i) {
                                if (listMessages.isNotEmpty) {
                                  return (i >= listMessages.length)
                                      ? Center(
                                          child: CupertinoActivityIndicator())
                                      : MessageWidget(listMessages[i], i);
                                }
                                return null;
                              },
                              itemCount: currentPage == maxPage
                                  ? listMessages.length
                                  : listMessages.length + 1,
                            ),

                      //Footer()
                    )
                  ],
                ),
              ))),
      Padding(
          padding: EdgeInsets.only(bottom: 19),
          child: Align(
              alignment: Alignment.bottomCenter,
              child:
                  Column(mainAxisAlignment: MainAxisAlignment.end, children: <
                      Widget>[
                if (timer != "" && timer != "0:0")
                  Container(
                      width: 40,
                      height: 40,
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xffffffff),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x33000000),
                              offset: Offset(0, 0),
                              blurRadius: 16,
                              spreadRadius: 0)
                        ],
                      ),
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 6, vertical: 12),
                        child: Text(timer,
                            style: TextStyle(
                              fontFamily: 'Ubuntu',
                              color: Color(0xff949494),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0,
                            )),
                      )),
                SizedBox(
                  height: 10,
                ),
                GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FilterRecordPage(),
                            fullscreenDialog: true),
                      );
                    },
                    onLongPress: () => {
                          HapticFeedback.vibrate(),
                          _start(),
                          setState(() {
                            opacity = 0.5;
                            spreadradius = 5;
                          })
                        },
                    onLongPressUp: () async {
                      await _stop();
                      Navigator.pushNamed(
                        context,
                        "/listsuggestions",
                        arguments: {'duree': duree, 'file': base64audio},
                      ).then((_) {
                        cancelMessage();
                      });
                      setState(() {
                        opacity = 0;
                        spreadradius = 0;
                      });
                    },
                    child: Container(
                        height: 88,
                        decoration: new BoxDecoration(
                          border: new Border.all(color: Colors.white, width: 2),
                          shape: BoxShape.circle,
                          color: Color(0xff25a996),
                          boxShadow: [
                            BoxShadow(
                              color: Color(0x33000000),
                              offset: Offset(0, 0),
                              blurRadius: 0,
                              spreadRadius: spreadradius,
                            )
                          ],
                        ),
                        child: Padding(
                            padding: EdgeInsets.all(15),
                            child:
                                SvgPicture.asset(("assets/icon/mic-3.svg")))))
              ])))
    ]);
  }

  MessageWidget(Messages message, int i) {
    print(message.photo);
    return GestureDetector(
        onTap: () {
          messageDetail(message.id, message.nom);
        },
        child: Column(children: [
          new Stack(overflow: Overflow.visible, children: <Widget>[
            Container(
              width: 52,
              height: 52,
              decoration: new BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Color(0x33000000),
                        offset: Offset(0, 0),
                        blurRadius: 5,
                        spreadRadius: 0)
                  ],
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(Endpoints.baseUrlImage + message.photo),
                  ),
                  color: Color(0xffffffff),
                  borderRadius: BorderRadius.circular(8)),
            ),
            if (message.notif)
              new Positioned(
                  top: -3.0,
                  right: -3.0,
                  child: new Stack(
                    children: <Widget>[
                      new Icon(Icons.brightness_1,
                          size: 12.0, color: Color(0xffe15656)),
                    ],
                  ))
          ]),
          SizedBox(height: 4),
          Text(message.nom,
              textAlign: TextAlign.center,
              style: (message.notif)
                  ? TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xff3f3c3c),
                      fontSize: 13,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.07083300000000001,
                    )
                  : TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xff3f3c3c),
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.2708333333333333,
                    )),
          SizedBox(height: 2),
          if (message.date != "")
            Text(getDate(DateTime.parse(message.date)),
                style: (message.notif)
                    ? TextStyle(
                        fontFamily: 'Ubuntu',
                        color: Color(0xff3f3c3c),
                        fontSize: 13,
                        fontWeight: FontWeight.w300,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0.2708333333333333,
                      )
                    : TextStyle(
                        fontFamily: 'Ubuntu',
                        color: Color(0xff949494),
                        fontSize: 13,
                        fontWeight: FontWeight.w300,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0.2708333333333333,
                      ))
        ]));
  }

  String getDate(DateTime date) {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final yesterday = DateTime(now.year, now.month, now.day - 1);
    String finaldate;
    final aDate = DateTime(date.year, date.month, date.day);
    if (aDate == today) {
      finaldate = DateFormat("h:mm a").format(date.toLocal());
    } else if (aDate == yesterday) {
      finaldate = AppLocalizations.of(context).translate('yesterday');
    } else {
      finaldate = DateFormat("EEE, dd MMM").format(date.toLocal());
    }
    return finaldate;
  }

  cancelMessage() {
    if (_timer != null) {
      _timer.cancel();
    }
    _init();
  }

  _init() async {
    timer = "";
    start = 60;

    try {
      if (await FlutterAudioRecorder.hasPermissions) {
        String customPath = '/flutter_audio_recorder_';
        io.Directory appDocDirectory;
//        io.Directory appDocDirectory = await getApplicationDocumentsDirectory();
        if (io.Platform.isIOS) {
          appDocDirectory = await getApplicationDocumentsDirectory();
        } else {
          appDocDirectory = await getExternalStorageDirectory();
        }

        // can add extension like ".mp4" ".wav" ".m4a" ".aac"
        customPath = appDocDirectory.path +
            customPath +
            DateTime.now().millisecondsSinceEpoch.toString();

        // .wav <---> AudioFormat.WAV
        // .mp4 .m4a .aac <---> AudioFormat.AAC
        // AudioFormat is optional, if given value, will overwrite path extension when there is conflicts.
        _recorder =
            FlutterAudioRecorder(customPath, audioFormat: AudioFormat.WAV);

        await _recorder.initialized;
        // after initialization
        var current = await _recorder.current(channel: 0);
        print(current);
        // should be "Initialized", if all working fine
        setState(() {
          _current = current;
          _currentStatus = current.status;
        });
      } else {
        Scaffold.of(context).showSnackBar(
            new SnackBar(content: new Text("You must accept permissions")));
      }
    } catch (e) {
      print(e);
    }
  }

  _start() async {
    try {
      startTimer(60);
      await _recorder.start();
      var recording = await _recorder.current(channel: 0);
      setState(() {
        _current = recording;
      });

      const tick = const Duration(seconds: 1);
      new Timer.periodic(tick, (Timer t) async {
        if (_currentStatus == RecordingStatus.Stopped) {
          t.cancel();
        }

        var current = await _recorder.current(channel: 0);
        // print(current.status);
        setState(() {
          _current = current;
          _currentStatus = _current.status;
          recorder = true;
          timer = _current?.duration.inMinutes.remainder(60).toString() +
              ":" +
              current?.duration.inSeconds.remainder(60).toString();
        });
      });
    } catch (e) {
      print(e);
    }
  }

  _stop() async {
    var result = await _recorder.stop();
    print("Stop recording: ${result.path}");
    print("Stop recording: ${result.duration}");
    file = widget.localFileSystem.file(result.path);
    print("File length: ${await file.length()}");
    List<int> imageBytes = file.readAsBytesSync();
    print(base64Encode(imageBytes));
    base64audio = base64Encode(imageBytes);

    setState(() {
      _current = result;
      _currentStatus = _current.status;
      recorder = false;
      duree = _current.duration.inMinutes.remainder(60).toString() +
          ":" +
          _current.duration.inSeconds.remainder(60).toString();
    });
  }
}
