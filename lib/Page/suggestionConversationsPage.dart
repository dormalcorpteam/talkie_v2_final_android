import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/data.dart';
import 'package:voicechanger/data/network/Model/suggestionConversations.dart';
import 'dart:convert';

class SuggestionConversationsPage extends StatefulWidget {
  SuggestionConversations createState() => SuggestionConversations();
}

class SuggestionConversations extends State<SuggestionConversationsPage> {
  Future<Data> data;
  Future<SuggestionConversation> suggestionConversation;
  var listSuggestion = new List<SuggestionConversationData>();
  SuggestionConversationData selectedAmis = new SuggestionConversationData();
  Map arguments;
  int id_filtre;
  String duree;
  String file;
  bool show = false;
  List<List<int>> idRece = new List();

  List<String> idConv = new List();

  @override
  void initState() {
    super.initState();
    getListFriends();
  }

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
      if (arguments != null) {
        print(arguments['id_filtre']);
        print(arguments['duree']);
        print(arguments['file']);
        id_filtre = arguments['id_filtre'];
        duree = arguments['duree'];
        file = arguments['file'];
      }
    }

    return Scaffold(
        backgroundColor: Color(0xfff8f9f9),
        appBar: AppBar(
            backgroundColor: Color(0xff25a996),
            elevation: 0,
            centerTitle: true,
            actions: <Widget>[
              FlatButton(
                  child: Text(AppLocalizations.of(context).translate('send'),
                      style: TextStyle(
                        fontFamily: 'SFCompactText',
                        color: Color(0xffffffff),
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0,
                      )),
                  onPressed: () {
                    getToken().then((token) => {
                          if (file != null)
                            {
                              sendQucikMessage(
                                  token, file, idRece, duree, idConv),
                            },
                          if (id_filtre != null)
                            {
                              sendMessage(
                                  token, id_filtre, idRece, duree, idConv)
                            }
                        });

                    print(idRece);
                    print(idConv);
                  }),
            ]),
        resizeToAvoidBottomInset: false,
        body: (listSuggestion.isEmpty && show)
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Center(
                      child: Text(
                          AppLocalizations.of(context)
                              .translate('empty_talkers'),
                          style: TextStyle(
                            fontFamily: 'Ubuntu',
                            color: Color(0xff9b9b9b),
                            fontSize: 21,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ))),
                ],
              )
            : Column(children: [
                Padding(
                    padding: EdgeInsets.fromLTRB(8, 15, 8, 0),
                    child: Theme(
                        data: Theme.of(context)
                            .copyWith(splashColor: Colors.transparent),
                        child: Container(
                          height: 36,
                          child: TextField(
                              onChanged: (query) => {
                                    if (query != "")
                                      {
                                        print(query),
                                        filterSearchResults(query),
                                      }
                                    else
                                      {
                                        getListFriends(),
                                      }
                                  },
                              style: TextStyle(
                                fontFamily: 'SFProText',
                                color: Color(0x7f9b9b9b),
                                fontSize: 17,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: -0.408,
                              ),
                              decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Color(0x129b9b9b),
                                  hintStyle: TextStyle(
                                    fontFamily: 'SFProText',
                                    color: Color(0x7f9b9b9b),
                                    fontSize: 17,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: -0.408,
                                  ),
                                  hintText: AppLocalizations.of(context)
                                      .translate('search'),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 15.0),
                                  prefixIcon: Icon(Icons.search,
                                      color: Color(0xff949494)),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1, color: Color(0x129b9b9b)),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1, color: Color(0x129b9b9b)),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1, color: Color(0x129b9b9b)),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1, color: Color(0x129b9b9b)),
                                  ))),
                        ))),
                SizedBox(height: 10),
                new Expanded(
                    child: GridView.builder(
                  itemCount: listSuggestion.length,
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    // number of items per row
                    crossAxisCount: 3,
                    // vertical spacing between the items
                    mainAxisSpacing: 30.0,
                    // horizontal spacing between the items
                    crossAxisSpacing: 15.0,
                    childAspectRatio: 1.0,
                  ),
                  itemBuilder: (BuildContext context, int i) {
                    return Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: UserWidget(listSuggestion[i], i));
                  },
                ))
              ]));
  }

  filterSearchResults(String value) {
    getToken().then((cleapi) {
      suggestionConversation = ClientApi.searchSuggestionConv(cleapi, value);
      suggestionConversation
          .then((value) => {
                setState(() {
                  listSuggestion = value.data;
                  for (int i = 0; i < listSuggestion.length; i++) {
                    for (int j = 0; j < idConv.length; j++) {
                      if (listSuggestion[i].idConv == idConv[j].toString()) {
                        listSuggestion[i].value = true;
                      }
                    }
                  }
                  show = false;
                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }

  getListFriends() {
    getToken().then((cleapi) {
      suggestionConversation = ClientApi.getSuggestionConv(cleapi);
      suggestionConversation
          .then((value) => {
                print(value.data),
                setState(() {
                  listSuggestion = value.data;
                  for (int i = 0; i < listSuggestion.length; i++) {
                    for (int j = 0; j < idConv.length; j++) {
                      if (listSuggestion[i].idConv == idConv[j].toString()) {
                        listSuggestion[i].value = true;
                      }
                    }
                  }
                  show = true;
                }),
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  UserWidget(SuggestionConversationData user, int i) {
    return GestureDetector(
        onTap: () {},
        child: Column(children: [
          new Stack(overflow: Overflow.visible, children: <Widget>[
            if (user.photo != "")
              GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    setState(() {
                      listSuggestion[i].value = !listSuggestion[i].value;
                      if (user.value) {
                        idRece.add(listSuggestion[i].idReceiver);
                        idConv.add(listSuggestion[i].idConv);
                      } else {
                        idRece.remove(listSuggestion[i].idReceiver);
                        idConv.remove(listSuggestion[i].idConv);
                      }
                    });
                  },
                  child: Container(
                    width: 52,
                    height: 52,
                    decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x33000000),
                              offset: Offset(0, 0),
                              blurRadius: 5,
                              spreadRadius: 0)
                        ],
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image:
                              NetworkImage(Endpoints.baseUrlImage + user.photo),
                        ),
                        color: Color(0xffffffff),
                        borderRadius: BorderRadius.circular(8)),
                  )),
            new Positioned(
                top: -20.0,
                right: -20.0,
                child: new Stack(
                  children: <Widget>[
                    Center(
                        child: Theme(
                            data: ThemeData(
                                unselectedWidgetColor: Color(0xff949494)),
                            child: Checkbox(
                              activeColor: Color(0xff25a996),
                              value: user.value,
                              onChanged: (bool value) => {},
                            ))),
                  ],
                ))
          ]),
          SizedBox(
            height: 4,
          ),
          Text(user.nom,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xff3f3c3c),
                fontSize: 13,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0.2708333333333333,
              )),
        ]));
  }

  sendQucikMessage(
      String token, String file, var id_reciver, String durre, var id_conv) {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );

    data = ClientApi.sendquickMessage(token, file, id_reciver, durre, id_conv);
    data
        .then((value) => {
              print(value.data),
              EasyLoading.dismiss(),
              EasyLoading.showSuccess(
                  AppLocalizations.of(context).translate('message_send')),
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/home', (Route<dynamic> route) => false),
            })
        .catchError((error) => {
              EasyLoading.showError(
                  AppLocalizations.of(context).translate('error_try_again')),
              print("erreur11 : " + error.toString()),
              EasyLoading.dismiss(),
            });
  }

  Future<void> sendMessage(String token, int id_voice, var id_reciver,
      String durre, var id_conv) async {
    FocusScope.of(context).requestFocus(FocusNode());

    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );

    data = ClientApi.sendMessage(token, id_voice, id_reciver, durre, id_conv);
    data
        .then((value) => {
              print(value.data),
              EasyLoading.dismiss(),
              EasyLoading.showSuccess(
                  AppLocalizations.of(context).translate('message_send')),
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/home', (Route<dynamic> route) => false),
            })
        .catchError((error) => {
              EasyLoading.showError(
                  AppLocalizations.of(context).translate('error_try_again')),
              print("erreur : " + error.toString()),
              EasyLoading.dismiss()
            });
  }
}
