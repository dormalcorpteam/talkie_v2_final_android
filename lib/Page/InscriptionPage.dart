import 'dart:io';

import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Model/data.dart';
import '../data/network/Model/user.dart';

import 'package:image_picker/image_picker.dart';
import 'dart:convert';

class InscriptionPage extends StatefulWidget {
  @override
  _InscriptionPageState createState() => _InscriptionPageState();
}

class _InscriptionPageState extends State<InscriptionPage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  File _image;
  String _base64Image;
  Future<Data> data;
  FocusNode _focusNode = FocusNode();
  bool is_checked = false;
  bool is_checked_eula = false;
  bool check_name = false;
  bool check_surname = false;
  bool check_username = false;
  bool check_email = false;
  bool check_date = false;
  var myFormat = DateFormat('dd-MM-yyyy');
  String indicatif = "+33";
  String code_pays_tel = "FR";
  bool phone_error = false;
  bool email_error_taken = false;
  bool email_error_valid = false;
  bool username_error = false;
  var selectedDate = DateTime.now();
  TextEditingController _date = new TextEditingController();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1901),
        lastDate: DateTime(2100));
    if (picked != null)
      setState(() {
        selectedDate = picked;

        _date.value = TextEditingValue(text: "${myFormat.format(picked)}");
        check_date = true;
      });
  }

  @override
  void initState() {
    super.initState();
    is_checked = false;
    is_checked_eula = false;

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();

    super.dispose();
  }

  TextEditingController usernameController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController surnameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextStyle style = TextStyle(
    fontFamily: 'Ubuntu',
    color: Color(0xffffffff),
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 0,
  );

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    final usernameField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            onChanged: (value) {
              if (value.isNotEmpty) {
                setState(() {
                  check_username = true;
                });
              } else {
                setState(() {
                  check_username = false;
                });
              }
            },
            style: style,
            controller: usernameController,
            obscureText: false,
            decoration: InputDecoration(
              errorText: username_error
                  ? AppLocalizations.of(context).translate('username_taken')
                  : null,
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('username'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));

    final nameField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            onChanged: (value) {
              if (value.isNotEmpty) {
                setState(() {
                  check_name = true;
                });
              } else {
                setState(() {
                  check_name = false;
                });
              }
            },
            style: style,
            controller: nameController,
            decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('surname'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));
    final surnameField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            onChanged: (value) {
              if (value.isNotEmpty) {
                setState(() {
                  check_surname = true;
                });
              } else {
                setState(() {
                  check_surname = false;
                });
              }
            },
            style: style,
            controller: surnameController,
            decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('name'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));

    final emailField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            onChanged: (value) {
              if (value.isNotEmpty) {
                setState(() {
                  check_email = true;
                });
              } else {
                setState(() {
                  check_email = false;
                });
              }
            },
            style: style,
            controller: emailController,
            decoration: InputDecoration(
              errorText: email_error_taken
                  ? AppLocalizations.of(context).translate('email_taken')
                  : email_error_valid
                      ? AppLocalizations.of(context).translate('email_invalid')
                      : null,
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('email_adress'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));
    Widget _buildDropdownItem(Country country) => Container(
          child: Row(
            children: <Widget>[
              CountryPickerUtils.getDefaultFlagImage(country),
              Text("+${country.phoneCode}(${country.isoCode})"),
            ],
          ),
        );

    final phoneField = Container(
        margin: const EdgeInsets.fromLTRB(8, 0, 8, 0),
        decoration: new BoxDecoration(
            color: Color(0xff25a996),
            borderRadius: new BorderRadius.all(Radius.circular(15))),
        child: Row(children: <Widget>[
          CountryPickerDropdown(
            initialValue: 'FR',
            itemBuilder: _buildDropdownItem,
            priorityList: [
              CountryPickerUtils.getCountryByIsoCode('FR'),
              CountryPickerUtils.getCountryByIsoCode('DE'),
              CountryPickerUtils.getCountryByIsoCode('ES'),
              CountryPickerUtils.getCountryByIsoCode('BE'),
              CountryPickerUtils.getCountryByIsoCode('IT'),
            ],
            sortComparator: (Country a, Country b) =>
                a.isoCode.compareTo(b.isoCode),
            onValuePicked: (Country country) {
              print("${country.name}");
              setState(() => {
                    indicatif = "+" + country.phoneCode,
                    code_pays_tel = country.isoCode,
                  });
            },
          ),
          Expanded(
              child: TextField(
                  style: style,
                  controller: phoneController,
                  onChanged: (value) {
                    if (value.isEmpty) {}
                  },
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                    errorText: phone_error
                        ? AppLocalizations.of(context).translate('phone_taken')
                        : null,
                    border: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white)),
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white)),
                    disabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white)),
                    labelText:
                        AppLocalizations.of(context).translate('phone_number'),
                    labelStyle: TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xffffffff),
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    ),
                  )))
        ]));

    final checkbox = Padding(
        padding: EdgeInsets.fromLTRB(8, 30, 8, 0),
        child: Row(children: <Widget>[
          Theme(
              data: ThemeData(
                unselectedWidgetColor: Colors.white,
              ),
              child: Checkbox(
                checkColor: Color(0xff25a996),
                value: is_checked,
                activeColor: Colors.white,
                onChanged: (bool value) {
                  this.setState(() {
                    is_checked = value;
                  });
                },
              )),
          Expanded(
              child: Text.rich(
            TextSpan(
              text: AppLocalizations.of(context).translate('read_accept'),
              style: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
              children: <TextSpan>[
                TextSpan(
                    text: AppLocalizations.of(context).translate('cgu'),
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () => FlutterWebBrowser.openWebPage(
                          url:
                              "http://ns3122723.ip-51-68-37.eu/talkie-dev/public/uploads/cgu_talkie.pdf",
                          ),
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'Ubuntu',
                      color: Color(0xffffffff),
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    )),
                // can add more TextSpans here...
              ],
            ),
            overflow: TextOverflow.visible,
          )),
        ]));
    final checkbox1 = Padding(
        padding: EdgeInsets.fromLTRB(8, 10, 8, 0),
        child: Row(children: <Widget>[
          Theme(
              data: ThemeData(
                unselectedWidgetColor: Colors.white,
              ),
              child: Checkbox(
                checkColor: Color(0xff25a996),
                value: is_checked_eula,
                activeColor: Colors.white,
                onChanged: (bool value) {
                  this.setState(() {
                    is_checked_eula = value;
                  });
                },
              )),
          Expanded(
              child: Text.rich(
            TextSpan(
              text: AppLocalizations.of(context).translate('read_accept'),
              style: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
              children: <TextSpan>[
                TextSpan(
                    text: "EULA",
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () => FlutterWebBrowser.openWebPage(
                          url:
                              "http://ns3122723.ip-51-68-37.eu/talkie-dev/public/uploads/eula_talkie.pdf",
                          ),
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'Ubuntu',
                      color: Color(0xffffffff),
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    )),
                // can add more TextSpans here...
              ],
            ),
            overflow: TextOverflow.visible,
          )),
        ]));
    final signupButon = Padding(
        padding: EdgeInsets.fromLTRB(38, 71, 37, 0),
        child: SizedBox(
            width: double.infinity, // match_parent
            height: 45,
            child: RaisedButton(
                elevation: 0,
                color: (is_checked &&
                        is_checked_eula &&
                        check_username &&
                        check_name &&
                        check_surname &&
                        check_email &&
                        check_date)
                    ? Colors.white
                    : Color(0xff6cc5b8),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                ),
                onPressed: () {
                  if (is_checked &&
                      is_checked_eula &&
                      check_username &&
                      check_name &&
                      check_surname &&
                      check_email &&
                      check_date) {
                    EasyLoading.instance
                      ..displayDuration = const Duration(seconds: 5)
                      ..loadingStyle = EasyLoadingStyle.custom
                      ..backgroundColor = Colors.white
                      ..progressColor = Colors.white
                      ..indicatorType = EasyLoadingIndicatorType.wave
                      ..indicatorColor = Color(0xff3bc0ae)
                      ..textColor = Color(0xff3bc0ae)
                      ..userInteractions = false
                      ..maskType = EasyLoadingMaskType.black;
                    EasyLoading.show(
                      status: AppLocalizations.of(context).translate('loading'),
                    );

                    data = ClientApi.verifRegisterApi(
                        usernameController.text,
                        emailController.text,
                        nameController.text,
                        surnameController.text,
                        selectedDate.month.toString() +
                            "/" +
                            selectedDate.day.toString() +
                            "/" +
                            selectedDate.year.toString(),
                        phoneController.text,
                        indicatif,
                        code_pays_tel);
                    data
                        .then((value) => {
                              EasyLoading.dismiss(),
                              Navigator.pushNamed(context, "/inscription2",
                                  arguments: {
                                    'image': _base64Image,
                                    'name': nameController.text,
                                    'surname': surnameController.text,
                                    'username': usernameController.text,
                                    'email': emailController.text,
                                    'phone': phoneController.text,
                                    'date': selectedDate.month.toString() +
                                        "/" +
                                        selectedDate.day.toString() +
                                        "/" +
                                        selectedDate.year.toString(),
                                    'code_pays_tel': code_pays_tel,
                                    'prefixe': indicatif
                                  })
                            })
                        .catchError((error) => {
                              if (error
                                  .toString()
                                  .contains("Phone number est déjà pris"))
                                {
                                  setState(() {
                                    phone_error = true;
                                  }),
                                }
                              else
                                {
                                  setState(() {
                                    phone_error = false;
                                  })
                                },
                              if (error
                                  .toString()
                                  .contains("Nom d'utilisateur est déjà pris"))
                                {
                                  setState(() {
                                    username_error = true;
                                  })
                                }
                              else
                                {
                                  setState(() {
                                    username_error = false;
                                  })
                                },
                              if (error.toString().contains(
                                  "This value is not a valid email address"))
                                {
                                  setState(() {
                                    email_error_valid = true;
                                  })
                                }
                              else
                                {
                                  setState(() {
                                    email_error_valid = false;
                                  })
                                },
                              if (error
                                  .toString()
                                  .contains("Email est déjà pris"))
                                {
                                  setState(() {
                                    email_error_taken = true;
                                  })
                                }
                              else
                                {
                                  setState(() {
                                    email_error_taken = false;
                                  })
                                },

                              //emailController.addError('Wrong ID.'),
                              //passwordController.addError('Wrong Password'),

                              EasyLoading.dismiss(),
                              EasyLoading.instance.loadingStyle =
                                  EasyLoadingStyle.custom,
                              EasyLoading.instance.backgroundColor = Colors.red,
                              EasyLoading.instance.indicatorColor =
                                  Colors.white,
                              EasyLoading.instance.progressColor = Colors.white,
                              EasyLoading.instance.textColor = Colors.white,
                              EasyLoading.instance.displayDuration =
                                  const Duration(milliseconds: 2000),
                              EasyLoading.showError(error.toString())
                            });
                  }
                },
                child: Text(AppLocalizations.of(context).translate('next'),
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xff25a996),
                      fontSize: 15,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    )))));

    final login = Padding(
        padding: EdgeInsets.fromLTRB(0, 15, 0, 60),
        child: Text.rich(
          TextSpan(
            text: AppLocalizations.of(context).translate('already_member'),
            style: TextStyle(
              fontFamily: 'Ubuntu',
              color: Color(0xffffffff),
              fontSize: 13,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
            ),
            children: <TextSpan>[
              TextSpan(
                  text: AppLocalizations.of(context).translate('connect'),
                  recognizer: new TapGestureRecognizer()
                    ..onTap = () => Navigator.pushNamed(context, "/login"),
                  style: TextStyle(
                    fontFamily: 'Ubuntu',
                    color: Color(0xffffffff),
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                    decoration: TextDecoration.underline,
                  )),
              // can add more TextSpans here...
            ],
          ),
          overflow: TextOverflow.visible,
        ));

    return Scaffold(
      backgroundColor: Color(0xff25a996),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xff25a996),
      ),
      body: Stack(children: <Widget>[
        SingleChildScrollView(
          child: new GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onPanDown: (_) {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            behavior: HitTestBehavior.translucent,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  StatefulBuilder(
                      builder: (BuildContext context, StateSetter setState) {
                    return GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () {
                          final act = CupertinoActionSheet(
                              actions: <Widget>[
                                CupertinoActionSheetAction(
                                  child: Text(
                                      AppLocalizations.of(context)
                                          .translate('choose_photo'),
                                      style: TextStyle(
                                        fontFamily: 'SFProText',
                                        color: Color(0xff262626),
                                        fontSize: 15,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                  onPressed: () {
                                    Navigator.of(context, rootNavigator: true)
                                        .pop();

                                    getImage(ImageSource.gallery);
                                  },
                                ),
                                CupertinoActionSheetAction(
                                  child: Text(
                                      AppLocalizations.of(context)
                                          .translate('take_picture'),
                                      style: TextStyle(
                                        fontFamily: 'SFProText',
                                        color: Color(0xff262626),
                                        fontSize: 15,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                  onPressed: () {
                                    Navigator.of(context, rootNavigator: true)
                                        .pop();
                                    getImage(ImageSource.camera);
                                  },
                                )
                              ],
                              cancelButton: CupertinoActionSheetAction(
                                child: Text(
                                    AppLocalizations.of(context)
                                        .translate('cancel'),
                                    style: TextStyle(
                                      fontFamily: 'SFProText',
                                      color: Color(0xff262626),
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                                onPressed: () {
                                  Navigator.of(context, rootNavigator: true)
                                      .pop();
                                },
                              ));
                          showCupertinoModalPopup(
                              context: context,
                              builder: (BuildContext context) => act);
                        },
                        child: Container(
                            margin: EdgeInsets.only(top: 18),
                            width: 100,
                            height: 100,
                            decoration: new BoxDecoration(
                              color: Color(0xa8000000),
                              borderRadius: BorderRadius.circular(8),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0x25000000),
                                    offset: Offset(0, 5),
                                    blurRadius: 15,
                                    spreadRadius: 0)
                              ],
                            ),
                            alignment: Alignment.center,
                            child: (_image != null)
                                ? Container(
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: FileImage(_image))))
                                : Padding(
                                    padding: EdgeInsets.all(30),
                                    child: SvgPicture.asset(
                                        "assets/icon/photo.svg"))));
                  }),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding:
                              EdgeInsets.only(top: 15, right: 52, left: 18),
                          child: Text(
                              AppLocalizations.of(context)
                                  .translate('register'),
                              style: TextStyle(
                                fontFamily: 'Salome',
                                color: Color(0xffffffff),
                                fontSize: 28,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  nameField,
                  surnameField,
                  usernameField,
                  emailField,
                  phoneField,
                  Padding(
                      padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: GestureDetector(
                        onTap: () => _selectDate(context),
                        child: AbsorbPointer(
                          child: TextFormField(
                              controller: _date,
                              style: style,
                              keyboardType: TextInputType.datetime,
                              decoration: InputDecoration(
                                border: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                disabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                labelText: AppLocalizations.of(context)
                                    .translate('birthdate'),
                                labelStyle: TextStyle(
                                  fontFamily: 'Ubuntu',
                                  color: Color(0xffffffff),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                ),
                              )),
                        ),
                      )),
                  checkbox,
                  checkbox1,
                  signupButon,
                  login
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }

  Future getImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(source: source);
    if (image != null) {
      List<int> imageBytes = image.readAsBytesSync();
      print(imageBytes);
      String base64Image = base64Encode(imageBytes);
      print(base64Image);

      setState(() {
        _image = image;
        _base64Image = base64Image;
      });
    }
  }
}
