import 'dart:io';

import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:file/local.dart';
import 'package:voicechanger/Page/Footer.dart';
import 'package:voicechanger/Page/discussionPage.dart';
import 'package:voicechanger/Page/MyWorldPage.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/DetailPodcast.dart';
import 'package:voicechanger/data/network/Model/categories.dart';
import 'package:voicechanger/data/network/Model/data.dart';

import '../player_widget.dart';
import 'AppBarHeader.dart';

class DetailTalks extends StatefulWidget {
  @override
  DetailTalksPage createState() => DetailTalksPage();
}

class DetailTalksPage extends State<DetailTalks> {
  @override
  Widget build(BuildContext context) {
    Map arguments;
    String titre;
    int id;
    int selectedindex;
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    if (arguments != null) {
      print(arguments['titre']);
      titre = arguments['titre'];
      print(arguments['id']);
      selectedindex = arguments['selectedindex'];
      id = arguments['id'];
    }
    return DetailPage(titre: titre, id: id, selectedindex: selectedindex);
  }
}

class DetailPage extends StatefulWidget {
  final LocalFileSystem localFileSystem;

  final String titre;
  final int id;
  final int selectedindex;
  DetailPage(
      {Key key, this.titre, this.id, localFileSystem, this.selectedindex})
      : this.localFileSystem = localFileSystem ?? LocalFileSystem();

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  int _selectedIndex = 1;
  Widget appBar;
  bool isFavoris = false;
  Future<DetailPodcast> detailPodcast;
  UserPodcast userPodcast;
  String nom = "";
  String description = "";
  String duree = "";
  String date = "";
  String file = "";
  int nbr_listeners;
  int nbr_talkers;
  Categorie categorie;
  String language;
  bool like;
  bool dislike;
  int nbr_like;
  int nbr_dislike;
  int nbr_comment;
  Future<Data> data;

  var tags;
  final GlobalKey<TagsState> _tagStateKey = GlobalKey<TagsState>();

  List<Widget> _widgetOptions;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;

      if (index == 0) {
        appBar = AppBarHeader(
            name: "Discussions",
            buttonback: false,
            actionTalk: false,
            actionmessage: true);
      }

      if (index == 1) {
        appBar = AppBarHeader(
            name: widget.titre,
            buttonback: true,
            actionTalk: false,
            actionmessage: false);
      }
      if (index == 2) {
        appBar = null;
      }
    });
  }

  getDetailPodcast(String token, int id) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    detailPodcast = ClientApi.getDetailPodcast(token, id);
    detailPodcast
        .then((value) => {
              EasyLoading.dismiss(),
              setState(() {
                userPodcast = value.data.user;
                nom = value.data.nom;
                description = value.data.description;
                date = value.data.createdAt;
                duree = value.data.duree;
                file = value.data.file;
                nbr_talkers = value.nbrTalkers;
                nbr_listeners = value.nbrListeners;
                categorie = value.data.categorie;
                tags = value.data.tags;
                nbr_like = value.data.nbrLike;
                nbr_dislike = value.data.nbrDislike;
                like = value.data.liked;
                dislike = value.data.disliked;
                nbr_comment = value.data.nbrComment;
              })
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }

  @override
  void initState() {
    super.initState();
    print(_selectedIndex);
    /*if(widget.selectedindex!=null){
      setState(() {
appBar=null;
_selectedIndex=widget.selectedindex;});
  }*/
    getToken().then((cleapi) => {getDetailPodcast(cleapi, widget.id)});

    appBar = AppBarHeader(
        name: widget.titre,
        buttonback: true,
        actionTalk: false,
        actionmessage: false);
    Devicelocale.currentLocale.then((value) => {
          setState(() {
            language = value.split(Platform.isAndroid ? '_' : '-')[0];
          })
        });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _widgetOptions = <Widget>[
      DiscussionPage(),
      SingleChildScrollView(
          child: Column(children: <Widget>[
        if (userPodcast != null)
          ListTile(
            contentPadding: EdgeInsets.fromLTRB(25, 25, 9, 25),
            leading: GestureDetector(
                onTap: () {
                  getID().then((id) {
                    if (userPodcast.id != id)
                      Navigator.pushNamed(context, "/chainePage", arguments: {
                        'id': userPodcast.id,
                        'username': userPodcast.nom + " " + userPodcast.prenom
                      });
                  });
                },
                child: Container(
                  width: 52,
                  height: 52,
                  decoration: new BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                            Endpoints.baseUrlImage + userPodcast.photo),
                      ),
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8)),
                )),
            title: Text(userPodcast.nom.toUpperCase(),
                style: TextStyle(
                  fontFamily: 'Helvetica',
                  color: Color(0xff3f3c3c),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0.4,
                )),
            subtitle: Column(children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      "/listtalkslisteners",
                      arguments: {
                        'id': userPodcast.id,
                        'username': userPodcast.nom + " " + userPodcast.prenom,
                        'page': 0
                      },
                    );
                  },
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text("$nbr_listeners listeners",
                          style: TextStyle(
                            fontFamily: 'Helvetica',
                            color: Color(0xff949494),
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0.2708333333333333,
                          )))),
              SizedBox(
                height: 2,
              ),
              InkWell(
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      "/listtalkslisteners",
                      arguments: {
                        'id': userPodcast.id,
                        'username': userPodcast.nom + " " + userPodcast.prenom,
                        'page': 1
                      },
                    );
                  },
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text("$nbr_talkers talkers",
                          style: TextStyle(
                            fontFamily: 'Helvetica',
                            color: Color(0xff949494),
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0.2708333333333333,
                          ))))
            ]),
            /* trailing: GestureDetector(
                onTap: () {
                  setState(() {
                    isFavoris = !isFavoris;
                  });
                },
                child: SvgPicture.asset(("assets/icon/fav.svg"),
                    color:
                        (isFavoris) ? Color(0xff25a996) : Color(0xff949494)))*/
          ),
        Padding(
            padding: EdgeInsets.fromLTRB(9, 12, 8, 0),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Text(nom.toUpperCase(),
                        style: TextStyle(
                          fontFamily: 'Helvetica',
                          color: Color(0xff3f3c3c),
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0.4166666666666666,
                        ))),
                /*  Spacer(), 
    GestureDetector(
                onTap: () {
                 
                },
                child: SvgPicture.asset(("assets/icon/share.svg"),
    )),
         SizedBox(width: 16), // give it width
     GestureDetector(
                onTap: () {
                 
                },
                child: SvgPicture.asset(("assets/icon/download-grey.svg"),
    )),*/
              ],
            )),
        if (categorie != null)
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
                padding: EdgeInsets.fromLTRB(9, 3, 8, 0),
                child: Text(
                    "${(language == "fr") ? categorie.nom_fr.toUpperCase() : categorie.nom_en.toUpperCase()}",
                    style: TextStyle(
                      fontFamily: 'Helvetica',
                      color: Color(0xff3f3c3c),
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.1559999999999999,
                    ))),
          ),
        if (date != "" && duree != "")
          Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.fromLTRB(9, 4, 0, 0),
                child: Text(
                    getDate(DateTime.parse(date)) +
                        "-" +
                        formatDuration(parseDuration(duree)),
                    style: TextStyle(
                      fontFamily: 'Helvetica',
                      color: Color(0xff949494),
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.2708333333333333,
                    )),
              )),
        Align(
            alignment: Alignment.centerLeft,
            child: Padding(
                padding: EdgeInsets.fromLTRB(9, 15, 8, 0),
                child: Text(
                  description,
                  style: TextStyle(
                    fontFamily: 'Helvetica',
                    color: Color(0xff949494),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0.2916666666666667,
                  ),
                ))),
        if (tags != null)
          Padding(
              padding: EdgeInsets.fromLTRB(8, 25, 8, 0),
              child: Tags(
                key: _tagStateKey,
                itemCount: tags.length, // required
                itemBuilder: (int index) {
                  final item = tags[index];

                  return ItemTags(
                    elevation: 0, textActiveColor: Color(0xff3f3c3c),
                    activeColor: Colors.white,
                    borderRadius: BorderRadius.circular(4),
                    border: new Border.all(color: Color(0xff949494), width: 1),

                    // Each ItemTags must contain a Key. Keys allow Flutter to
                    // uniquely identify widgets.
                    key: Key(index.toString()),
                    index: index, // required
                    title: "#" + item,
                    textStyle: TextStyle(
                      fontFamily: 'Helvetica',
                      color: Color(0xff3f3c3c),
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                    combine: ItemTagsCombine.withTextBefore,
                  );
                },
              )),
        if (file != "")
          Padding(
              padding: EdgeInsets.fromLTRB(9, 45, 8, 20),
              child: PlayerWidget(
                  url: Endpoints.baseUrlImage + file, duree: duree)),
        if (like != null &&
            dislike != null &&
            nbr_like != null &&
            nbr_dislike != null &&
            nbr_comment != null)
          IntrinsicHeight(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      primary: Colors.white,
                      onPrimary: Colors.white,
                      shadowColor: Colors.transparent,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, "/comments", arguments: {
                        'id': widget.id,
                      });
                    },
                    icon: SvgPicture.asset(
                      ("assets/icon/comment.svg"),
                      width: 25.83,
                      height: 24.25,
                    ),
                    label: Text(
                      nbr_comment.toString(),
                      style: TextStyle(
                          color: Color(0xFF949494),
                          fontSize: 18,
                          fontFamily: "Helvetica",
                          fontWeight: FontWeight.normal),
                    )),
                Padding(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    child: VerticalDivider(
                      color: Color(0xFFC4C4C4),
                      thickness: 1.0,
                    )),
                ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      primary: Colors.white,
                      onPrimary: Colors.white,
                      shadowColor: Colors.transparent,
                    ),
                    onPressed: () {},
                    icon: InkWell(
                        onTap: () {
                          if (dislike) {
                            likeApi();
                            disLikeApi();
                          } else {
                            likeApi();
                          }
                        },
                        child: SvgPicture.asset(("assets/icon/like.svg"),
                            width: 19.44,
                            height: 20,
                            color:
                                like ? Color(0xFF25A996) : Color(0xFF949494))),
                    label: InkWell(
                        onTap: () {
                          Navigator.pushNamed(
                            context,
                            "/listlikedislike",
                            arguments: {
                              'id': widget.id,
                              'nom': widget.titre,
                              'page': 0
                            },
                          );
                        },
                        child: Text(
                          nbr_like.toString(),
                          style: TextStyle(
                              color:
                                  like ? Color(0xFF25A996) : Color(0xFF949494),
                              fontSize: 18,
                              fontFamily: "Helvetica",
                              fontWeight: FontWeight.normal),
                        ))),
                ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      primary: Colors.white,
                      onPrimary: Colors.white,
                      shadowColor: Colors.transparent,
                    ),
                    onPressed: () {},
                    icon: InkWell(
                        onTap: () {
                          if (like) {
                            disLikeApi();
                            likeApi();
                          } else {
                            disLikeApi();
                          }
                        },
                        child: SvgPicture.asset(("assets/icon/dislike.svg"),
                            width: 19.44,
                            height: 20,
                            color: dislike
                                ? Color(0xFFE15656)
                                : Color(0xFF949494))),
                    label: InkWell(
                        onTap: () {
                          Navigator.pushNamed(
                            context,
                            "/listlikedislike",
                            arguments: {
                              'id': widget.id,
                              'nom': widget.titre,
                              'page': 1
                            },
                          );
                        },
                        child: Text(
                          nbr_dislike.toString(),
                          style: TextStyle(
                              color: dislike
                                  ? Color(0xFFE15656)
                                  : Color(0xFF949494),
                              fontSize: 18,
                              fontFamily: "Helvetica",
                              fontWeight: FontWeight.normal),
                        ))),
              ]))
      ])),
      MyWorldPage(),
    ];
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: appBar,
        body: Container(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: Footer(_selectedIndex, _onItemTapped));
  }

  likeApi() {
    getToken().then((token) => {
          data = ClientApi.likePodcast(token, widget.id),
          data
              .then((value) => {
                    setState(() {
                      if (like) {
                        nbr_like = nbr_like - 1;
                      } else {
                        nbr_like = nbr_like + 1;
                      }
                      like = !like;
                    })
                  })
              .catchError((error) => {print("erreur : " + error.toString())}),
        });
  }

  disLikeApi() {
    getToken().then((token) => {
          data = ClientApi.dislikePodcast(token, widget.id),
          data
              .then((value) => {
                    setState(() {
                      if (dislike) {
                        nbr_dislike = nbr_dislike - 1;
                      } else {
                        nbr_dislike = nbr_dislike + 1;
                      }
                      dislike = !dislike;
                    })
                  })
              .catchError((error) => {print("erreur : " + error.toString())}),
        });
  }

  String getDate(DateTime date) {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final yesterday = DateTime(now.year, now.month, now.day - 1);
    String finaldate;
    final aDate = DateTime(date.year, date.month, date.day);
    if (aDate == today) {
      finaldate = DateFormat("h:mm a").format(date.toLocal());
    } else if (aDate == yesterday) {
      finaldate = AppLocalizations.of(context).translate('yesterday');
    } else {
      finaldate = DateFormat("EEE, dd MMM").format(date.toLocal());
    }
    return finaldate;
  }

  Duration parseDuration(String s) {
    int hours = 0;
    int minutes = 0;
    int micros;
    List<String> parts = s.split(':');
    if (parts.length > 2) {
      hours = int.parse(parts[parts.length - 3]);
    }
    if (parts.length > 1) {
      minutes = int.parse(parts[parts.length - 2]);
    }
    micros = (double.parse(parts[parts.length - 1]) * 1000000).round();
    return Duration(hours: hours, minutes: minutes, microseconds: micros);
  }

  static String formatDuration(Duration d) {
    var seconds = d.inSeconds;
    final days = seconds ~/ Duration.secondsPerDay;
    seconds -= days * Duration.secondsPerDay;
    final hours = seconds ~/ Duration.secondsPerHour;
    seconds -= hours * Duration.secondsPerHour;
    final minutes = seconds ~/ Duration.secondsPerMinute;
    seconds -= minutes * Duration.secondsPerMinute;

    final List<String> tokens = [];

    if (hours != 0) {
      tokens.add('${hours}h');
    }
    if (minutes != 0) {
      tokens.add('${minutes}min');
    }
    if (seconds != 0) {
      tokens.add('${seconds}s');
    }
    return tokens.join('');
  }
}

Future<String> getToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("apikey");
}

Future<int> getID() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getInt("id");
}

Future<String> setToken(String token) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('token', token);
}
