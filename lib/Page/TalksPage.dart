import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/Page/PodcastWidget.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Model/Podcasts.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Talks extends StatefulWidget {
  TalksPage createState() => TalksPage();
}

class TalksPage extends State<Talks> {
  var listPodcast = new List<PodcastsData>();
  Future<Podcasts> podcast;
  int currentPage = 1;
  int maxPage;
  ScrollController _scrollController;
  bool show = false;
  List<Map<String, String>> listFavoris = [
    {
      "id": "15",
      'image': "public/uploads/profile/avatar.png",
      'name': "Nathan\nLaufer"
    },
    {
      "id": "15",
      'image': "public/uploads/profile/1584962021-ahmed.png",
      'name': "Axel\nFreon"
    },
    {
      "id": "15",
      'image': "public/uploads/profile/1584962021-ahmed.png",
      'name': "cave",
    },
    {
      "id": "15",
      'image': "public/uploads/profile/1584962021-ahmed.png",
      'name': "astronaut"
    },
    {
      "id": "15",
      'image': "public/uploads/profile/1584962021-ahmed.png",
      'name': "robot1"
    },
    {
      "id": "15",
      'image': "public/uploads/profile/1584962021-ahmed.png",
      'name': "mosquito",
    },
  ];

  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent &&
          currentPage < maxPage) {
        _getMoreData();
      }
    });
    getToken().then((cleapi) {
      getAllPodcast(cleapi, 1);
    });
  }

  _getMoreData() {
    currentPage++;
    getToken().then((cleapi) {
      podcast = ClientApi.getAllPodcast(cleapi, currentPage);
      podcast
          .then((value) => {
                EasyLoading.dismiss(),
                setState(() {
                  print(value.data);
                  listPodcast = listPodcast + value.data;
                }),
              })
          .catchError((error) =>
              {EasyLoading.dismiss(), print("erreur : " + error.toString())});
    });
  }

  final textFavoris = new Text("Favoris",
      style: TextStyle(
        fontFamily: 'Helvetica',
        color: Color(0xff3f3c3c),
        fontSize: 20,
        fontWeight: FontWeight.w700,
        fontStyle: FontStyle.normal,
        letterSpacing: 0.39,
      ));

  final textAjouterFavoris = new Text("Ajouter un favoris",
      style: TextStyle(
        fontFamily: 'Roboto',
        color: Color(0xff949494),
        fontSize: 13,
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
        letterSpacing: 0.1559999999999999,
      ),
      textAlign: TextAlign.center);

  final iconAdd =
      SvgPicture.asset(("assets/icon/plus.svg"), width: 22, height: 22);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Color(0xfff8f9f9),
        child: Column(children: [
          /* Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                  child: textFavoris,
                  padding: EdgeInsets.fromLTRB(8, 15, 0, 8))),
          Container(
              height: 110,
              child: new ListView.builder(
                  itemCount: listFavoris.length + 1,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext ctxt, int index) {
                    if (index == 0)
                      return Container(
                          margin: EdgeInsets.only(left: 8.0),
                          width: 90,
                          height: 110,
                          decoration: new BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(10)),
                          child: DottedBorder(
                              radius: Radius.circular(10),
                              dashPattern: [4],
                              strokeWidth: 1,
                              strokeCap: StrokeCap.round,
                              borderType: BorderType.RRect,
                              color: Color(0xffc6c6c6),
                              child: Container(
                                  child: Column(
                                children: [
                                  Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 19),
                                      child: iconAdd),
                                  Padding(
                                      padding:
                                          const EdgeInsets.fromLTRB(0, 0, 0, 8),
                                      child: textAjouterFavoris)
                                ],
                              ))));
                    else
                      return new 
                      GestureDetector(onTap: (){
                                    Navigator.pushNamed(context, "/chainePage", arguments: {'id': int.parse(listFavoris[index - 1]['id']),'username': listFavoris[index - 1]['name']});

                      },child:
                      
                      Container(
                          margin: EdgeInsets.only(left: 12.0),
                          width: 90,
                          height: 110,
                          decoration: new BoxDecoration(
                            color: Color(0xffffffff),
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0x0d000000),
                                  offset: Offset(0, 0),
                                  blurRadius: 20,
                                  spreadRadius: 0)
                            ],
                          ),
                          child: Column(children: [
                            Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(0, 16, 0, 10),
                                child: GestureDetector(
                                    onTap: () {},
                                    child: Container(
                                      width: 40,
                                      height: 40,
                                      decoration: new BoxDecoration(
                                          image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: NetworkImage(
                                                Endpoints.baseUrlImage +
                                                    listFavoris[index - 1]
                                                        ['image']),
                                          ),
                                          color: Color(0xffd8d8d8),
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 13),
                                    ))),
                            Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: Text(listFavoris[index - 1]['name'],
                                    style: TextStyle(
                                      fontFamily: 'Roboto',
                                      color: Color(0xff949494),
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0.1559999999999999,
                                    ),
                                    textAlign: TextAlign.center))
                          ])));
                  })),*/
          Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                  child: Text(
                      AppLocalizations.of(context)
                          .translate('talkers_to_listen'),
                      style: TextStyle(
                        fontFamily: 'Helvetica',
                        color: Color(0xff3f3c3c),
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0.39,
                      )),
                  padding: EdgeInsets.fromLTRB(8, 30, 0, 8))),
          Expanded(
              child: (listPodcast.isEmpty && show)
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                            AppLocalizations.of(context)
                                .translate('empty_talks_list'),
                            style: TextStyle(
                              fontFamily: 'Helvetica',
                              color: Color(0xff9b9b9b),
                              fontSize: 21,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ))
                      ],
                    )
                  : ListView.builder(
                      itemCount: currentPage == maxPage
                          ? listPodcast.length
                          : listPodcast.length + 1,
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      controller: _scrollController,
                      itemBuilder: (BuildContext ctxt, int index) {
                        if (listPodcast.isNotEmpty) {
                          return (index >= listPodcast.length)
                              ? Center(child: CupertinoActivityIndicator())
                              : GestureDetector(
                                  onTap: () {
                                    Navigator.pushNamed(context, "/detailTalks",
                                        arguments: {
                                          'id': listPodcast[index].id,
                                          'titre': listPodcast[index].nom,
                                        }).then((value) => {
                                          Navigator.of(context)
                                              .pushNamedAndRemoveUntil(
                                                  '/home',
                                                  (Route<dynamic> route) =>
                                                      false,
                                                  arguments: {
                                                'selectedindex': 1
                                              }),
                                        });
                                  },
                                  child: PodcastWidget(
                                    listPodcast[index],
                                    refreshPodcast: () {
                                      Navigator.of(context)
                                          .pushNamedAndRemoveUntil('/home',
                                              (Route<dynamic> route) => false,
                                              arguments: {'selectedindex': 1});
                                    },
                                    reset: () => reset(listPodcast[index].id),
                                  ));
                        }
                        return null;
                      }))
        ]));
  }

  reset(id) {
    for (int i = 0; i < listPodcast.length; i++) {
      if (listPodcast[i].playerState == PlayerState.playing)
        setState(() {
          listPodcast[i].playerState = PlayerState.stopped;
        });
    }
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  getAllPodcast(String token, int page) {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    // here you "register" to get "notifications" when the getEmail method is done.
    podcast = ClientApi.getAllPodcast(token, page);
    podcast
        .then((value) => {
              EasyLoading.dismiss(),
              setState(() {
                print(value.data);
                listPodcast = value.data;
                maxPage = value.pagination.pages;
                show = true;
              })
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }
}
