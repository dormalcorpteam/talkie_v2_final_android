import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Model/data.dart';

class Activate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ActivatePage();
  }
}

class ActivatePage extends StatefulWidget {
  @override
  _ActivatePageState createState() => _ActivatePageState();
}

class _ActivatePageState extends State<ActivatePage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  Future<Data> data;
  FocusNode _focusNode = FocusNode();
  TextEditingController codeController = new TextEditingController();
  TextStyle style = TextStyle(
    fontFamily: 'Ubuntu',
    color: Color(0xffffffff),
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 0,
  );
  bool is_checked = false;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final codeField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            onChanged: (value) {
              if (value.isNotEmpty) {
                setState(() {
                  is_checked = true;
                });
              } else {
                setState(() {
                  is_checked = false;
                });
              }
            },
            style: style,
            controller: codeController,
            decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('enter_code'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));

    final sendButon = Padding(
        padding: EdgeInsets.fromLTRB(38, 71, 37, 0),
        child: SizedBox(
          width: double.infinity, // match_parent
          height: 45,
          child: RaisedButton(
            elevation: 0,
            color: (is_checked) ? Colors.white : Color(0xff6cc5b8),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            onPressed: () {
              EasyLoading.instance
                ..loadingStyle = EasyLoadingStyle.custom
                ..backgroundColor = Colors.white
                ..progressColor = Colors.white
                ..indicatorType = EasyLoadingIndicatorType.wave
                ..indicatorColor = Color(0xff3bc0ae)
                ..textColor = Color(0xff3bc0ae)
                ..userInteractions = false
                ..maskType = EasyLoadingMaskType.black;
              EasyLoading.show(
                status: AppLocalizations.of(context).translate('loading'),
              );

              data = ClientApi.activateApi(codeController.text);
              data
                  .then((value) => {
                        EasyLoading.instance.loadingStyle =
                            EasyLoadingStyle.light,
                        EasyLoading.dismiss(),
                        Fluttertoast.showToast(
                            msg: value.data,
                            toastLength: Toast.LENGTH_LONG,
                            gravity: ToastGravity.CENTER,
                            backgroundColor: Colors.black,
                            textColor: Colors.white,
                            fontSize: 16.0),
                        Navigator.pushNamedAndRemoveUntil(
                            context, "/login", (r) => false, arguments: {
                          'text': AppLocalizations.of(context).translate('account_created')
                        })
                      })
                  .catchError((error) => {
                        EasyLoading.dismiss(),
                        EasyLoading.instance.loadingStyle =
                            EasyLoadingStyle.custom,
                        EasyLoading.instance.backgroundColor = Colors.red,
                        EasyLoading.instance.indicatorColor = Colors.white,
                        EasyLoading.instance.progressColor = Colors.white,
                        EasyLoading.instance.textColor = Colors.white,
                        EasyLoading.showError(AppLocalizations.of(context).translate('incorrect_code'))
                      });
            },
            child: Text(AppLocalizations.of(context).translate('next'),
                style: TextStyle(
                  fontFamily: 'Ubuntu',
                  color: Color(0xff25a996),
                  fontSize: 15,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                )),
          ),
        ));
    _willPopCallback() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('route', "activation");
    }

    return Scaffold(
        backgroundColor: Color(0xff25a996),
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Color(0xff25a996),
        ),
        body: WillPopScope(
          onWillPop: () => _willPopCallback(),
          child: Stack(children: <Widget>[
            SingleChildScrollView(
              child: new GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onPanDown: (_) {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                behavior: HitTestBehavior.translucent,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 172.0,
                        child: Image.asset(
                          "assets/logo.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                      Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                              padding:
                                  EdgeInsets.only(top: 0, right: 52, left: 18),
                              child: Text("Activation",
                                  style: TextStyle(
                                    fontFamily: 'Salome',
                                    color: Color(0xffffffff),
                                    fontSize: 28,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  )))),
                                  
                      Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                              padding:
                                  EdgeInsets.only(top: 15, right: 52, left: 18),
                              child: Text(
                                  AppLocalizations.of(context).translate('enter_code_email'),
                                  style: TextStyle(
                                    fontFamily: 'Ubuntu',
                                    color: Color(0xffffffff),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  )))),
                      codeField,
                      SizedBox(
                        height: 25.0,
                      ),
                      sendButon,
                      SizedBox(
                        height: 25.0,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ]),
        ));
  }
}
