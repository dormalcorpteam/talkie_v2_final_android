import 'dart:io';

import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/Page/AppBarHeader.dart';
import 'package:voicechanger/Page/Footer.dart';
import 'package:voicechanger/Page/discussionPage.dart';
import 'package:voicechanger/Page/myWorldPage.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/Chaine.dart';
import 'package:voicechanger/data/network/Model/Podcasts.dart';
import 'package:voicechanger/data/network/Model/data.dart';

import 'PodcastWidget.dart';

class ChainePage extends StatefulWidget {
  @override
  _ChainePage createState() => _ChainePage();
}

class _ChainePage extends State<ChainePage> {
  @override
  Widget build(BuildContext context) {
    Map arguments;
    String username;
    int id;
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    if (arguments != null) {
      print(arguments['username']);
      username = arguments['username'];
      print(arguments['id']);
      id = arguments['id'];
    }
    return ChaineProfilPage(username: username, id: id);
  }
}

class ChaineProfilPage extends StatefulWidget {
  final String username;
  final int id;
  ChaineProfilPage({Key key, this.username, this.id});

  @override
  _ChaineProfilPageState createState() => _ChaineProfilPageState();
}

class _ChaineProfilPageState extends State<ChaineProfilPage> {
  int _selectedIndex = 1;
  Widget appBar;
  bool isFavoris = false;
  bool isAbonne;
  Future<Chaine> chaine;
  UserChaine user;
  Future<Data> data;
  bool show = false;
  var listPodcast = new List<PodcastsData>();
  int nbr_listeners;
  int nbr_talkers;
  String language;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;

      if (index == 0) {
        appBar = AppBarHeader(
            name: "Discussions",
            buttonback: false,
            actionTalk: false,
            actionmessage: true);
      }

      if (index == 1) {
        appBar = AppBar(
          elevation: 0,
          centerTitle: true,
                  backgroundColor: Color(0xff25a996),

          title: RichText(
            textAlign: TextAlign.center,
            text: (user != null)
                ? TextSpan(
                    text: user.name,
                    style: TextStyle(
                      fontFamily: 'Helvetica',
                      color: Color(0xffffffff),
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                    children: <TextSpan>[
                        TextSpan(
                          text: '\n${user.username}',
                          style: TextStyle(
                            fontFamily: 'Helvetica-thin',
                            color: Color(0xffffffff),
                            fontSize: 14,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      ])
                : TextSpan(),
          ),
        );
      }
      if (index == 2) {
        appBar = null;
      }
    });
  }

  getChaine(String token, int id) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    chaine = ClientApi.getChaine(token, id);
    chaine
        .then((value) => {
              EasyLoading.dismiss(),
              setState(() {
                nbr_listeners = value.nbrListeners;
                nbr_talkers = value.nbrTalkers;
                user = value.user;
                listPodcast = value.data;
                show = true;
                appBar = AppBar(
                  elevation: 0,
                          backgroundColor: Color(0xff25a996),

                  centerTitle: true,
                  title: RichText(
                    textAlign: TextAlign.center,
                    text: (user != null)
                        ? TextSpan(
                            text: user.name,
                            style: TextStyle(
                              fontFamily: 'Helvetica',
                              color: Color(0xffffffff),
                              fontSize: 17,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                            children: <TextSpan>[
                                TextSpan(
                                  text: '\n${user.username}',
                                  style: TextStyle(
                                    fontFamily: 'Helvetica',
                                    color: Color(0xffffffff),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                              ])
                        : TextSpan(),
                  ),
                );
                if (value.user.subscriber == 0) {
                  isAbonne = false;
                } else {
                  isAbonne = true;
                }
              })
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }

  subscribeUser(String token, int id) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    data = ClientApi.subscribeUser(token, id);
    data
        .then((value) => {
              EasyLoading.dismiss(),
              EasyLoading.showSuccess(value.data),
              setState(() {
                isAbonne = true;
                nbr_listeners = nbr_listeners + 1;
              })
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }

  unsubscribeUser(String token, int id) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    data = ClientApi.unsubscribeUser(token, id);
    data
        .then((value) => {
              EasyLoading.dismiss(),
              EasyLoading.showSuccess(value.data),
              setState(() {
                isAbonne = false;
                nbr_listeners = nbr_listeners - 1;
              })
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }

  @override
  void initState() {
    super.initState();
    getToken().then((cleapi) => {getChaine(cleapi, widget.id)});
    Devicelocale.currentLocale.then((value) => {
          setState(() {
            language = value.split(Platform.isAndroid ? '_' : '-')[0];
          })
        });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _widgetOptions = <Widget>[
      DiscussionPage(),
       WillPopScope(
        onWillPop: () async {
          
            Navigator.pop(context);
          
          return false;
        },
        child: 

      Scaffold(
        body: Container(
            color: Color(0xfff8f9f9),
            child: Column(children: [
              if (user != null)
                ListTile(
                  contentPadding: EdgeInsets.fromLTRB(25, 25, 9, 25),
                  leading: Container(
                    width: 52,
                    height: 52,
                    decoration: new BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image:
                              NetworkImage(Endpoints.baseUrlImage + user.photo),
                        ),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x33000000),
                              offset: Offset(0, 0),
                              blurRadius: 5,
                              spreadRadius: 0)
                        ],
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  title: Padding(
                      padding: EdgeInsets.only(top: 7),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(user.username,
                              style: TextStyle(
                                fontFamily: 'Helvetica',
                                color: Color(0xff3F3C3C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0.2708333333333333,
                              )))),
                  subtitle: Padding(
                      padding: EdgeInsets.only(top: 5),
                      child: Column(children: [
                        Align(
                                alignment: Alignment.centerLeft,
                                child:  InkWell(
                            onTap: () {
                              Navigator.pushNamed(
                                context,
                                "/listtalkslisteners",
                                arguments: {
                                  'id': user.id,
                                  'username': user.name,
                                  'page': 0
                                },
                              );
                            },
                            child:Text("$nbr_listeners listeners",
                                    style: TextStyle(
                                      fontFamily: 'Helvetica',
                                      color: Color(0xff949494),
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0.2708333333333333,
                                    )))),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                               Padding(
                                      padding: EdgeInsets.only(top: 5),
                                      child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: InkWell(
                                  onTap: () {
                                    Navigator.pushNamed(
                                      context,
                                      "/listtalkslisteners",
                                      arguments: {
                                        'id': user.id,
                                        'username': user.name,
                                        'page': 1
                                      },
                                    );
                                  },
                                  child:Text("$nbr_talkers talkers",
                                              style: TextStyle(
                                                fontFamily: 'Helvetica',
                                                color: Color(0xff949494),
                                                fontSize: 13,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                                letterSpacing:
                                                    0.2708333333333333,
                                              ))))),
                            TextButton(
                                      onPressed: () {
                                        if (isAbonne) {
                                          getToken().then((cleapi) => {
                                                unsubscribeUser(
                                                    cleapi, widget.id)
                                              });
                                        } else {
                                          getToken().then((cleapi) => {
                                                subscribeUser(cleapi, widget.id)
                                              });
                                        }
                                      },
                                      child: (isAbonne == false)
                                          ? Text(
                                              AppLocalizations.of(context)
                                                  .translate('follow'),
                                              style: TextStyle(
                                                fontFamily: 'Helvetica',
                                                color: Color(0xff25a996),
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                                letterSpacing: 0.25,
                                              ))
                                          : Text(
                                              AppLocalizations.of(context)
                                                  .translate('unfollow'),
                                              style: TextStyle(
                                                fontFamily: 'Helvetica',
                                                color: Color(0xff25a996),
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                                letterSpacing: 0.25,
                                              ))),
                            ])
                        /*GestureDetector(
                onTap: () {
                  setState(() {
                    isFavoris = !isFavoris;
                  });
                },
                child: SvgPicture.asset(("assets/icon/fav.svg"),
                    color:
                        (isFavoris) ? Color(0xff25a996) : Color(0xff949494)))*/
                      ])),
                  /*  trailing:  Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 100, 0),
                      child: TextButton(
                         
                          onPressed: () {
                            if (isAbonne) {
                              getToken().then((cleapi) =>
                                  {unsubscribeUser(cleapi, widget.id)});
                            } else {
                              getToken().then((cleapi) =>
                                  {subscribeUser(cleapi, widget.id)});
                            }
                          },
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: (isAbonne == false)
                                  ? Text(AppLocalizations.of(context).translate('follow'),
                                      style: TextStyle(
                                        fontFamily: 'Helvetica',
                                        color: Color(0xff25a996),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0.25,
                                      ))
                                  : Text(AppLocalizations.of(context).translate('unfollow'),
                                      style: TextStyle(
                                        fontFamily: 'Helvetica',
                                        color: Color(0xff25a996),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0.25,
                                      )))),
                    )*/
                ),
              if (user != null)
                Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                        child: Text(
                            (language == "fr")
                                ? "Talk de ${user.username}"
                                : "${user.username} Talk",
                            style: TextStyle(
                              fontFamily: 'Helvetica',
                              color: Color(0xff3f3c3c),
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0.4166666666666666,
                            )),
                        padding: EdgeInsets.fromLTRB(8, 0, 0, 0))),
              SizedBox(height: 12),
              Expanded(
                  child: (listPodcast.isEmpty && show)
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SvgPicture.asset(("assets/icon/mic-3-barr.svg")),
                            Text(
                                "${user.username} " +
                                    AppLocalizations.of(context)
                                        .translate('empty_talks_chain'),
                                style: TextStyle(
                                  fontFamily: 'Helvetica',
                                  color: Color(0xff9b9b9b),
                                  fontSize: 21,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ))
                          ],
                        )
                      : ListView.builder(
                          itemCount: listPodcast.length,
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          itemBuilder: (BuildContext ctxt, int index) {
                            return GestureDetector(
                                onTap: () {
                                  Navigator.pushNamed(context, "/detailTalks",
                                      arguments: {
                                        'id': listPodcast[index].id,
                                        'titre': listPodcast[index].nom,
                                      }).then((value) => {
                                        Navigator.of(context)
                                            .pushReplacementNamed('/chainePage',
                                                arguments: {
                                              'id': user.id,
                                              'username': user.username
                                            }),
                                      });
                                },
                                child: PodcastWidget(listPodcast[index],
                                    reset: () => reset(listPodcast[index].id),
                                    refreshPodcast: () {
                                      Navigator.of(context)
                                          .pushReplacementNamed('/chainePage',
                                              arguments: {
                                            'id': user.id,
                                            'username': user.username
                                          });
                                    }));
                          })),
            ])),
      )),
      MyWorldPage(),
    ];
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    return Scaffold(
        appBar: appBar,
        body: Container(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: Footer(_selectedIndex, _onItemTapped));
  }

  reset(id) {
    for (int i = 0; i < listPodcast.length; i++) {
      if (listPodcast[i].playerState == PlayerState.playing)
        setState(() {
          listPodcast[i].playerState = PlayerState.stopped;
        });
    }
  }
}

Future<String> getToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("apikey");
}
