import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/Page/addCommentPage.dart';
import 'package:voicechanger/Page/commentsPage.dart';
import 'package:voicechanger/Page/editPodcastPage1.dart';
import 'package:voicechanger/Page/listLikeDislikePage.dart';
import 'package:voicechanger/Page/listTalkersListenersPage.dart';
import 'package:voicechanger/Page/onBoarding.dart';
import 'package:voicechanger/app_localizations.dart';

import 'AppLanguage.dart';
import 'Page/InscriptionPage.dart';
import 'Page/TalksPage.dart';
import 'Page/activationPage.dart';
import 'Page/activationPage1.dart';
import 'Page/chainePage.dart';
import 'Page/createPodcastPage2.dart';
import 'Page/creationGroupePage1.dart';
import 'Page/creationGroupePage2.dart';
import 'Page/detailCategoriePage.dart';
import 'Page/detailMessagePage.dart';
import 'Page/detailTalksPage.dart';
import 'Page/editGroupePage.dart';
import 'Page/editPodcastPage2.dart';
import 'Page/explorerPage.dart';
import 'Page/infoGroupePage.dart';
import 'Page/inscriptionPage2.dart';
import 'Page/loginPage.dart';
import 'Page/HomePage.dart';
import 'Page/nouvelleDiscussionPage.dart';
import 'Page/profilePage.dart';
import 'Page/resetPasswordPage1.dart';
import 'Page/resetPasswordPage2.dart';
import 'Page/resetPasswordPage3.dart';
import 'Page/signalerContactPage.dart';
import 'Page/suggestionConversationsPage.dart';
import 'package:firebase_core/firebase_core.dart';

AppLanguage appLanguage;
Locale myLocale;

main() async => {
      WidgetsFlutterBinding.ensureInitialized(),
      await Firebase.initializeApp(),
      appLanguage = AppLanguage(),
      await appLanguage.fetchLocale(),
      getToken().then((token) => {
            print(token),
            runApp(ChangeNotifierProvider<AppLanguage>(
                create: (_) => appLanguage,
                child: Consumer<AppLanguage>(builder: (context, model, child) {
                  return MaterialApp(
                      locale: model.appLocal,
                      supportedLocales: [
                        Locale('en', ''),
                        Locale('fr', ''),
                      ],
                      localizationsDelegates: [
                        AppLocalizations.delegate,
                        GlobalMaterialLocalizations.delegate,
                        GlobalWidgetsLocalizations.delegate,
                      ],
                      debugShowCheckedModeBanner: false,
                      title: 'Talkie',
                      theme: ThemeData(
                        fontFamily: 'SFProText',
                        primaryColor: Color(0xff25a996),
                      ),
                      routes: {
                        '/detailmessage': (BuildContext context) =>
                            DetailMessagePage(),
                        '/login': (BuildContext context) => LoginPage(),
                        '/profile': (BuildContext context) => ProfilePage(),
                        '/home': (BuildContext context) => Home(),
                        '/detailTalks': (BuildContext context) => DetailTalks(),
                        '/talks': (BuildContext context) => Talks(),
                        '/chainePage': (BuildContext context) => ChainePage(),
                        '/explorerPage': (BuildContext context) =>
                            ExplorerPage(),
                        '/detailCategorie': (BuildContext context) =>
                            DetailCategoriePage(),
                        '/nouvellediscussion': (BuildContext context) =>
                            NouvelleDiscussionPage(),
                        '/creatingroup1': (BuildContext context) =>
                            CreationGroupePage1(),
                        '/creatingroup2': (BuildContext context) =>
                            CreationGroupePage2(),
                        '/infogroup': (BuildContext context) =>
                            InfoGroupePage(),
                        '/editGroup': (BuildContext context) =>
                            EditGroupePage(),
                        '/listsuggestions': (BuildContext context) =>
                            SuggestionConversationsPage(),
                        '/createpodcast2': (BuildContext context) =>
                            CreatePodcastPage2(),
                        '/resetpassword1': (BuildContext context) =>
                            ResetPassword1(),
                        '/resetpassword2': (BuildContext context) =>
                            ResetPassword2(),
                        '/resetpassword3': (BuildContext context) =>
                            ResetPassword3(),
                        '/inscription': (BuildContext context) =>
                            InscriptionPage(),
                        '/inscription2': (BuildContext context) =>
                            Inscription2Page(),
                        '/activate': (BuildContext context) => Activate(),
                        '/activate1': (BuildContext context) => Activate1(),
                        '/listtalkslisteners': (BuildContext context) =>
                            ListTalkersListenersPage(),
                        '/editpodcast1': (BuildContext context) =>
                            EditPodcastPage1(),
                        '/editpodcast2': (BuildContext context) =>
                            EditPodcastPage2(),
                        '/signalercontact': (BuildContext context) =>
                            SignalerContactPage(),
                        '/comments': (BuildContext context) => CommentsPage(),
                        '/addcomment': (BuildContext context) =>
                            AddCommentPage(),
                        '/listlikedislike': (BuildContext context) =>
                            ListLikeDislikePage(),
                      },
                      builder: (BuildContext context, Widget child) {
                        return FlutterEasyLoading(
                            child: GestureDetector(
                          onTap: () {
                            FocusScopeNode currentFocus =
                                FocusScope.of(context);
                            if (!currentFocus.hasPrimaryFocus) {
                              currentFocus.unfocus();
                            }
                          },
                          child: child,
                        ));
                      },
                      home: (token != null) ? Home() : OnBoardingPage());
                }))),
          })
    };

Future<String> getToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("apikey");
}
