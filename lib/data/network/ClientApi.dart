import 'dart:io';

import 'package:voicechanger/Page/myWorldPage.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/DetailGroupe.dart';
import 'package:voicechanger/data/network/Model/DetailPodcast.dart';
import 'package:voicechanger/data/network/Model/ListeAmis.dart';
import 'package:voicechanger/data/network/Model/SearchUser.dart';
import 'package:voicechanger/data/network/Model/dataGroupe.dart';
import 'package:voicechanger/data/network/Model/detailMessage.dart';
import 'package:voicechanger/data/network/Model/listComments.dart';
import 'package:voicechanger/data/network/Model/suggestionConversations.dart';
import 'package:voicechanger/data/network/Model/tab.dart';
import 'dart:convert';

import 'Model/Chaine.dart';
import 'Model/MyWorld.dart';
import 'Model/Podcasts.dart';
import 'Model/filtre.dart';
import 'Model/user.dart';

import 'Model/categories.dart';
import 'Model/login.dart';
import 'Model/data.dart';
import 'Model/message.dart';
import 'Model/header.dart';

class ClientApi {
  static Future<Login> loginApi(String username, String password) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.postUrl(Uri.parse(Endpoints.baseUrl + '/api/login_check'));
    request.headers.set('content-type', 'application/json');
    Map map = {"username": username, "password": password};
    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Login.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to connect');
    }
  }

  static Future<User> registerApi(
    String username,
    String email,
    String nom,
    String prenom,
    String password,
    String dateNaissance,
    String photo,
    String nomPhoto,
    String tel,
    String prefixe_tel,
    String code_pays_tel,
    String token_firebase,
  ) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.postUrl(Uri.parse(Endpoints.baseUrl + '/auth/register'));
    request.headers.set('content-type', 'application/json');
    Map map = {
      "username": username,
      "email": email,
      "nom": nom,
      "prenom": prenom,
      "password": password,
      "dateNaissance": dateNaissance,
      "photo": photo,
      "nomPhoto": nomPhoto,
      "tel": tel,
      "prefixe_tel": prefixe_tel,
      "code_pays_tel": code_pays_tel,
      "token_firebase": token_firebase
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return User.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (User.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> verifRegisterApi(
    String username,
    String email,
    String nom,
    String prenom,
    String dateNaissance,
    String tel,
    String prefixe_tel,
    String code_pays_tel,
  ) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/auth/verif-data-register'));
    request.headers.set('content-type', 'application/json');
    Map map = {
      "username": username,
      "email": email,
      "nom": nom,
      "prenom": prenom,
      "dateNaissance": dateNaissance,
      "tel": tel,
      "prefixe_tel": prefixe_tel,
      "code_pays_tel": code_pays_tel,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> forgotApi(String email) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/auth/forgot-password'));
    request.headers.set('content-type', 'application/json');
    Map map = {"email": email};
    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> activateCheckApi(String email) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/auth/get-activation-code'));
    request.headers.set('content-type', 'application/json');
    Map map = {"email": email};
    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> resetApi(String code, String password) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/auth/reset-password'));
    request.headers.set('content-type', 'application/json');
    Map map = {"code": code, "password": password};
    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> activateApi(String code) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/auth/activate-account'));
    request.headers.set('content-type', 'application/json');
    Map map = {"code": code};
    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Header> getPhotoApi(String token) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.getUrl(Uri.parse(Endpoints.baseUrl + '/api/get-photo'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Header.fromJson(json.decode(reply));
    } else if (response.statusCode == 401) {
      throw (response.statusCode);
    } else {
      // If that response was not OK, throw an error.
      throw (Header.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Messagee> getListemessages(String token, int page) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(
        Uri.parse(Endpoints.baseUrl + '/api/liste-conversation?page=$page'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Messagee.fromJson(json.decode(reply));
    } else if (response.statusCode == 401) {
      throw (response.statusCode);
    } else {
      // If that response was not OK, throw an error.
      throw ("Erreur");
    }
  }

  static Future<Categories> getListeCategories(String token) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.getUrl(Uri.parse(Endpoints.baseUrl + '/api/categories'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Categories.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("Erreur");
    }
  }

  static Future<DetailMessage> getDetailMessage(
      String token, int id, int page) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(Uri.parse(
        Endpoints.baseUrl +
            '/api/liste-message-pagination/${id.toString()}?page=$page'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return DetailMessage.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("Error");
    }
  }

  static Future<User> getUserDetail(String token) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.getUrl(Uri.parse(Endpoints.baseUrl + '/api/profile'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return User.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (User.fromJson(json.decode(reply)).error);
    }
  }

  static Future<User> editProfile(
    String token,
    String username,
    String email,
    String nom,
    String prenom,
    String password,
    String dateNaissance,
    String photo,
    String nomPhoto,
    String tel,
    String prefixe_tel,
    String code_pays_tel,
  ) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.postUrl(Uri.parse(Endpoints.baseUrl + '/api/profile'));
    request.headers.set('Content-Type', 'application/json');

    request.headers.set('Authorization', token);

    Map map = {
      "username": username,
      "email": email,
      "nom": nom,
      "prenom": prenom,
      "password": password,
      "dateNaissance": dateNaissance,
      "photo": photo,
      "nomPhoto": nomPhoto,
      "tel": tel,
      "prefixe_tel": prefixe_tel,
      "code_pays_tel": code_pays_tel,
    };

    request.add(utf8.encode(json.encode(map)));
    print(map);
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return User.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (User.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Filtre> filterVoice(
      String token, String filtername, String voice) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.postUrl(Uri.parse(Endpoints.baseUrl + '/api/send-voice'));
    request.headers.set('Authorization', token);

    Map map = {
      "filter_name": filtername,
      "voice": voice,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Filtre.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to connect');
    }
  }

  static Future<Data> sendMessage(String token, var id_voice, var id_recivers,
      String durre, var id_conv) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    List<IdConvs> idConvs = [];
    for (var i = 0; i < id_conv.length; i++) {
      idConvs.add(IdConvs(idConv: id_conv[i], idReceivers: id_recivers[i]));
    }

    print(json.encode(idConvs));

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/send-message'));
    request.headers.set('Authorization', token);

    Map map = {
      "id_voice": id_voice,
      "duration": durre.toString(),
      "id_convs": idConvs
    };
    print(map.toString());
    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<ListeAmis> searchFriends(String token, String key) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(
        Uri.parse(Endpoints.baseUrl + '/api/search-in-contact?key=$key'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return ListeAmis.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (ListeAmis.fromJson(json.decode(reply)).error);
    }
  }

  static Future<ListeAmis> listFriends(String token) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .getUrl(Uri.parse(Endpoints.baseUrl + '/api/list-contact?'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return ListeAmis.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (ListeAmis.fromJson(json.decode(reply)).error);
    }
  }

  static Future<SearchUser> searchUser(
      String token, String key, int page) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(
        Uri.parse(Endpoints.baseUrl + '/api/search-user?page=$page&key=$key'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return SearchUser.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (SearchUser.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> addUserConversation(
      String token, int id_conv, int id_user) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.postUrl(
        Uri.parse(Endpoints.baseUrl + '/api/add-user-to-conversation'));
    request.headers.set('Authorization', token);

    Map map = {
      "id_conv": id_conv,
      "id_user": id_user,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Messagee> searchMessage(String token, String key) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(
        Uri.parse(Endpoints.baseUrl + "/api/search-conversation?&key=$key"));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Messagee.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("erreur");
    }
  }

  static Future<Data> sendquickMessage(String token, String file,
      var id_recivers, String durre, var id_conv) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    List<IdConvs> idConvs = [];
    for (var i = 0; i < id_conv.length; i++) {
      idConvs.add(IdConvs(idConv: id_conv[i], idReceivers: id_recivers[i]));
    }

    print(json.encode(idConvs));

    Map map = {
      "file": file,
      "id_conv": id_conv,
      "duration": durre,
      "id_convs": idConvs
    };
    print("acb " + map.toString());

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/send-quick-message'));
    request.headers.set('Authorization', token);

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> testArray(var list) async {
    Map map;
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    String array = "";
    // make GET request
    for (var i = 0; i < list.length; i++) {
      if (i != list.length - 1) {
        array = array + "array[$i]=" + list[i] + "&";
      } else {
        array = array + "array[$i]=" + list[i];
      }
    }
    print(array);
    HttpClientRequest request = await client.postUrl(
        Uri.parse("http://193.70.3.61/talkie/public/get-array?$array"));
    request.headers.set('content-type', 'application/json');

    map = {"name": "aaa"};

    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      print(reply);
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("erreur");
    }
  }

  static Future<Podcasts> getAllPodcast(String token, int page) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .getUrl(Uri.parse(Endpoints.baseUrl + '/api/all-podcasts?page=$page'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Podcasts.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("erreur");
    }
  }

  static Future<DetailPodcast> getDetailPodcast(String token, int id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(
        Uri.parse(Endpoints.baseUrl + "/api/detail-podcast/${id.toString()}"));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return DetailPodcast.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("erreur");
    }
  }

  static Future<Chaine> getChaine(String token, int id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .getUrl(Uri.parse(Endpoints.baseUrl + "/api/chaine/${id.toString()}"));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Chaine.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("erreur");
    }
  }

  static Future<Data> subscribeUser(String token, int id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.postUrl(Uri.parse(Endpoints.baseUrl + '/api/subscribe'));
    request.headers.set('Authorization', token);

    Map map = {
      "id": id,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> unsubscribeUser(String token, int id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.postUrl(Uri.parse(Endpoints.baseUrl + '/api/unsubscribe'));
    request.headers.set('Authorization', token);

    Map map = {
      "id": id,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Podcasts> getPodcastByCategories(
      String token, int category, int page) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(Uri.parse(Endpoints
            .baseUrl +
        '/api/get-podcasts-category?page=$page&category=${category.toString()}'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Podcasts.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("erreur");
    }
  }

  static Future<DataGroupe> creategroupe(
      String token, String name, String photo, var list) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    String array = "";
    // make GET request
    for (var i = 0; i < list.length; i++) {
      if (i != list.length - 1) {
        array = array + "ids[$i]=" + list[i].id.toString() + "&";
      } else {
        array = array + "ids[$i]=" + list[i].id.toString();
      }
    }
    print(array);
    print(name);
    print(photo);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/create-groupe?$array'));
    request.headers.set('Authorization', token);

    Map map = {"name": name, "photo": photo};

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return DataGroupe.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (DataGroupe.fromJson(json.decode(reply)).error);
    }
  }

  static Future<DetailGroupe> getInfoGroup(String token, int id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(
        Uri.parse(Endpoints.baseUrl + '/api/info-groupe/${id.toString()}'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return DetailGroupe.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("erreur");
    }
  }

  static Future<DataGroupe> editgroupe(
      String token, String name, String photo, var list, int id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    String array = "";
    // make GET request
    for (var i = 0; i < list.length; i++) {
      if (i != list.length - 1) {
        array = array + "ids[$i]=" + list[i].id.toString() + "&";
      } else {
        array = array + "ids[$i]=" + list[i].id.toString();
      }
    }
    print(array);
    print(name);
    print(photo);
    print(id.toString());

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/edit-groupe?$array'));
    request.headers.set('Authorization', token);

    Map map = {
      "name": name,
      "photo": photo,
      "id": id,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return DataGroupe.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (DataGroupe.fromJson(json.decode(reply)).error);
    }
  }

  static Future<SuggestionConversation> getSuggestionConv(String token) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .getUrl(Uri.parse(Endpoints.baseUrl + '/api/suggestion-conversation'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return SuggestionConversation.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("erreur");
    }
  }

  static Future<MyWorld> getMyWorld(String token) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.getUrl(Uri.parse(Endpoints.baseUrl + '/api/my-world'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return MyWorld.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("erreur");
    }
  }

  static Future<Data> createPodcast(String token, String nom, int category,
      var list, String file, String duree, String description) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    String array = "";
    // make GET request
    for (var i = 0; i < list.length; i++) {
      if (i != list.length - 1) {
        array = array + "tags[$i]=" + list[i] + "&";
      } else {
        array = array + "tags[$i]=" + list[i];
      }
    }
    print(array);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/add-podcast?$array'));
    request.headers.set('Authorization', token);

    Map map = {
      "nom": nom,
      "category": category,
      "dure": duree,
      "file": file,
      "description": description,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (DataGroupe.fromJson(json.decode(reply)).error);
    }
  }

  static Future<SuggestionConversation> searchSuggestionConv(
      String token, String key) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(Uri.parse(
        Endpoints.baseUrl + '/api/search-suggestion-conversation?key=$key'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return SuggestionConversation.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("erreur");
    }
  }

  static Future<Data> getUserID(String token) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .getUrl(Uri.parse(Endpoints.baseUrl + '/api/current-user-id'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("Error");
    }
  }

  static Future<Data> lastFilteredVoice(String token, String id_voice) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(Uri.parse(
        Endpoints.baseUrl + '/api/last-filtered-voice?id_voice=$id_voice'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<SearchUser> getListTalkers(String token, int id) async {
    print("aaa " + id.toString());
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(
        Uri.parse(Endpoints.baseUrl + '/api/get-talkers?id=${id.toString()}'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return SearchUser.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (SearchUser.fromJson(json.decode(reply)).error);
    }
  }

  static Future<SearchUser> searchListTalkers(
      String token, int id, String key) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/search-talkers'));
    request.headers.set('Authorization', token);

    Map map = {
      "key": key,
      "id": id,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return SearchUser.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (SearchUser.fromJson(json.decode(reply)).error);
    }
  }

  static Future<SearchUser> getListListeners(String token, int id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .getUrl(Uri.parse(Endpoints.baseUrl + '/api/get-listeners?id=$id'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return SearchUser.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (SearchUser.fromJson(json.decode(reply)).error);
    }
  }

  static Future<SearchUser> searchListListeners(
      String token, int id, String key) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/search-listeners'));
    request.headers.set('Authorization', token);

    Map map = {
      "key": key,
      "id": id,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return SearchUser.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (SearchUser.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> updateTokenFCM(
      String token, String token_firebase, String pushkitToken) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/update-token-firebase'));
    request.headers.set('Authorization', token);

    Map map = {"token_firebase": token_firebase, "token_ios": pushkitToken};

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> deletePodcast(String token, int id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/delete-podcast'));
    request.headers.set('Authorization', token);

    Map map = {
      "id": id,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> editPodcast(String token, String nom, int category,
      var list, String file, String duree, String description, int id) async {
    print("aaa" + category.toString());
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    String array = "";
    // make GET request
    for (var i = 0; i < list.length; i++) {
      if (i != list.length - 1) {
        array = array + "tags[$i]=" + list[i] + "&";
      } else {
        array = array + "tags[$i]=" + list[i];
      }
    }
    print(array);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/edit-podcast?$array'));
    request.headers.set('Authorization', token);

    Map map = {
      "nom": nom,
      "category": category,
      "dure": duree,
      "file": file,
      "description": description,
      "id": id,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (DataGroupe.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> deleteMessage(String token, int id_message) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/delete-message'));
    request.headers.set('Authorization', token);

    Map map = {
      "id_message": id_message,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> blockUser(String token, int id_user) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.postUrl(Uri.parse(Endpoints.baseUrl + '/api/bloquer'));
    request.headers.set('Authorization', token);

    Map map = {
      "id": id_user,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> signalerUser(
      String token, int id_user, String cause) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.postUrl(Uri.parse(Endpoints.baseUrl + '/api/signaler'));
    request.headers.set('Authorization', token);

    Map map = {
      "cause": cause,
      "id": id_user,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> call(
      String token, int id_reciever, String id_conv) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.postUrl(Uri.parse(Endpoints.baseUrl + '/api/call'));
    request.headers.set('Authorization', token);

    Map map = {
      "id_reciever": id_reciever,
      "id_conv": id_conv,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> endCall(
      String token, int id_reciever, String id_conv, int call_id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.postUrl(Uri.parse(Endpoints.baseUrl + '/api/end-call'));
    request.headers.set('Authorization', token);

    Map map = {
      "id_reciever": id_reciever,
      "id_conv": id_conv,
      "call_id": call_id
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> acceptCall(String token, int call_id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request =
        await client.postUrl(Uri.parse(Endpoints.baseUrl + '/api/accept-call'));
    request.headers.set('Authorization', token);

    Map map = {"call_id": call_id};

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> sendInstantMessage(
      String token,
      var id_recivers,
      String type,
      String id_conv,
      String message,
      String extension,
      String file) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    String array = "";
    // make GET request
    for (var i = 0; i < id_recivers.length; i++) {
      if (i != id_recivers.length - 1) {
        array = array + "id_recivers[$i]=" + id_recivers[i].toString() + "&";
      } else {
        array = array + "id_recivers[$i]=" + id_recivers[i].toString();
      }
    }
    // make GET request
    HttpClientRequest request = await client.postUrl(
        Uri.parse(Endpoints.baseUrl + '/api/send-instant-message?$array'));
    request.headers.set('Authorization', token);

    Map map = {
      "type": type,
      "message": message,
      "id_conv": id_conv,
      "extension": extension,
      "file": file
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> likePodcast(String token, int id_podcast) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/like-podcast'));
    request.headers.set('Authorization', token);

    Map map = {"id_podcast": id_podcast};

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> dislikePodcast(String token, int id_podcast) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/dislike-podcast'));
    request.headers.set('Authorization', token);

    Map map = {"id_podcast": id_podcast};

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> addComment(
      String token, int id_voice, String durre, int id_podcast) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/add-comment-podcast'));
    request.headers.set('Authorization', token);

    Map map = {"id_voice": id_voice, "duree": durre, "id_podcast": id_podcast};

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> addQucikComment(
      String token, String file, String durre, int id_podcast) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client.postUrl(
        Uri.parse(Endpoints.baseUrl + '/api/add-quick-comment-podcast'));
    request.headers.set('Authorization', token);

    Map map = {"file": file, "duree": durre, "id_podcast": id_podcast};

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<ListComments> getListComments(
      String token, int id_podcast) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(Uri.parse(
        Endpoints.baseUrl +
            '/api/list-comment-podcast?id_podcast=$id_podcast'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return ListComments.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw ("Error");
    }
  }

  static Future<SearchUser> getListLike(String token, int id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(Uri.parse(
        Endpoints.baseUrl +
            '/api/list-like-podcast?id_podcast=${id.toString()}'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return SearchUser.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (SearchUser.fromJson(json.decode(reply)).error);
    }
  }

  static Future<SearchUser> getListDislike(String token, int id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client.getUrl(Uri.parse(
        Endpoints.baseUrl +
            '/api/list-dislike-podcast?id_podcast=${id.toString()}'));
    //request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', token);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    //print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return SearchUser.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (SearchUser.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> likeComment(String token, int id_comment) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/like-comment-podcast'));
    request.headers.set('Authorization', token);

    Map map = {"id_comment": id_comment};

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> dislikeComment(String token, int id_comment) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/dislike-comment-podcast'));
    request.headers.set('Authorization', token);

    Map map = {"id_comment": id_comment};

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }

  static Future<Data> deleteComment(String token, int id) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // make GET request
    HttpClientRequest request = await client
        .postUrl(Uri.parse(Endpoints.baseUrl + '/api/delete-comment-podcast'));
    request.headers.set('Authorization', token);

    Map map = {
      "id_comment": id,
    };

    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Data.fromJson(json.decode(reply));
    } else {
      // If that response was not OK, throw an error.
      throw (Data.fromJson(json.decode(reply)).error);
    }
  }
}
