class DataGroupe {
  String error;
  int data;

  DataGroupe({this.error, this.data});

  DataGroupe.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['data'] = this.data;
    return data;
  }
}
