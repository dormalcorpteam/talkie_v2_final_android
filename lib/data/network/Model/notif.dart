class notif {
  String _callerPhoto;
  String _callerName;
  String _clickAction;
  String _callId;
  String _action;
  String _recieverId;
  String _chanelId;
  String _priority;
  String _type;
  String _callerId;

  notif(
      {String callerPhoto,
      String callerName,
      String clickAction,
      String callId,
      String action,
      String recieverId,
      String chanelId,
      String priority,
      String type,
      String callerId}) {
    this._callerPhoto = callerPhoto;
    this._callerName = callerName;
    this._clickAction = clickAction;
    this._callId = callId;
    this._action = action;
    this._recieverId = recieverId;
    this._chanelId = chanelId;
    this._priority = priority;
    this._type = type;
    this._callerId = callerId;
  }

  String get callerPhoto => _callerPhoto;
  set callerPhoto(String callerPhoto) => _callerPhoto = callerPhoto;
  String get callerName => _callerName;
  set callerName(String callerName) => _callerName = callerName;
  String get clickAction => _clickAction;
  set clickAction(String clickAction) => _clickAction = clickAction;
  String get callId => _callId;
  set callId(String callId) => _callId = callId;
  String get action => _action;
  set action(String action) => _action = action;
  String get recieverId => _recieverId;
  set recieverId(String recieverId) => _recieverId = recieverId;
  String get chanelId => _chanelId;
  set chanelId(String chanelId) => _chanelId = chanelId;
  String get priority => _priority;
  set priority(String priority) => _priority = priority;
  String get type => _type;
  set type(String type) => _type = type;
  String get callerId => _callerId;
  set callerId(String callerId) => _callerId = callerId;

  notif.fromJson(Map<String, dynamic> json) {
    _callerPhoto = json['caller_photo'];
    _callerName = json['caller_name'];
    _clickAction = json['click_action'];
    _callId = json['call_id'];
    _action = json['action'];
    _recieverId = json['reciever_id'];
    _chanelId = json['chanel_id'];
    _priority = json['priority'];
    _type = json['type'];
    _callerId = json['caller_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['caller_photo'] = this._callerPhoto;
    data['caller_name'] = this._callerName;
    data['click_action'] = this._clickAction;
    data['call_id'] = this._callId;
    data['action'] = this._action;
    data['reciever_id'] = this._recieverId;
    data['chanel_id'] = this._chanelId;
    data['priority'] = this._priority;
    data['type'] = this._type;
    data['caller_id'] = this._callerId;
    return data;
  }
}
