class SuggestionConversation {
  List<SuggestionConversationData> data;

  SuggestionConversation({this.data});

  SuggestionConversation.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<SuggestionConversationData>();
      json['data'].forEach((v) {
        data.add(new SuggestionConversationData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SuggestionConversationData {
  String nom;
  String photo;
  List<int> idReceiver;
  String  idConv;
  bool value=false;
  SuggestionConversationData({this.nom, this.photo, this.idReceiver, this.idConv, this.value
});

  SuggestionConversationData.fromJson(Map<String, dynamic> json) {
    nom = json['nom'];
    photo = json['photo'];
    idReceiver = json['id_receiver'].cast<int>();
    idConv = json['id_conv'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nom'] = this.nom;
    data['photo'] = this.photo;
    data['id_receiver'] = this.idReceiver;
    data['id_conv'] = this.idConv;
    return data;
  }
}
