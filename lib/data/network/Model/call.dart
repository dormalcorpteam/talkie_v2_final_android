class Call {
  String callerId;
  String callerName;
  String callerPic;
  int receiverId;
  String channelId;
  String receiverName;
  String receiverPhoto;
  int call_id;
  Call({
    this.callerId,
    this.callerName,
    this.callerPic,
    this.receiverId,
    this.channelId,
    this.call_id,
    this.receiverName,
    this.receiverPhoto
  });

  // to map
  Map<String, dynamic> toMap(Call call) {
    Map<String, dynamic> callMap = Map();
    callMap["caller_id"] = call.callerId;
    callMap["caller_name"] = call.callerName;
    callMap["caller_pic"] = call.callerPic;
    callMap["receiver_id"] = call.receiverId;
    callMap["channel_id"] = call.channelId;
    callMap["call_id"] = call.call_id;
    callMap["receiverName"] = call.receiverName;
    callMap["receiverPhoto"] = call.receiverPhoto;
    return callMap;
  }

  Call.fromMap(Map callMap) {
    this.callerId = callMap["caller_id"];
    this.callerName = callMap["caller_name"];
    this.callerPic = callMap["caller_pic"];
    this.receiverId = callMap["receiver_id"];
    this.channelId = callMap["channel_id"];
    this.call_id = callMap["call_id"];
    this.receiverName=callMap["receiverName"];
    this.receiverPhoto=callMap["receiverPhoto"];
  }
}
