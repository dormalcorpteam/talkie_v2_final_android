import 'package:voicechanger/data/network/Model/Podcasts.dart';

class MyWorld {
  MyWorlEntete entete;
  List<PodcastsData> data;
  int nbrTalkers;
  int nbrListeners;

  MyWorld({this.entete, this.data,this.nbrTalkers,this.nbrListeners});

  MyWorld.fromJson(Map<String, dynamic> json) {
    entete =
        json['entete'] != null ? new MyWorlEntete.fromJson(json['entete']) : null;
    if (json['data'] != null) {
      data = new List<PodcastsData>();
      json['data'].forEach((v) {
        data.add(new PodcastsData.fromJson(v));
      });
    }
    nbrTalkers = json['nbr_talkers'];
    nbrListeners = json['nbr_listeners'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.entete != null) {
      data['entete'] = this.entete.toJson();
    }
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
        data['nbr_talkers'] = this.nbrTalkers;
    data['nbr_listeners'] = this.nbrListeners;

    return data;
  }
}

class MyWorlEntete {
  String nom;
  String username;
  int nbrPodcast;
  String photo;
  int id;

  MyWorlEntete({this.nom, this.username, this.nbrPodcast,this.photo,this.id});

  MyWorlEntete.fromJson(Map<String, dynamic> json) {
    nom = json['nom'];
    username = json['username'];
    nbrPodcast = json['nbr_podcast'];
    photo = json['photo'];
    id = json['id'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nom'] = this.nom;
    data['username'] = this.username;
    data['nbr_podcast'] = this.nbrPodcast;
    data['photo'] = this.photo;
    data['id'] = this.id;

    return data;
  }
}


