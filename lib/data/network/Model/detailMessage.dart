import 'package:voicechanger/Page/MyWidgetSlider.dart';
import 'package:voicechanger/data/network/Model/Podcasts.dart';

class DetailMessage {
  Detail data;

  DetailMessage({this.data});

  DetailMessage.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Detail.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Detail {
  Entete entete;
  List<DetailMessages> messages;
  Sender sender;
  List<Sender> receivers;
  Pagination pagination;

  Detail(
      {this.entete,
      this.messages,
      this.sender,
      this.receivers,
      this.pagination});

  Detail.fromJson(Map<String, dynamic> json) {
    entete =
        json['entete'] != null ? new Entete.fromJson(json['entete']) : null;
    sender =
        json['sender'] != null ? new Sender.fromJson(json['sender']) : null;
    pagination = json['pagination'] != null
        ? new Pagination.fromJson(json['pagination'])
        : null;

    if (json['messages'] != null) {
      messages = new List<DetailMessages>();
      json['messages'].forEach((v) {
        messages.add(new DetailMessages.fromJson(v));
      });
    }
    if (json['receivers'] != null) {
      receivers = new List<Sender>();
      json['receivers'].forEach((v) {
        receivers.add(new Sender.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.entete != null) {
      data['entete'] = this.entete.toJson();
    }
    if (this.messages != null) {
      data['messages'] = this.messages.map((v) => v.toJson()).toList();
    }
    if (this.sender != null) {
      data['sender'] = this.sender.toJson();
    }
    if (this.receivers != null) {
      data['receivers'] = this.receivers.map((v) => v.toJson()).toList();
    }
    if (this.pagination != null) {
      data['pagination'] = this.pagination.toJson();
    }
    return data;
  }
}

class Entete {
  String nom;
  String photo;
  int id_conv;
  String talkers;
  List<int> idReceivers;
  bool is_groupe;
  Entete(
      {this.nom,
      this.photo,
      this.id_conv,
      this.talkers,
      this.idReceivers,
      this.is_groupe});

  Entete.fromJson(Map<String, dynamic> json) {
    nom = json['nom'];
    photo = json['photo'];
    id_conv = json['id_conv'];
    talkers = json['talkers'];
    idReceivers = json['id_receivers'].cast<int>();
    is_groupe = json['is_groupe'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nom'] = this.nom;
    data['photo'] = this.photo;
    data['id_conv'] = this.id_conv;
    data['talkers'] = this.talkers;
    data['id_receivers'] = this.idReceivers;
    data['is_groupe'] = this.is_groupe;
    return data;
  }
}

class DetailMessages {
  int id;
  int sender_id;
  String sender;
  String voice;
  String date;
  String vuPar;
  String sender_photo;
  String duree;
  bool current_user;
  bool selected = false;
  String type;
  String message;
  bool missed_call;
  bool visible = true;

  PlayerState playerState = PlayerState.stopped;
  DetailMessages(
      {this.id,
      this.sender_id,
      this.sender,
      this.voice,
      this.date,
      this.vuPar,
      this.sender_photo,
      this.duree,
      this.current_user,
      this.selected,
      this.type,
      this.message,
      this.missed_call});

  DetailMessages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sender_id = json['sender_id'];
    sender = json['sender'];
    voice = json['voice'];
    date = json['date'];
    vuPar = json['VuPar'];
    sender_photo = json['sender_photo'];
    duree = json['duree'];
    current_user = json['current_user'];
    type = json['type'];
    message = json['message'];
    missed_call = json["missed_call"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['sender_id'] = this.sender_id;

    data['sender'] = this.sender;
    data['voice'] = this.voice;
    data['date'] = this.date;
    data['VuPar'] = this.vuPar;
    data['sender_photo'] = this.sender_photo;
    data['duree'] = this.duree;
    data['current_user'] = this.current_user;
    data['type'] = this.type;
    data['message'] = this.message;
    data['missed_call'] = this.missed_call;
    return data;
  }
}

class Sender {
  int id;
  String nom;
  String photo;

  Sender({this.id, this.nom, this.photo});

  Sender.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nom = json['nom'];
    photo = json['photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nom'] = this.nom;
    data['photo'] = this.photo;
    return data;
  }
}
