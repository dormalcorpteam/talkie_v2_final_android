import 'categories.dart';

class DetailPodcast {
  DetailPodcastData data;
  int nbrTalkers;
  int nbrListeners;

  DetailPodcast({this.data, this.nbrTalkers, this.nbrListeners});

  DetailPodcast.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new DetailPodcastData.fromJson(json['data']) : null;
    nbrTalkers = json['nbr_talkers'];
    nbrListeners = json['nbr_listeners'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['nbr_talkers'] = this.nbrTalkers;
    data['nbr_listeners'] = this.nbrListeners;
    return data;
  }
}



class DetailPodcastData {
  int id;
  String file;
  String createdAt;
  String duree;
  String nom;
  String description;
  UserPodcast user;
  List<String> tags;
  Categorie categorie;
  bool liked;
  bool disliked;
  int nbrLike;
  int nbrDislike;
  int nbrComment;

  DetailPodcastData(
      {this.id,
      this.file,
      this.createdAt,
      this.duree,
      this.nom,
      this.description,
      this.user,
      this.tags,
      this.categorie,
       this.liked,
      this.disliked,
      this.nbrLike,
      this.nbrDislike,
      this.nbrComment,
      });

  DetailPodcastData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    file = json['file'];
    createdAt = json['created_At'];
    duree = json['dure'];
    nom = json['nom'];
    description = json['description'];
        liked = json['liked'];
    disliked = json['disliked'];
    nbrLike = json['nbr_like'];
    nbrDislike = json['nbr_dislike'];
    nbrComment = json['nbr_comment'];

    user = json['user'] != null ? new UserPodcast.fromJson(json['user']) : null;
        tags = json['tags'] != null ? json['tags'].cast<String>():null ;
    categorie = json['categorie'] != null
        ? new Categorie.fromJson(json['categorie'])
        : null;

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['file'] = this.file;
    data['created_At'] = this.createdAt;
    data['dure'] = this.duree;
    data['nom'] = this.nom;
    data['description'] = this.description;
      data['liked'] = this.liked;
    data['disliked'] = this.disliked;
    data['nbr_like'] = this.nbrLike;
    data['nbr_dislike'] = this.nbrDislike;
    data['nbr_comment'] = this.nbrComment;
        if (this.tags != null) {
     data['tags'] = this.tags;
        }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.categorie != null) {
      data['categorie'] = this.categorie.toJson();
    }
    return data;
  }
}

class UserPodcast {
  int id;
  String nom;
  String prenom;
  String photo;

  UserPodcast({this.id, this.nom, this.prenom, this.photo});

  UserPodcast.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nom = json['nom'];
    prenom = json['prenom'];
    photo = json['photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nom'] = this.nom;
    data['prenom'] = this.prenom;
    data['photo'] = this.photo;
    return data;
  }
}
