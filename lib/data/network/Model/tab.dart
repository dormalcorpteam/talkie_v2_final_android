class tab {
  List<IdConvs> idConvs;

  tab({this.idConvs});

  tab.fromJson(Map<String, dynamic> json) {
    if (json['id_convs'] != null) {
      idConvs = new List<IdConvs>();
      json['id_convs'].forEach((v) {
        idConvs.add(new IdConvs.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.idConvs != null) {
      data['id_convs'] = this.idConvs.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class IdConvs {
  String idConv;
  List<int> idReceivers;

  IdConvs({this.idConv, this.idReceivers});

  IdConvs.fromJson(Map<String, dynamic> json) {
    idConv = json['id_conv'];
    idReceivers = json['id_receivers'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_conv'] = this.idConv;
    data['id_receivers'] = this.idReceivers;
    return data;
  }
}
