import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

enum PlayerState { stopped, playing, paused }

class PlayerWidget extends StatefulWidget {
  final String url;
  final PlayerMode mode;
  final duree;
  PlayerWidget(
      {Key key,
      @required this.url,
      this.mode = PlayerMode.MEDIA_PLAYER,
      this.duree})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PlayerWidgetState(url, mode);
  }
}

class _PlayerWidgetState extends State<PlayerWidget> {
  String url;
  PlayerMode mode;

  AudioPlayer _audioPlayer;
  AudioPlayerState _audioPlayerState;
  Duration _duration;
  Duration _position;

  PlayerState _playerState = PlayerState.stopped;
  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  StreamSubscription _playerCompleteSubscription;
  StreamSubscription _playerErrorSubscription;
  StreamSubscription _playerStateSubscription;
  bool seekDone;

  get _isPlaying => _playerState == PlayerState.playing;
  get _isPaused => _playerState == PlayerState.paused;
  get _durationText =>
      "${_duration?.inMinutes?.remainder(60) ?? ''}:${(_duration?.inSeconds?.remainder(60)) ?? ''}";
  get _positionText =>
      "${_position?.inMinutes?.remainder(60) ?? ''}:${(_position?.inSeconds?.remainder(60)) ?? ''}";

  _PlayerWidgetState(this.url, this.mode);

  @override
  void initState() {
    super.initState();
    AudioPlayer.logEnabled = true;
  }

  @override
  dispose() {
    super.dispose();
  }

  Future<int> _stopPlayer() async {
    final result = await _audioPlayer.stop();
    if (result == 1) {
      setState(() {
        _playerState = PlayerState.stopped;
        _position = Duration();
      });
      _onComplete();
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          print(_playerState);
          if (_playerState == PlayerState.paused ||
              _playerState == PlayerState.playing) {
            await _audioPlayer.release();
          }
          Navigator.pop(context);
          return false;
        },
        child: Column(
          children: <Widget>[
            Row(
              children: [
                Expanded(
                    child: Text(_position != null ? _positionText : "0:0",
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          color: Color(0xff949494),
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0.2708333333333333,
                        ))),
                Expanded(
                  child: SliderTheme(
                      data: SliderThemeData(
                          trackHeight: 4,
                          thumbShape:
                              RoundSliderThumbShape(enabledThumbRadius: 0)),
                      child: Slider(
                        activeColor: Color(0xff25a996),
                        inactiveColor: Color(0xffc6c6c6),
                        onChanged: (v) {
                          if (_duration != null) {
                            final Position = v * _duration.inMilliseconds;
                            _audioPlayer
                                .seek(Duration(milliseconds: Position.round()));
                          }
                        },
                        value: (_position != null &&
                                _duration != null &&
                                _position.inMilliseconds > 0 &&
                                _position.inMilliseconds <
                                    _duration.inMilliseconds)
                            ? _position.inMilliseconds /
                                _duration.inMilliseconds
                            : 0.0,
                      )),
                  flex: 10,
                ),
                Expanded(
                  child: Text(_duration != null ? _durationText : widget.duree,
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        color: Color(0xff949494),
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0.2708333333333333,
                      )),
                ),
              ],
            ),
            Padding(
                padding: EdgeInsets.fromLTRB(59, 42, 58, 0),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                          child: SvgPicture.asset(("assets/icon/left.svg")),
                          onTap: () {
                            if (seekDone == false && _position != null) {
                              _audioPlayer.seek(Duration(
                                  milliseconds:
                                      _position.inMilliseconds - 15000));

                              setState(() => seekDone = false);
                            }
                          }),
                      new InkWell(
                        onTap: () => {
                          if (_isPlaying) {_pause()} else {_play()},
                        },
                        child: Container(
                            width: 54,
                            height: 54,
                            decoration: new BoxDecoration(
                              border:
                                  new Border.all(color: Colors.white, width: 2),
                              shape: BoxShape.circle,
                              color: Color(0xff25a996),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0x2b2d2d2d),
                                    offset: Offset(0, 8),
                                    blurRadius: 20,
                                    spreadRadius: 0)
                              ],
                            ),
                            child: _isPlaying
                                ? Padding(
                                    padding: EdgeInsets.all(13),
                                    child: SvgPicture.asset(
                                        ("assets/icon/stop.svg")))
                                : Padding(
                                    padding: EdgeInsets.all(13),
                                    child: SvgPicture.asset(
                                        ("assets/icon/play.svg")))),
                      ),
                      GestureDetector(
                          child: SvgPicture.asset(("assets/icon/next.svg")),
                          onTap: () {
                            if (seekDone == false && _position != null) {
                              _audioPlayer.seek(Duration(
                                  milliseconds:
                                      _position.inMilliseconds + 15000));
                              setState(() => seekDone = false);
                            }
                          }),
                    ])),
          ],
        ));
  }

  Future<void> _initAudioPlayer() async {
    _audioPlayer = AudioPlayer(mode: mode);
    seekDone = false;

    ///await _audioPlayer.setUrl(url);

// prepare the player with this audio but do not start playing
    await _audioPlayer.setReleaseMode(
        ReleaseMode.STOP); // set release mode so that it never releases

    _durationSubscription = _audioPlayer.onDurationChanged.listen((duration) {
      setState(() => {_duration = duration});
      print(duration);
    });

    _positionSubscription =
        _audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
              _position = p;
            }));

    _playerCompleteSubscription =
        _audioPlayer.onPlayerCompletion.listen((event) {
      _onComplete();
      setState(() {
        _position = null;
      });
    });

    _playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        _playerState = PlayerState.stopped;
        _duration = Duration(seconds: 0);
        _position = Duration(seconds: 0);
      });
    });

    _audioPlayer.onPlayerStateChanged.listen((state) {
      setState(() {
        _audioPlayerState = state;
      });
    });

    _audioPlayer.onNotificationPlayerStateChanged.listen((state) {
      setState(() => _audioPlayerState = state);
    });
  }

  Future<int> _play() async {
    if (_playerState != PlayerState.paused) {
      _initAudioPlayer();
    }

    if (seekDone == true) {
      seekDone = false;
    }
    final playPosition = (_position != null &&
            _duration != null &&
            _position.inMilliseconds > 0 &&
            _position.inMilliseconds < _duration.inMilliseconds)
        ? _position
        : null;
    final result = await _audioPlayer.play(url, position: playPosition);

    if (result == 1) {
      setState(() => _playerState = PlayerState.playing);
    }
    print(_playerState);

    // default playback rate is 1.0
    // this should be called after _audioPlayer.play() or _audioPlayer.resume()
    // this can also be called everytime the user wants to change playback rate in the UI
    _audioPlayer.setPlaybackRate(playbackRate: 1.0);

    return result;
  }

  Future<int> _pause() async {
    final result = await _audioPlayer.pause();
    if (result == 1) setState(() => _playerState = PlayerState.paused);
    return result;
  }

  Future<int> _stop() async {
    final result = await _audioPlayer.stop();
    if (result == 1) {
      setState(() {
        _playerState = PlayerState.stopped;
        _position = Duration();
      });
    }
    return result;
  }

  void _onComplete() {
    setState(() => {
          _playerState = PlayerState.stopped,
          seekDone = true,
        });
  }
}
